/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * abstract constant interface
 *
 */

class IfConstant : public IfObject {

 public:
  IfConstant(char* Name = NULL, 
             unsigned Flags = 0);
  virtual ~IfConstant() {}

 public:
  inline IfType* GetType() const { return m_pType; }
  virtual IfConstant* GetBase() const { return (IfConstant*) this; }

 public:
  virtual const char* GetClass() const { return "constant"; }

  virtual int IsBoolean() const { return 0; }
  virtual int IsInteger() const { return 0; }
  virtual int IsReal() const { return 0; }
  virtual int IsPid() const { return 0; }
  virtual int IsEnum() const { return 0; }
  virtual int IsOther() const { return 0; }

 public:
  virtual void Dump(FILE* file) const;
  virtual void DumpValue(FILE* file) const;
  virtual void DumpNameOrValue(FILE* file) const;

  virtual void PostCompile();

  virtual void Code(FILE* file) const;
  virtual void CodeValue(FILE* file) const;
  virtual void CodeNameOrValue(FILE* file) const;

 protected:
  IfType* m_pType; 

};

/*
 *
 * defines boolean-valued constants : false, true
 *
 */

class IfBooleanConstant : public IfConstant {

 public:
  enum EValue {FALSE, TRUE};

 public:
  IfBooleanConstant(EValue Value,
                    char* Name = NULL, 
                    unsigned Flags = 0);
  virtual ~IfBooleanConstant() {}

 public:
  inline EValue GetValue() const { return m_eValue; }
  inline void SetValue(const EValue Value) { m_eValue = Value; }

 public:
  virtual int IsBoolean() const { return 1; }
  virtual void DumpValue(FILE* file) const;
  virtual void PreCompile();
  virtual void CodeValue(FILE* file) const;

 protected:
  EValue m_eValue;

};

/*
 *
 * defines integer-valued constants : 0, 1, 2, 3 ...
 *
 */

class IfIntegerConstant : public IfConstant {

 public:
  IfIntegerConstant(int Value, 
                    char* Name = NULL, 
                    unsigned Flags = 0);
  virtual ~IfIntegerConstant() {}

 public:
  inline int GetValue() const { return m_iValue; }
  inline void SetValue(const int Value) { m_iValue = Value; }

 public:
  virtual int IsInteger() const { return 1; }
  virtual void DumpValue(FILE* file) const;
  virtual void PreCompile();
  virtual void CodeValue(FILE* file) const;

 protected:
  int m_iValue;

};

/*
 *
 * defines real-valued constants : 0.00, 0.01, ...
 *
 */

class IfRealConstant : public IfConstant {

 public:
  IfRealConstant(float Value, 
                 char* Name = NULL, 
                 unsigned Flags = 0);
  virtual ~IfRealConstant() {}

 public:
  inline float GetValue() const { return m_fValue; }
  inline void SetValue(const float Value) { m_fValue = Value; }

 public:
  virtual int IsReal() const { return 1; }
  virtual void DumpValue(FILE* file) const;
  virtual void PreCompile();
  virtual void CodeValue(FILE* file) const;

 protected:
  float m_fValue;

};

/*
 *
 * defines pid-valued constants : null, self
 *
 */

class IfPidConstant : public IfConstant {

 public:
  enum EValue {NIL, SELF};

 public:
  IfPidConstant(EValue Value,
                char* Name = NULL, 
                unsigned Flags = 0);
  virtual ~IfPidConstant() {}
  
 public:
  inline EValue GetValue() const { return m_eValue; }
  inline void SetValue(const EValue Value) { m_eValue = Value; }

 public:
  virtual int IsPid() const { return 1; }
  virtual void DumpValue(FILE* file) const;
  virtual void PreCompile();
  virtual void CodeValue(FILE* file) const;

 protected:
  EValue m_eValue;

};

/*
 *
 * defines enum-valued constants
 *
 */

class IfEnumConstant : public IfConstant {
  
 public:
  IfEnumConstant(char* Name, 
                 unsigned Flags = 0);
  virtual ~IfEnumConstant() {}

 public:
  inline int GetValue() const { return m_iValue; }
  inline void SetValue(const int Value) { m_iValue = Value; }

 public:
  virtual int IsEnum() const { return 1; }
  virtual void DumpValue(FILE* file) const;
  virtual void PreCompile();
  virtual void CodeValue(FILE* file) const;

 protected:
  int m_iValue;

};

/*
 *
 * defines other-constant constants [rename] 
 *
 */

class IfOtherConstant : public IfConstant {

 public:  
  IfOtherConstant(IfConstant* Other,
                  char* Name, 
                  unsigned Flags = 0);
  virtual ~IfOtherConstant() {}

 public:
  inline IfConstant* GetOther() const { return m_pOther; }
  virtual IfConstant* GetBase() const { return m_pOther->GetBase(); }

 public:
  virtual int IsOther() const { return 1; }
  virtual void DumpValue(FILE* file) const;
  virtual void PreCompile();
  virtual void CodeValue(FILE* file) const;

 protected:
  IfConstant* m_pOther;

};
