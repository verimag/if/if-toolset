/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * state interface
 *
 */

class IfState : public IfObject {
 public:
  enum CFlag { STABLE = 1024, START  = 2048, 
               TNONE = 4096, TSIG = 8192,
               CONCURRENT = 16384,
               SUCCESS = 32768, ERROR = 65536,  // not used (Marius' observers)
               CONTROL = 131072};
                           
 public:
  IfState(IfConstraint* Tpc, 
          IfList<IfSignal>* Saves, 
          IfList<IfTransition>* Transitions, 
          IfList<IfState>* States,
          char* Option[], 
          char* Name, 
          unsigned Flags = 0);
  virtual ~IfState();

 public:
  inline IfConstraint* GetTpc() const { return m_pTpc; }
  inline IfList<IfSignal>* GetSaves() const { return m_pSaves; }
  inline IfList<IfTransition>* GetOutTransitions() const { return m_pOutTransitions; }
  inline IfList<IfTransition>* GetTransitions() const { return GetOutTransitions(); }
  inline IfList<IfState>* GetStates() const { return m_pStates; }
  inline int IsStable() const { return GetFlag(STABLE) ? 1 : 0; }
  inline int IsStart() const { return GetFlag(START) ? 1 : 0; }
  inline int IsConcurrent() const { return GetFlag(CONCURRENT) ? 1 : 0; }
  inline int IsControl() const { return GetFlag(CONTROL) ? 1 : 0; }

  inline IfList<IfTransition>* GetInTransitions() const { return m_pInTransitions; }
  inline IfState* GetParent() const { return m_pParent; }
  inline IfState* GetEntry() const { return m_pEntry; }
  inline IfAutomatonEntity* GetProcess() const { return m_pProcess; }

  IfState* Find(const char*) const;
  IfState* FindVisible(const char*) const;
  IfState* FindVisibleRec(const char*) const;

  virtual const char* GetClass() const 
    { return "state"; }
  virtual int IsState() const
    { return 1; }

  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;
  virtual void SetParent(IfState* state);
  virtual void SetIndex(int* Index);

  virtual void CodeEntry(FILE* file) const;
  virtual void CodeEntryRecursive(FILE* file) const;
  virtual void CodeExit(FILE* file) const;
  virtual void CodeExitRecursive(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 public:
  virtual unsigned GetStatus(const IfSignal* Signal) const;
  virtual void CodeTable(FILE* file) const;

  static IfState* 
    LeastCommonAncestor(const IfState*, const IfState*);

 protected:
  IfConstraint* m_pTpc;                // time progress condition
  IfList<IfSignal>* m_pSaves;                    // saved signals
  IfList<IfTransition>* m_pOutTransitions; // outgoing transitions
  IfList<IfState>* m_pStates;                 // nested substates
  int m_iSuccess;                         // success state marker
  int m_iError;                             // error state marker

 protected:
  IfAutomatonEntity* m_pProcess;                 // nesting process
  IfList<IfTransition>* m_pInTransitions;   // ingoing transitions 
  IfState* m_pParent;                             // parent state
  IfState* m_pEntry;                               // entry state 
  IfList<IfSignal>* m_pInputs;           // (local) input signals
  int m_iInputNone;                         // (local) input none

};

