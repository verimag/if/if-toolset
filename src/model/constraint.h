/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * abstract constraint interface
 *
 */

class IfConstraint : public IfObject {

 public:
  IfConstraint(unsigned Flags = 0);
  virtual ~IfConstraint();

 public:
  virtual const char* GetClass() const 
    { return "constraint"; }
  virtual int IsConstraint() const 
    { return 1; }

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 public:
  virtual int IsBasic() const { return 0; }
  virtual int IsComposed() const { return 0; }

};

/*
 *
 * basic constraints interface
 *
 */

class IfBasicConstraint : public IfConstraint {

 public:
  enum EOperator { LT, LE, EQ, GE, GT };

 public:
  IfBasicConstraint(IfExpression* X, 
                    IfExpression* Y,
                    EOperator Operator,
                    IfExpression* K,
                    unsigned Flags = 0);
  virtual ~IfBasicConstraint();

 public:
  inline EOperator GetOperator() const { return m_eOperator; }
  inline IfExpression* GetX() const { return m_pX; }
  inline IfExpression* GetY() const { return m_pY; }
  inline IfExpression* GetK() const { return m_pK; }
  inline void SetOperator(const EOperator Operator) { m_eOperator = Operator; } 

 public:
  virtual int IsBasic() const { return 1; }

 public:
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;  

 public:
  virtual int Use(const IfVariable* Variable) const;

 public:
  static IfExpression::Operator OPERATOR[];

 protected:
  EOperator m_eOperator;
  IfExpression* m_pX;
  IfExpression* m_pY;
  IfExpression* m_pK;
};

/*
 *
 * composed constraints interface
 *
 */

class IfComposedConstraint : public IfConstraint {

 public:
  IfComposedConstraint(IfList<IfBasicConstraint>* Constraints,
                       unsigned Flags = 0);
  virtual ~IfComposedConstraint();

 public:
  inline IfList<IfBasicConstraint>* GetConstraints() const 
    { return m_pConstraints; }
  virtual int IsComposed() const { return 1; }

 public:
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;

 protected:
  IfList<IfBasicConstraint>* m_pConstraints;

};
