/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * transition interface
 *
 */

class IfTransition : public IfObject {

 public:
  enum EDeadline { EAGER = 0, DELAYABLE, LAZY };

 public:
  IfTransition(float Probability,
               EDeadline Deadline, 
               int Priority, 
               IfAction* Acquire,
               IfExpression* Provided,
               IfConstraint* When,
               IfAction* Input,
               IfBlockStatement* Body, 
               IfAction* Terminator, 
               char* Name = NULL, 
               unsigned Flags = 0);
  virtual ~IfTransition();

 public:
  inline float GetProbability() const { return m_fProbability; }
  inline EDeadline GetDeadline() const { return m_eDeadline; }
  inline int IsEager() const { return m_eDeadline == EAGER; }
  inline int IsDelayable() const { return m_eDeadline == DELAYABLE; }
  inline int IsLazy() const { return m_eDeadline == LAZY; }
  inline int GetPriority() const { return m_iPriority; }
  inline IfAction* GetAcquire() const { return m_pAcquire; }
  inline IfExpression* GetProvided() const { return m_pProvided; }
  inline IfConstraint* GetWhen() const { return m_pWhen; }
  inline IfAction* GetInput() const { return m_pInput; }
  inline IfBlockStatement* GetBody() const { return m_pBody; }
  inline IfAction* GetTerminator() const { return m_pTerminator; }
  inline IfState* GetSource() const { return m_pSource; }
  inline IfState* GetTarget() const { return m_pTarget; }

 public:
  inline void SetAcquire(IfAction* Acquire) { m_pAcquire = Acquire; }
  inline void SetProvided(IfExpression* Provided) { m_pProvided = Provided; }
  inline void SetWhen(IfConstraint* When) { m_pWhen = When; }
  inline void SetInput(IfAction* Input) { m_pInput = Input; }
  inline void SetBody(IfBlockStatement* Body) { m_pBody = Body; }
  inline void SetTerminator(IfAction* Terminator) { m_pTerminator = Terminator; }

 public:
  virtual const char* GetClass() const { return "transition"; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Ask(const IfVariable* Variable) const;
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  float m_fProbability;
  EDeadline m_eDeadline;
  int m_iPriority;
  IfAction* m_pAcquire;
  IfExpression* m_pProvided;
  IfExpression* m_pProvidedTransformed;  // allow transformation of guard in PreCompile, keeping old one also
  IfConstraint* m_pWhen;
  IfAction* m_pInput;
  IfBlockStatement* m_pBody; 
  IfAction* m_pTerminator;

  IfState* m_pSource;
  IfState* m_pTarget;
  
 protected:
  static const char* DEADLINE[];

};
