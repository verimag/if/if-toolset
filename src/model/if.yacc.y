/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */


%{ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "model.h"

extern int yylex();                           /* scanner function */
extern char* yytext;                             /* scanner token */
extern int yylineno;                       /* current line number */
extern FILE* yyin;

IfSystemEntity* SYSTEM;                          /* system entity */
IfAutomatonEntity* PROCESS;         /* process or observer entity */ 
IfPriorityRuleEntity* PRIORITYRULE;       /* priority rule entity */

IfState* STACK[32];               /* state stack (max 32 nesting) */
IfState* *STATE = STACK;

static int STATUS = 0;                  /* internal (compilation) */

enum { 
  WHEN     = 1,             /* solve AND_K conflicts (constraint) */
  ABSTRACT = 2           /* solve abstract type cross definitions */
};

#ifdef CYGWIN
int isatty(int desc)
{
  return 1;
}
#endif

/*
 *
 * contextual name binding
 *
 */

IfObject* Bind(const IfEntity* Entity, 
	       const char* Name, 
	       const char Kind) {
  IfObject* object = NULL;
  int i;
  switch (Kind) {
  case 'C': // constants
    object = Entity->GetConstants()->Find(Name);
    for(i = 0; i < Entity->GetTypes()->GetCount() && !object; i++)
      if (Entity->GetTypes()->GetAt(i)->IsEnum()) {
	IfEnumType* type = (IfEnumType*) Entity->GetTypes()->GetAt(i);
	object = type->GetLiterals()->Find(Name);
      }
    break;
  case 'F': // functions
    for(i = 0; i < Entity->GetTypes()->GetCount() && !object; i++)
      if (Entity->GetTypes()->GetAt(i)->IsAbstract()) {
	IfAbstractType* type = (IfAbstractType*) Entity->GetTypes()->GetAt(i);
	object = type->GetFunctions()->Find(Name);
      }
    break;
  case 'V': // variables
    object = Entity->GetVariables()->Find(Name);
    if (Entity->IsProcess() && !object) 
      object = ((IfProcessEntity*) Entity)->GetParameters()->Find(Name);
    break;
  case 'T': // types
    object = Entity->TYPES.Find(Name);
    if (!object)
      object = Entity->GetTypes()->Find(Name);
    break;
  case 'M': // procedures
    object = Entity->GetProcedures()->Find(Name);
    break;
  case 'S': // signal
    if (Entity->IsSystem())
      object = ((IfSystemEntity*) Entity)->GetSignals()->Find(Name);
    break;
  case 'R': // signalroutes
    if (Entity->IsSystem())
      object = ((IfSystemEntity*) Entity)->GetSignalroutes()->Find(Name);
    break;
  case 'X': // resources
    if (Entity->IsSystem())
      object = ((IfSystemEntity*) Entity)->GetResources()->Find(Name);
    break; 
  default: // unknown
    break;
  }
  return object;
}

IfObject* Bind(const char* Name, const char Kind) {
  IfObject* object = NULL;
  
  if (!object && Kind=='V' && PRIORITYRULE) {
      if(!strcmp(PRIORITYRULE->GetProcessVar1()->GetName(), Name)) 
           object = PRIORITYRULE->GetProcessVar1();
      if(!strcmp(PRIORITYRULE->GetProcessVar2()->GetName(), Name))
           object = PRIORITYRULE->GetProcessVar2();
  }
  if (!object && PROCESS) object = Bind(PROCESS, Name, Kind);
  if (!object && SYSTEM) object = Bind(SYSTEM, Name, Kind);
  
  return object;
}

/*
 *
 * error handling
 *
 */

void yyerror(const char* msg) {
  fprintf(stderr, "line %d : %s (at or near `%s')\n", 
	  yylineno, msg, yytext);
  exit(1);
}
 
void yywarning(const char* msg) {
  fprintf(stderr, "line %d : %s (at or near `%s')\n", 
	  yylineno, msg, yytext);
}

#include "if.lex.i"

%}

%expect 0

%union{
  IfConstant* constant;
  IfList<IfConstant>* constant_list;
  IfEnumConstant* literal;
  IfList<IfEnumConstant>* literal_list;
  IfFunction* function;
  IfList<IfFunction>* function_list;
  IfType* type;
  IfList<IfType>* type_list;
  IfVariable::EKind vkind;
  IfVariable::EScope scope;
  IfVariable::EMode mode;
  IfVariable* variable;
  IfList<IfVariable>* variable_list;
  IfExpression* expression;
  IfList<IfExpression>* expression_list;
  IfConstraint* constraint;
  IfResource* resource;
  IfList<IfResource>* resource_list;
  IfSignal* signal;
  IfList<IfSignal>* signal_list;
  IfSignalroute* signalroute;
  IfProcedure* procedure;
  IfAction* action;
  IfBasicStatement* statement;
  IfBlockStatement* block;
  IfTransition* transition;
  IfList<IfTransition>* transition_list;
  IfList<IfObject>* transition_state_decl_list;
  IfTransition::EDeadline deadline;
  IfState* state;
  IfList<IfState>* state_list;
  IfEntity* entity;
  IfList<IfEntity>* entity_list;
  IfPriorityRuleEntity* priority_rule;
  IfList<IfPriorityRuleEntity>* priority_rule_list;
  char* name;
  char** name_list;
  int integer;
  float real;
}

%type <constant> constant constant_decl constant_body constant_value range_bound

%type <literal> enum_decl  
%type <literal_list> enum_decl_list

%type <function> function_decl 
%type <function_list> function_decl_set

%type <type> type type_decl type_body type_value returns_opt
%type <type_list> type_list type_opt_list

%type <variable> field_decl variable_decl fpar_decl
%type <variable_list> field_decl_set fpar_decl_opt_list fpar_decl_list 

%type <scope> scope

%type <expression> expression expression_opt provided_opt to_opt via_opt initializer obs_opt_in obs_output_opt_ord obs_output_opt_from priority_expression

%type <expression_list> expression_list expression_opt_list  

%type <constraint> constraint when_opt tpc_opt

%type <resource> resource resource_decl
%type <resource_list> resource_list

%type <signal> signal signal_decl
%type <signal_list> signal_list save_opt

%type <signalroute> signalroute_decl

%type <mode> fpar_mode
%type <procedure> procedure procedure_decl

%type <action> action input input_opt terminator

%type <action> match match_input match_discard match_output match_fork match_kill match_deliver match_informal match_probability

%type <action> acquire acquire_opt

%type <block> block
%type <statement> statement

%type <transition> transition

%type <deadline> deadline_opt

%type <state> state_decl

%type <transition_state_decl_list> transition_state_decl_set

%type <entity> process_decl system_decl observer_decl

%type <name> constant_name type_name function_name 
%type <name> field_name variable_name fpar_name name 
%type <name> signal_name signalroute_name signalroute_endpoint
%type <name> state_name state_reference procedure_name process_name system_name process_name_opt
%type <name> resource_name
%type <name> string code_opt option

%type <name_list> option_set

%type <integer> integer priority_opt obs_kind
%type <real> real probability_opt

%type <name>                priority_rule_id
%type <priority_rule>       priority_decl
%type <priority_rule_list>  priority_decl_set
%type <priority_rule_list>  priority_section

%token  _IDENTIFIER_ _LITERAL_ _XSTATE_ _STRING_ _CODE_ _OPTION_

%token CONST_K TRUE_K FALSE_K SELF_K NIL_K _INTEGER_ _REAL_

%token TYPE_K ENUM_K ENDENUM_K RANGE_K DDOT_K ARRAY_K TREE_K OF_K 
%token RECORD_K ENDRECORD_K STRING_K ABSTRACT_K ENDABSTRACT_K

%token VAR_K PUBLIC_K PRIVATE_K

%token VOID_K NOT_K UMIN_K ACTIVE_K TIMEVAL_K LENGTH_K
%token LE_K GE_K NE_K AND_K OR_K XOR_K
%token INSTATE_K
%token INSTANCEOF_K

%token SIGNAL_K SIGNALROUTE_K FROM_K TO_K WITH_K ENV_K 

%token RESOURCE_K

%token PROCEDURE_K ENDPROCEDURE_K 
%token FPAR_K IN_K INOUT_K OUT_K RETURNS_K 

%token SKIP_K INFORMAL_K TASK_K SET_K RESET_K STOP_K FLUSH_K CUT_K
%token INPUT_K  OUTPUT_K VIA_K DOTSEQ_K
%token OBSINPUT_K OBSOUTPUT_K OBSFORK_K OBSKILLPID_K OBSKILLPROCESS_K
%token MATCH_K DELIVER_K DISCARD_K
%token FORK_K KILL_K CALL_K NEXTSTATE_K
%token ACQUIRE_K RELEASE_K

%token IF_K THEN_K ELSE_K ENDIF_K
%token WHILE_K DO_K ENDWHILE_K

%token DEADLINE_K PRIORITY_K PROVIDED_K WHEN_K PROBABILITY_K

%token STATE_K ENDSTATE_K SAVE_K TPC_K


%token PROCESS_K ENDPROCESS_K
%token OBSERVER_K ENDOBSERVER_K
%token INTRUSIVE_K PURE_K

%token SYSTEM_K ENDSYSTEM_K
%token PRIORITY_SECTION_K END_PRIORITY_SECTION_K
%nonassoc OBSOUTPUT_K '?' ':'

%left ','

%left OR_K XOR_K

%left AND_K

%left '=' NE_K

%left '>' '<' LE_K GE_K

%nonassoc '{' '}'

%left '^'

%left '+' '-'

%left '*' '/' '%'

%right NOT_K 

%right UMIN_K 

%right ACTIVE_K TIMEVAL_K LENGTH_K

%left '.'

%nonassoc '[' ']' '(' ')' 

%nonassoc INSTATE_K INSTANCEOF_K OBSKILLPID_K IN_K FROM_K VIA_K TO_K 

%start system_decl

%%

/*
--------------------------------------------------------------------
Constant
--------------------------------------------------------------------
*/

constant_decl :
  CONST_K constant_name '=' constant_body ';' 
    { $$ = $4; $4->SetName($2); }
;

constant_name : 
  _IDENTIFIER_ { $$ = strdup(yytext); }
| _LITERAL_ { $$ = strdup(yytext); }
;

constant_body : 
  constant_value { $$ = $1; }
| constant { $$ = new IfOtherConstant($1, NULL); }
;

constant_value : 
  TRUE_K { $$ = new IfBooleanConstant(IfBooleanConstant::TRUE, NULL); }
| FALSE_K { $$ = new IfBooleanConstant(IfBooleanConstant::FALSE, NULL); }
| _INTEGER_ { $$ = new IfIntegerConstant(atoi(yytext), NULL); }
| _REAL_ { $$ = new IfRealConstant(atof(yytext), NULL); }
| SELF_K { $$ = new IfPidConstant(IfPidConstant::SELF, NULL); }
| NIL_K { $$ = new IfPidConstant(IfPidConstant::NIL, NULL); }
;

constant:
  constant_name 
    { $$ = (IfConstant*) Bind($1, 'C'); free($1);
      if (!$$) yyerror("undefined constant"); }
;

/*
--------------------------------------------------------------------
Type
--------------------------------------------------------------------
*/

type_decl : 
  TYPE_K type_name '=' type_body ';' 
    { $$ = $4; $$->SetName($2); }
;

type_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

type_body : 
  type_value { $$ = $1; }
| type { $$ = new IfOtherType($1, NULL); }
;

type_value :
  ENUM_K enum_decl_list ENDENUM_K 
    { $$ = new IfEnumType($2, NULL); } 
| RANGE_K range_bound DDOT_K range_bound 
    { $$ = new IfRangeType($2, $4, new IfIntegerConstant(1), NULL); }
| RECORD_K field_decl_set ENDRECORD_K 
    { $$ = new IfRecordType($2, NULL); }
| ARRAY_K '[' constant_body ']' OF_K type
    { $$ = new IfArrayType($3, $6, NULL); }
| STRING_K '[' constant_body ']' OF_K type 
    { $$ = new IfStringType($3, $6, NULL); }
| TREE_K '[' constant_body ',' constant_body ']' OF_K type
    { $$ = new IfTreeType($3, $5, $8, NULL); }
| ABSTRACT_K { STATUS |= ABSTRACT; } function_decl_set ENDABSTRACT_K 
    { IfAbstractType* at = new IfAbstractType($3, NULL); at->Link();
      $$ = at; STATUS &= ~ABSTRACT; }
;

type :
  type_name 
    { $$ = (IfType*) Bind($1, 'T'); free($1); 
      if (!$$ && (STATUS & ABSTRACT))
	$$ = (IfType*) 1;
      if (!$$) yyerror("undefined type"); }
;


type_opt_list :
  type_list { $$ = $1; }
| /* empty */ { $$ = new IfList<IfType>; }
;

type_list : 
  type_list ',' type { $$ = $1; $$->Add($3); }
| type { $$ = new IfList<IfType>; $$->Add($1); }
;

/*
--------------------------------------------------------------------
*/

range_bound :
  constant_body { $$ = $1; }
| '-' integer { $$ = new IfIntegerConstant(-$2, NULL); }
| '-' real { $$ = new IfRealConstant(-$2, NULL); }
;

/*
--------------------------------------------------------------------
*/

enum_decl :
  constant_name { $$ = new IfEnumConstant($1); }
;

enum_decl_list : 
  enum_decl_list ',' enum_decl { $$ = $1; $$->Add($3); } 
| enum_decl { $$ = new IfList<IfEnumConstant>(1); $$->Add($1); }
;

/*
--------------------------------------------------------------------
*/

field_decl :
  field_name type ';'
    { $$ = new IfVariable(IfVariable::FIELD, IfVariable::NOSCOPE, IfVariable::NOMODE, 
			  $2, NULL, $1); }
;

field_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

field_decl_set : 
  field_decl_set field_decl { $$ = $1; $$->Add($2); } 
| { $$ = new IfList<IfVariable>(1); }
;

/*
--------------------------------------------------------------------
*/

function_decl : 
  type function_name '(' type_opt_list ')' ';' 
    { $$ = new IfFunction($4, $1, $2); }
;

function_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

function_decl_set :
  function_decl_set function_decl { $$ = $1; $$->Add($2); }
| /* empty */ { $$ = new IfList<IfFunction>(1); }
;


/* 
--------------------------------------------------------------------
Variable Definition
--------------------------------------------------------------------
*/

variable_decl : 
  VAR_K  variable_name type initializer scope ';' 
    { $$ = new IfVariable(IfVariable::VAR, $5, IfVariable::NOMODE, 
			  $3, $4, $2); }
;

variable_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

scope :
  PUBLIC_K { $$ = IfVariable::PUBLIC; }
| PRIVATE_K { $$ = IfVariable::PRIVATE; }
| /* empty */ { $$ = IfVariable::NOSCOPE; }
;

initializer : 
  DOTSEQ_K expression { $$ = $2; }
| /* empty */ { $$ = NULL; }
;

/* 
--------------------------------------------------------------------
Expression
--------------------------------------------------------------------
*/

expression : 
  VOID_K
    { $$ = new IfVoidExpression(); }
| constant_value 
    { $$ = new IfConstantExpression($1); }
| name '(' expression_opt_list ')' 
    { $$ = new IfCallExpression($1, $3); }
| name 
    { IfConstant* constant = (IfConstant*) Bind($1, 'C');
      IfVariable* variable = (IfVariable*) Bind($1, 'V'); free($1);
      $$ = NULL; 
      if (!$$ && constant) $$ = new IfConstantExpression(constant);
      if (!$$ && variable) $$ = new IfVariableExpression(variable);
      if (!$$) yyerror("undefined reference"); }
| expression '.' name 
    { $$ = new IfFieldExpression($1, $3); }
| expression '[' expression ']' 
    { $$ = new IfIndexExpression($1, $3); }
| expression '[' expression ':' expression ']'
    { $$ = new IfTernaryExpression(IfTernaryExpression::SUBSTRING, $1, $3, $5); }
| '{' name '}' expression 
    { IfObject* object = NULL;
      $$ = NULL;
      if ($$ == NULL && (object = Bind($2, 'T')) != NULL) 
	$$ = new IfCastExpression(IfCastExpression::TYPE, object, $4);
      if ($$ == NULL && (object = Bind($2, 'R')) != NULL)
	$$ = new IfCastExpression(IfCastExpression::ROUTE, object, $4);
      if ($$ == NULL)
	$$ = new IfCastExpression(IfCastExpression::PROCESS, $2, $4);
      else
        free($2); }
| ACTIVE_K expression 
    { $$ = new IfUnaryExpression(IfUnaryExpression::ACTIVE, $2); }
| TIMEVAL_K expression 
    { $$ = new IfUnaryExpression(IfUnaryExpression::TIMEVAL, $2); }
| LENGTH_K expression 
    { $$ = new IfUnaryExpression(IfUnaryExpression::LENGTH, $2); }
| '+' expression %prec UMIN_K 
    { $$ = new IfUnaryExpression(IfUnaryExpression::PLUS, $2); }
| '-' expression %prec UMIN_K 
    { $$ = new IfUnaryExpression(IfUnaryExpression::MINUS, $2); }
| NOT_K expression 
    { $$ = new IfUnaryExpression(IfUnaryExpression::NOT, $2); }
| expression '*' expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::MULT, $3); }
| expression '/' expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::DIV, $3); }
| expression '%' expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::MOD, $3); }
| expression '+' expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::PLUS, $3); }
| expression '-' expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::MINUS, $3); }
| expression '^' expression
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::CONCAT, $3); }
| expression '<' expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::LT, $3); }
| expression '>' expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::GT, $3); }
| expression LE_K expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::LE, $3); }
| expression GE_K expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::GE, $3); }
| expression '=' expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::EQ, $3); }
| expression NE_K expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::NE, $3); }
| expression AND_K expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::AND, $3); }
| expression OR_K expression 
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::OR, $3); }
| expression XOR_K expression
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::XOR, $3); }
| expression IN_K expression
    { $$ = new IfBinaryExpression($1, IfBinaryExpression::IN, $3); }
| expression '?' expression ':' expression 
    { $$ = new IfTernaryExpression(IfTernaryExpression::CONDITIONAL, $1, $3, $5); }
| '(' expression ')' 
    { $$ = $2; }
/* special observer expressions */
| expression INSTATE_K name 
    { $$ = new IfInstateExpression($1, $3); }
| expression INSTANCEOF_K name 
    { $$ = new IfInstanceofExpression($1, $3); }
| expression INSTANCEOF_K '*'
    { $$ = new IfInstanceofExpression($1, NULL); }
| OBSINPUT_K '(' name obs_opt_in ')'
    { $$ = new IfEventObsExpression(IfEventObsExpression::INPUT, $3, $4); }
| OBSOUTPUT_K '(' name  obs_output_opt_ord  obs_output_opt_from  via_opt  to_opt ')'
    { $$ = new IfEventObsExpression(IfEventObsExpression::OUTPUT, $3, $4, $5, $6, $7); }
| OBSFORK_K '(' process_name_opt obs_opt_in ')'
    { $$ = new IfEventObsExpression(IfEventObsExpression::FORK, $3, $4); }
| OBSKILLPID_K '(' expression ')'
    { $$ = new IfEventObsExpression(IfEventObsExpression::KILL, NULL, $3); }
| OBSKILLPROCESS_K '(' name ')'
    { $$ = new IfEventObsExpression(IfEventObsExpression::KILL, $3, NULL); }
;

obs_opt_in : 
  IN_K expression
    { $$ = $2; }
| /* empty */
    { $$ = NULL; }
;

obs_output_opt_ord : 
  '[' expression ']'
    { $$ = $2; }
| /* empty */
    { $$ = NULL; }
;

obs_output_opt_from : 
  FROM_K expression 
    { $$ = $2; }
| /* empty */
    { $$ = NULL; }
;

name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
| _LITERAL_ { $$ = strdup(yytext); }
;

  
expression_opt_list :
  expression_list { $$ = $1; }
| /* empty */ { $$ = new IfList<IfExpression>(1); }
;

expression_list : 
  expression_list ',' expression { $$ = $1; $$->Add($3); }
| expression {  $$ = new IfList<IfExpression>(1); $$->Add($1); }
;


priority_expression :
   expression { $$ = $1;}
;
/*
--------------------------------------------------------------------
Constraint
--------------------------------------------------------------------
*/

constraint : 
  expression '<' expression 
    { $$ = new IfBasicConstraint($1, NULL, IfBasicConstraint::LT, $3); }
| expression LE_K expression 
    { $$ = new IfBasicConstraint($1, NULL, IfBasicConstraint::LE, $3); }
| expression '=' expression 
    { $$ = new IfBasicConstraint($1, NULL, IfBasicConstraint::EQ, $3); }
| expression GE_K expression 
    { $$ = new IfBasicConstraint($1, NULL, IfBasicConstraint::GE, $3); }
| expression '>' expression 
    { $$ = new IfBasicConstraint($1, NULL, IfBasicConstraint::GT, $3); }
| constraint ',' constraint
    { IfComposedConstraint* C = NULL;
      if ($1->IsBasic()) {
	C = new IfComposedConstraint(new IfList<IfBasicConstraint>(1));
	C->GetConstraints()->Add((IfBasicConstraint*) $1);
      }
      else 
	C = (IfComposedConstraint*)$1;
      C->GetConstraints()->Add((IfBasicConstraint*) $3);
      $$ = C;
    }
;

/*
--------------------------------------------------------------------
Resource
--------------------------------------------------------------------
*/

resource_decl :
  RESOURCE_K resource_name ';'
    { $$ = new IfResource($2); }
;

resource_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

resource :
  resource_name 
    { $$ = (IfResource*) Bind($1, 'X'); free($1); 
      if (!$$) yyerror("undefined resource"); } 
;

resource_list :
  resource_list ',' resource { $$ = $1; $$->Add($3); }
| resource { $$ = new IfList<IfResource>; $$->Add($1); }
;

/*
--------------------------------------------------------------------
Signal
--------------------------------------------------------------------
*/

signal_decl :
  SIGNAL_K signal_name '(' type_opt_list ')' ';' 
    { 
	$$ = new IfSignal($4, $2); 
    }
;

signal_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

signal :
  signal_name 
    { $$ = (IfSignal*) Bind($1, 'S'); free($1);
      if (!$$) yyerror("undefined signal"); }
;

signal_list :
  signal_list ',' signal { $$ = $1; $$->Add($3); }
| signal { $$ = new IfList<IfSignal>; $$->Add($1); }
;

/*
--------------------------------------------------------------------
Signalroute
--------------------------------------------------------------------
*/

signalroute_decl :
  SIGNALROUTE_K signalroute_name '(' constant_body ')' option_set
  FROM_K signalroute_endpoint TO_K signalroute_endpoint 
  WITH_K signal_list ';' 
    { $$ = new IfSignalroute($4, $6, $8, $10, $12, $2); 
      for(int i = 0; $6[i]; i++) free($6[i]); free($6); }
;

signalroute_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

signalroute_endpoint :
  ENV_K { $$ = strdup("env"); }
| process_name { $$ = $1; }
;

/*
--------------------------------------------------------------------
Procedure
--------------------------------------------------------------------
*/

procedure_decl :
  PROCEDURE_K procedure_name ';'
  fpar_decl_opt_list
  returns_opt
  code_opt
  ENDPROCEDURE_K ';'
    { $$ = new IfProcedure($4, $5, $6, $2); }
;

procedure_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

procedure :
  procedure_name 
    { $$ = (IfProcedure*) Bind($1, 'M'); free($1); 
      if (!$$) yyerror("undefined procedure"); }
;

returns_opt :
  RETURNS_K type ';' { $$ = $2; }
| /* empty */ { $$ = NULL; }
;

code_opt : 
  _CODE_ { $$ = strdup(yytext); }
| /* empty */ { $$ = NULL; }
;

/*
--------------------------------------------------------------------
Input / Action
--------------------------------------------------------------------
*/

input:
  INPUT_K signal '(' expression_opt_list ')' ';' 
    { $$ = new IfInputAction($2, $4); }
;

match:
  match_input	{ $$ = $1; }
| match_discard	{ $$ = $1; }
| match_output	{ $$ = $1; }
| match_fork	{ $$ = $1; }
| match_kill	{ $$ = $1; }
| match_deliver	{ $$ = $1; }
| match_informal { $$ = $1; }
| match_probability { $$ = $1; }
;

match_input :
  MATCH_K INPUT_K signal '(' expression_opt_list ')' obs_opt_in ';' 
    { $$ = new IfMatchInputAction($3, $5, NULL, $7); }
| MATCH_K INPUT_K '(' expression_opt ')' obs_opt_in ';' 
    { $$ = new IfMatchInputAction(NULL, NULL, $4, $6); }
;

match_discard :
  MATCH_K DISCARD_K signal '(' expression_opt_list ')' obs_opt_in ';' 
    { $$ = new IfMatchInputAction($3, $5, NULL, $7, 1); }
| MATCH_K DISCARD_K '(' expression_opt ')' obs_opt_in ';' 
    { $$ = new IfMatchInputAction(NULL, NULL, $4, $6, 1); }
;

match_output :
  MATCH_K OUTPUT_K signal '(' expression_opt_list ')' obs_output_opt_ord  obs_output_opt_from  via_opt  to_opt ';' 
    { $$ = new IfMatchOutputAction($3, $5, NULL, $7, $8, $9, $10); }
| MATCH_K OUTPUT_K '(' expression_opt ')' obs_output_opt_ord  obs_output_opt_from  via_opt  to_opt ';' 
    { $$ = new IfMatchOutputAction(NULL, NULL, $4, $6, $7, $8, $9); }
;

match_fork :
  MATCH_K FORK_K '(' expression_opt ')' process_name_opt obs_opt_in ';' 
    { $$ = new IfMatchForkAction($6, $4, $7); }
;

match_kill :
  MATCH_K KILL_K '(' expression_opt ')' process_name_opt obs_opt_in ';' 
    { $$ = new IfMatchKillAction($6, $4, $7); }
;

match_deliver :
  MATCH_K DELIVER_K signal '(' expression_opt_list ')' obs_output_opt_from ';'
    { $$ = new IfMatchDeliverAction($3, $5, NULL, $7); }
| MATCH_K DELIVER_K '(' expression_opt ')' obs_output_opt_from ';'
    { $$ = new IfMatchDeliverAction(NULL, NULL, $4, $6); }
;

match_informal :
  MATCH_K INFORMAL_K string obs_opt_in ';'
    { $$ = new IfMatchInformalAction($3, $4); }
;

match_probability :
  MATCH_K PROBABILITY_K '(' expression_opt ')' ';' 
    { $$ = new IfMatchProbabilityAction($4); }
;

process_name_opt : 
    process_name    
	{ $$ = $1; }
|   /* empty */
	{ $$ = NULL; }
;

expression_opt :
    expression
	{ $$ = $1; }
|   /* empty */
	{ $$ = NULL; }
;

acquire:
  ACQUIRE_K resource_list ';'
    { $$ = new IfAcquireAction($2); }
;

/*
--------------------------------------------------------------------
*/

action :
  SKIP_K ';' 
    { $$ = new IfSkipAction(); }
| INFORMAL_K string ';' 
    { $$ = new IfInformalAction($2); }
| TASK_K expression DOTSEQ_K expression ';' 
    { $$ = new IfTaskAction($2, $4); }
| SET_K expression DOTSEQ_K expression ';' 
    { $$ = new IfSetAction($2, $4); }
| RESET_K expression ';' 
    { $$ = new IfResetAction($2); }
| OUTPUT_K signal '(' expression_opt_list ')' via_opt to_opt ';' 
    { $$ = new IfOutputAction($2, $4, $6, $7); }
| RELEASE_K resource_list ';'
    { $$ = new IfReleaseAction($2); }
| FORK_K process_name '(' expression_opt_list ')' ';' 
    { $$ = new IfForkAction(NULL, $2, $4); }
| expression DOTSEQ_K FORK_K process_name '(' expression_opt_list ')' ';' 
    { $$ = new IfForkAction($1, $4, $6); }
| KILL_K expression ';' 
    { $$ = new IfKillAction($2); }
| CALL_K procedure '(' expression_opt_list ')' ';'
    { $$ = new IfCallAction(NULL, $2, $4); }
| expression DOTSEQ_K CALL_K procedure '(' expression_opt_list ')' ';'
  { $$ = new IfCallAction($1, $4, $6); }
| FLUSH_K ';' 
    { $$ = new IfFlushAction(); }
| CUT_K ';' 
    { $$ = new IfCutAction(); }
;

string :
  _STRING_ { $$ = strdup(yytext); }
;

via_opt :
  VIA_K expression { $$ = $2; }
| /* empty */ { $$ = NULL; }
;

to_opt :
  TO_K expression { $$ = $2; }
| /* empty */ { $$ = NULL; }
;

/*
--------------------------------------------------------------------
*/

terminator :
  NEXTSTATE_K state_reference ';' 
    { $$ = new IfNextstateAction($2); }
| STOP_K ';'
    { $$ = new IfStopAction(); }
;

state_reference :
   state_name { $$ = $1; }
|  _XSTATE_ { $$ = strdup(yytext); }
|  '-' { $$ = strdup("-"); }
;

/*
--------------------------------------------------------------------
Statement
--------------------------------------------------------------------
*/

block : 
  block statement 
    { $$ = $1; $$->GetStatements()->Add($2); }
| /* empty */ 
    { $$ = new IfBlockStatement(new IfList<IfBasicStatement>(1)); }
;

statement : 
  action 
    { $$ = new IfActionStatement($1); }
| IF_K expression THEN_K block ENDIF_K 
    { $$ = new IfConditionalStatement($2, $4, NULL); }
| IF_K expression THEN_K block ELSE_K block ENDIF_K 
    { $$ = new IfConditionalStatement($2, $4, $6); }
| WHILE_K expression DO_K block ENDWHILE_K 
    { $$ = new IfLoopStatement($2, $4); }
;

/*
--------------------------------------------------------------------
Transition
--------------------------------------------------------------------
*/

transition :
  probability_opt
  deadline_opt 
  priority_opt
  acquire_opt
  provided_opt
  when_opt 
  input_opt
  block 
  terminator 
   { $$ = new IfTransition($1, $2, $3, $4, $5, $6, $7, $8, $9); }
;

probability_opt :
  PROBABILITY_K constant_value ';' 
    { 
      if( !$2->IsReal() )
        yyerror("real constant expected in probability clause");
      if( ((IfRealConstant*)$2)->GetValue() < 0 || ((IfRealConstant*)$2)->GetValue() > 1 )
        yyerror("real constant within [0,1] expected in probability clause");

      $$ = ((IfRealConstant*)$2)->GetValue();
    }
| /* empty */ 
    { 
      $$ = -1; 
    }
;

deadline_opt :
  DEADLINE_K name ';' 
    { 
      if( PROCESS->IsObserver() )
	$$ = IfTransition::LAZY; 
      else
	$$ = IfTransition::EAGER; 
      if (!strcmp($2, "delayable")) $$ = IfTransition::DELAYABLE;
      if (!strcmp($2, "lazy")) $$ = IfTransition::LAZY; 
      free($2);}
| /* empty */ 
    {
      if( PROCESS->IsObserver() )
	$$ = IfTransition::LAZY; 
      else
	$$ = IfTransition::EAGER; 
    }
;

priority_opt :
  PRIORITY_K integer ';' { $$ = $2; }
| /* empty */ { $$ = -1; }
;

acquire_opt :
  acquire { $$ = $1; }
| /* empty */ { $$ = NULL; }
;

provided_opt :
  PROVIDED_K expression ';' { $$ = $2; }
| /* empty */ { $$ = NULL; }
;

when_opt :
  WHEN_K { STATUS |= WHEN; } constraint ';' { $$ = $3; STATUS &= ~WHEN; }
| /* empty */ { $$ = NULL; }
;

input_opt :
  input 
  { 
    if(PROCESS->IsObserver()) yyerror("input clause not allowed in observer");
    $$ = $1; 
  }
| match 
  { 
    if(!PROCESS->IsObserver()) yyerror("match clause only allowed in observer");
    $$ = $1; 
  }
| /* empty */ { $$ = NULL; }
;

/*
--------------------------------------------------------------------
State
--------------------------------------------------------------------
*/

state_decl :
  STATE_K state_name option_set ';'
  tpc_opt
  save_opt
  transition_state_decl_set
  ENDSTATE_K ';' 
    { IfList<IfTransition>* transitions = new IfList<IfTransition>(1);
      IfList<IfState>* state_decls = new IfList<IfState>(1);
      for(int ii = 0; ii < $7->GetCount(); ii++)
	if(!strcmp($7->GetAt(ii)->GetClass(), "transition"))
	  transitions->Add( (IfTransition*)($7->GetAt(ii)) );
	else
	  state_decls->Add( (IfState*)($7->GetAt(ii)) );
      delete $7;

      $$ = new IfState($5, $6, transitions, state_decls, $3, $2); 
      for(int i = 0; $3[i]; i++) free($3[i]); free($3); }
;

state_name :
  _INTEGER_ { $$ = strdup(yytext); }
| _IDENTIFIER_ { $$ = strdup(yytext); }
| _LITERAL_ { $$ = strdup(yytext); }
;

tpc_opt :
  TPC_K constraint ';' { $$ = $2; }
| /* empty */ { $$ = NULL; }
;

save_opt :
  SAVE_K signal_list ';' { $$ = $2; }
| /* empty */ { $$ = new IfList<IfSignal>; }
;

transition_state_decl_set :
  transition_state_decl_set state_decl { $$ = $1; $$->Add($2); }
| transition_state_decl_set transition { $$ = $1; $$->Add($2); }
| /* empty */ { $$ = new IfList<IfObject>(0); }
;

/*
--------------------------------------------------------------------
Process
--------------------------------------------------------------------
*/

fpar_decl : 
  fpar_mode fpar_name type 
    { $$ = new IfVariable(IfVariable::FPAR, IfVariable::NOSCOPE, $1, 
			  $3, NULL, $2); }
;

fpar_mode : 
  IN_K { $$ = IfVariable::IN; }
| INOUT_K { $$ = IfVariable::INOUT; }
| OUT_K { $$ = IfVariable::OUT; }
| /* empty */ { $$ = IfVariable::NOMODE; }
;

fpar_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

fpar_decl_opt_list :
  fpar_decl_list ';' { $$ = $1; } 
| /* empty */ { $$ = new IfList<IfVariable>(1); }
;

fpar_decl_list :
  fpar_decl_list ',' fpar_decl { $$ = $1; $$->Add($3); } 
| FPAR_K fpar_decl { $$ = new IfList<IfVariable>(1); $$->Add($2); }
;

/*
--------------------------------------------------------------------
*/

process_decl :
  PROCESS_K process_name '(' constant_body ')' ';'
  fpar_decl_opt_list 
  { PROCESS = 
      new IfProcessEntity($4,
			  $7,
			  new IfList<IfState>(1),
			  new IfList<IfConstant>(1), 
			  new IfList<IfType>(1),
			  new IfList<IfVariable>(1),
			  new IfList<IfProcedure>(1),
			  $2); }
  process_component_set
  ENDPROCESS_K ';' { $$ = PROCESS; PROCESS = NULL; }
;

observer_decl :
  obs_kind OBSERVER_K process_name ';'
  { PROCESS = 
      new IfObserverEntity((IfObserverEntity::EObsKind)$1,
			  new IfList<IfState>(1),
			  new IfList<IfConstant>(1), 
			  new IfList<IfType>(1),
			  new IfList<IfVariable>(1),
			  new IfList<IfProcedure>(1),
			  $3);
  }
  process_component_set
  ENDOBSERVER_K ';' 
	  { 
	    $$ = PROCESS; 
	    PROCESS = NULL;
	  }
;

obs_kind : 
    PURE_K
	{ $$ = IfObserverEntity::PURE; }
|   CUT_K
	{ $$ = IfObserverEntity::CUT; }
|   INTRUSIVE_K
	{ $$ = IfObserverEntity::INTRUSIVE; }
;
priority_decl :
  priority_rule_id ':' process_name '<' process_name
     {
         PRIORITYRULE = 
	    new IfPriorityRuleEntity(
	                              new IfVariable(IfVariable::FIELD, 
	                                            IfVariable::PRIVATE, 
						    IfVariable::NOMODE,
                                                    IfObject::TYPES[IfBasicType::PID],
						    NULL,
						    $3),
	                              new IfVariable(IfVariable::FIELD, 
	                                            IfVariable::PRIVATE, 
						    IfVariable::NOMODE,
						    IfObject::TYPES[IfBasicType::PID],
						    NULL,
						    $5),
						    NULL,
						    $1,
						    0
                                    );

     }
  IF_K priority_expression ';' { 
  PRIORITYRULE->SetExpression($8); $$ = PRIORITYRULE; PRIORITYRULE = NULL; }
;

priority_rule_id :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;
    
process_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

process_component_set :
  process_component_set process_component { }
| /* empty */ { }
;

process_component :
  constant_decl { PROCESS->GetConstants()->Add($1); }
| type_decl { PROCESS->GetTypes()->Add($1); }
| variable_decl { PROCESS->GetVariables()->Add($1); }
| procedure_decl { PROCESS->GetProcedures()->Add($1); }
| state_decl { PROCESS->GetStates()->Add($1); }
;

/*
--------------------------------------------------------------------
System
--------------------------------------------------------------------
*/

system_decl :
  SYSTEM_K system_name ';'
  { 
     SYSTEM = 
      new IfSystemEntity(new IfList<IfProcessEntity>(1),
                         new IfList<IfPriorityRuleEntity>(1),
			 new IfList<IfObserverEntity>(1),
			 new IfList<IfSignalroute>(1),
			 new IfList<IfSignal>(1),
			 new IfList<IfResource>(1),
			 new IfList<IfConstant>(1), 
			 new IfList<IfType>(1),
			 new IfList<IfVariable>(1),
			 new IfList<IfProcedure>(1),
			 $2); 
  }
  system_component_set 
  ENDSYSTEM_K ';' 
  {
    SYSTEM->CreateSigUnionType();
  }
  observer_or_priority_decl_set
  { }
;

system_name :
  _IDENTIFIER_ { $$ = strdup(yytext); }
;

system_component_set :
  system_component_set system_component { }
| /* empty */ { }
;

system_component :
  constant_decl { SYSTEM->GetConstants()->Add($1); }
| type_decl { SYSTEM->GetTypes()->Add($1); }
| variable_decl { SYSTEM->GetVariables()->Add($1); }
| procedure_decl { SYSTEM->GetProcedures()->Add($1); }
| resource_decl { SYSTEM->GetResources()->Add($1); }
| signal_decl { SYSTEM->GetSignals()->Add($1); }
| signalroute_decl { SYSTEM->GetSignalroutes()->Add($1); }
| process_decl { SYSTEM->GetProcesses()->Add((IfProcessEntity*)$1); }
;

observer_or_priority_decl_set : 
  observer_or_priority_decl_set observer_decl { SYSTEM->GetObservers()->Add((IfObserverEntity*)$2); }
|
  observer_or_priority_decl_set priority_section { }
| /* empty */ { }
;

priority_section :
  PRIORITY_SECTION_K 
  priority_decl_set   
  END_PRIORITY_SECTION_K ';'  { }
;
priority_decl_set :
  priority_decl_set priority_decl 
    { 
	SYSTEM->GetPriorityRules()->Add((IfPriorityRuleEntity*)$2);
    }
  | /*empty*/ { }
;  


/*
--------------------------------------------------------------------
Misc
--------------------------------------------------------------------
*/

integer : 
  _INTEGER_ { $$ = atoi(yytext); }
;

real :
  _REAL_ { $$ = atof(yytext); }
;

option_set : 
  option_set option { int i = 0; while ($1[i]) i++; $1[i] = $2; $$ = $1; }
| /* empty */ { $$ = (char**) calloc(16, sizeof(char*)); }
;

option : 
  _OPTION_ { $$ = strdup(yytext); }
;

%%

IfSystemEntity* Load(FILE* file, FILE** p_pfObservers, int p_nObservers) {
    int i;
    yyin = file;

    nObservers = p_nObservers;
    for(i = 0; i < nObservers; i++) {
	hbObservers[i] = yy_create_buffer( p_pfObservers[i], YY_BUF_SIZE);
    }
    yyparse();
    return SYSTEM;
}
