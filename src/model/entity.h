/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * abstract entity interface
 *
 */

class IfEntity : public IfObject {

 public:
  IfEntity(IfList<IfConstant>* Constants, 
           IfList<IfType>* Types, 
           IfList<IfVariable>* Variables,
           IfList<IfProcedure>* Procedures,
           char* Name,
           unsigned Flags = 0);
  virtual ~IfEntity();

 public:
  inline IfList<IfConstant>* GetConstants() const { return m_pConstants; }
  inline IfList<IfType>* GetTypes() const { return m_pTypes; }
  inline IfList<IfVariable>* GetVariables() const { return m_pVariables; }
  inline IfList<IfProcedure>* GetProcedures() const { return m_pProcedures; }

 public:
  virtual const char* GetClass() const 
    { return "entity"; }
  virtual int IsEntity() const 
    { return 1; }

  virtual int IsSystem() const { return 0; }
  virtual int IsProcess() const { return 0; }
  virtual int IsObserver() const { return 0; }
  virtual int IsPriority() const { return 0; }

  virtual void PreCompile();
  virtual void Code(FILE* file) const;
 
 protected:
  IfList<IfConstant>* m_pConstants;
  IfList<IfType>* m_pTypes;
  IfList<IfVariable>* m_pVariables;
  IfList<IfProcedure>* m_pProcedures;

};

/*
 *
 * system entity interface
 *
 */

class IfSystemEntity : public IfEntity {

 public:
  IfSystemEntity(IfList<IfProcessEntity>* Processes, 
                 IfList<IfPriorityRuleEntity>* PriorityRules,
                 IfList<IfObserverEntity>* Observers,    
                 IfList<IfSignalroute>* Signalroutes,
                 IfList<IfSignal>* Signals,
                 IfList<IfResource>* Resources,
                 IfList<IfConstant>* Constants, 
                 IfList<IfType>* Types, 
                 IfList<IfVariable>* Variables,
                 IfList<IfProcedure>* Procedures,
                 char* Name,
                 unsigned Flags = 0);
  virtual ~IfSystemEntity();
 
 public:
  inline IfList<IfProcessEntity>* GetProcesses() const { return m_pProcesses; }
  inline IfList<IfPriorityRuleEntity>*  GetPriorityRules() const { return m_pPriorityRules; }
  inline IfList<IfObserverEntity>* GetObservers() const { return m_pObservers; }
  inline IfList<IfSignalroute>* GetSignalroutes() const { return m_pSignalroutes; }
  inline IfList<IfSignal>* GetSignals() const { return m_pSignals; }
  inline IfList<IfResource>* GetResources() const { return m_pResources; }

 public:
  virtual int IsSystem() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

  virtual void CreateSigUnionType();

 protected:
  IfList<IfProcessEntity>* m_pProcesses;
  IfList<IfPriorityRuleEntity>* m_pPriorityRules;
  IfList<IfObserverEntity>* m_pObservers;
  IfList<IfSignalroute>* m_pSignalroutes;
  IfList<IfSignal>* m_pSignals;
  IfList<IfResource>* m_pResources;

};

/*
 *
 * automaton entity interface : abstract base for process and observer
 *
 */

class IfAutomatonEntity : public IfEntity {

 public:
  IfAutomatonEntity(IfList<IfState>* States,
                  IfList<IfConstant>* Constants, 
                  IfList<IfType>* Types, 
                  IfList<IfVariable>* Variables,
                  IfList<IfProcedure>* Procedures,
                  char* Name,
                  unsigned Flags = 0);
  virtual ~IfAutomatonEntity();

 public:
  inline IfList<IfState>* GetStates() const { return m_pStates; }
  inline IfState* GetEntry() const { return m_pEntry; } 
  inline IfList<IfState>* GetControl() const { return m_pControl; }
  inline IfSystemEntity* GetSystem() const { return m_pSystem; }
  
  
 protected:
  IfList<IfState>* m_pStates;

 protected:
  IfState* m_pEntry;
  IfList<IfState>* m_pControl;
  IfSystemEntity* m_pSystem; 

};


/*
 *
 * process entity interface
 *
 */

class IfProcessEntity : public IfAutomatonEntity {

 public:
  IfProcessEntity(IfConstant* Instances,
                  IfList<IfVariable>* Parameters,
                  IfList<IfState>* States,
                  IfList<IfConstant>* Constants, 
                  IfList<IfType>* Types, 
                  IfList<IfVariable>* Variables,
                  IfList<IfProcedure>* Procedures,
                  char* Name,
                  unsigned Flags = 0);
  virtual ~IfProcessEntity();

 public:
  inline IfConstant* GetInstances() const { return m_pInstances; }
  inline IfList<IfVariable>* GetParameters() const { return m_pParameters; }
  
  inline IfList<IfSignalroute>* GetInRoutes() const { return m_pInRoutes; }
  inline IfList<IfSignalroute>* GetOutRoutes() const { return m_pOutRoutes; }
  inline IfList<IfSignal>* GetInSignals() const { return m_pInSignals; }
  inline IfList<IfSignal>* GetOutSignals() const { return m_pOutSignals; }
  inline IfList<IfSignal>* GetEnvSignals() const { return m_pEnvSignals; }

  //inline IfState* GetEntry() const { return m_pEntry; } 
  //inline IfSystemEntity* GetSystem() const { return m_pSystem; }
  
  virtual void Link(IfSignalroute* route);

 public:
  virtual int IsProcess() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 protected:
  IfConstant* m_pInstances; 
  IfList<IfVariable>* m_pParameters;

 protected:
  //IfState* m_pEntry;
  IfList<IfSignalroute>* m_pInRoutes;
  IfList<IfSignalroute>* m_pOutRoutes;
  IfList<IfSignal>* m_pInSignals;
  IfList<IfSignal>* m_pOutSignals;
  IfList<IfSignal>* m_pEnvSignals;

};

/*
 *
 * observer entity interface
 *
 */

class IfObserverEntity : public IfAutomatonEntity {

 public:
  enum EObsKind { 
    PURE = 1,
    CUT = 2,
    INTRUSIVE = 3,
  };

 public:
  IfObserverEntity(EObsKind kind,
                  IfList<IfState>* States,
                  IfList<IfConstant>* Constants, 
                  IfList<IfType>* Types, 
                  IfList<IfVariable>* Variables,
                  IfList<IfProcedure>* Procedures,
                  char* Name,
                  unsigned Flags = 0);
  virtual ~IfObserverEntity();

 public:
  virtual int IsObserver() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

  virtual EObsKind getKind() const { return m_kind; }

 protected:
  EObsKind m_kind;

};
