/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * abstract object interface
 *
 */

class IfObject {
 
 public:
  static void Initialize();

  static IfList<IfBasicType> TYPES;

 public:
  IfObject(char* Name = NULL, 
           unsigned Flags = 0);
  virtual ~IfObject();

 public:
  inline char* GetName() const 
    { return m_pName; }
  inline int GetLinenum() const 
    { return m_uLinenum; }
  inline int GetFlag(const unsigned Flag) const 
    { return (m_uFlags & Flag) ? 1 : 0; }
  inline int GetIndex() const 
    { return m_iIndex; }
  inline void* GetData() const 
    { return m_pData; }

 public:  
  inline void SetName(char* Name) 
    { m_pName = Name; }
  inline void SetLinenum(const int Linenum) 
    { m_uLinenum = Linenum; }
  inline void SetFlag(const unsigned Flag, 
                      const int Value) 
    { if (Value) m_uFlags |= Flag; else m_uFlags &= ~Flag; }
  virtual void SetIndex(int* Index) 
    { m_iIndex = (*Index)++; }
  virtual void SetData(void* Data) 
    { m_pData = Data; }
  
 public:
  virtual const char* GetClass() const 
    { return "object"; }
  virtual int IsExpression() const
    { return 0; }
  virtual int IsConstraint() const 
    { return 0; }
  virtual int IsAction() const
    { return 0; }
  virtual int IsState() const 
    { return 0; }
  virtual int IsEntity() const
    { return 0; }

 public:
  virtual void Dump(FILE* file) const;
 protected:
  static unsigned DEPTH;

 public:
  enum CFlag { COMPILED = 2, CORRECT = 4, TIMED = 8 };

  virtual void Protest(const char* Message) const;
  virtual int Compile();
  virtual void PreCompile();
  virtual void PostCompile();
 protected:
  static IfList<IfObject> CONTEXT;

 public:
  enum TFlag { XSCOPE = 2 };
  static unsigned TRANSFORM;
  
 public:
  virtual void Code(FILE* file) const;
  virtual const char* GetNameIndexed() const;

 protected:
  char* m_pName;
  unsigned m_uFlags;
  unsigned m_uLinenum;
  int m_iIndex;

  void* m_pData; // user data

};
