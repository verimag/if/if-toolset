/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */
/*
 *
 * signalroute.h 1.5 [06/17/01]
 *
 */

/*
 *
 * signalroute interface
 *
 */

class IfSignalroute : public IfObject {

 public: 
  enum {
    FIFO        = 0x00000001,               // queueing policies
    MULTISET    = 0x00000002,

    RELIABLE    = 0x00000100,                     // reliability 
    LOSSY       = 0x00000200,

    PEER        = 0x00010000,            // dispacthing policies
    MULTICAST   = 0x00020000,
    UNICAST     = 0x00040000,

    URGENT      = 0x01000000,                  // delaying modes
    DELAY       = 0x02000000,
    RATE        = 0x04000000
  };

 public:
  IfSignalroute(IfConstant* Instances,
                char* Option[], 
                char* FromName, 
                char* ToName, 
                IfList<IfSignal>* Signals, 
                char* Name, 
                unsigned Flags = 0);
  virtual ~IfSignalroute();

 public:
  inline IfConstant* GetInstances() const { return m_pInstances; }
  inline IfProcessEntity* GetFrom() const { return m_pFrom; }
  inline IfProcessEntity* GetTo() const { return m_pTo; }
  inline IfList<IfSignal>* GetSignals() const { return m_pSignals; }
  inline int Is(const unsigned option) const { return m_uOptions & option; }
  inline int GetLower() const { return m_iLower; }
  inline int GetUpper() const { return m_iUpper; }

 public:
  virtual const char* GetClass() const { return "signalroute"; }

  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 protected:
  IfConstant* m_pInstances;
  unsigned m_uOptions;
  IfProcessEntity* m_pFrom;
  IfProcessEntity* m_pTo;
  IfList<IfSignal>* m_pSignals;
  int m_iLower;
  int m_iUpper;
  double m_fProba;

  char* m_pFromName;
  char* m_pToName;

};
