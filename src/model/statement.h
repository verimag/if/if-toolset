/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * abstract statement interface
 *
 */

class IfStatement : public IfObject {

 public:
  IfStatement(unsigned Flags = 0);
  virtual ~IfStatement();
  
 public:
  virtual const char* GetClass() const { return "statement"; }
  virtual void PreCompile();
  virtual void SetIndex(int* Index);
  virtual void Alpha(FILE* file) const;

 public:
  virtual int Ask(const IfVariable* Variable) const;
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;
  
 public:
  virtual int IsBlock() const { return 0; }
  virtual int IsBasic() const { return 0; }
  
 protected:
  IfTransition* m_pTransition;
  
};

/*
 * 
 * block statement interface
 *
 */

class IfBlockStatement : public IfStatement {
  
 public:
  IfBlockStatement(IfList<IfBasicStatement>* Statements,
                   unsigned Flags = 0);
  virtual ~IfBlockStatement();
  
 public:
  inline IfList<IfBasicStatement>* GetStatements() const 
    { return m_pStatements; }

 public:
  virtual int IsBlock() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void SetIndex(int* Index);
  virtual void Alpha(FILE* file) const;
  virtual void Code(FILE* file) const;

 public:
  virtual int Ask(const IfVariable* Variable) const;
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;
  
 protected:
  IfList<IfBasicStatement>* m_pStatements;
 
};

/*
 *
 * basic statement interface
 *
 */

class IfBasicStatement : public IfStatement {

 public:
  IfBasicStatement(unsigned Flags = 0);
  virtual ~IfBasicStatement();

 public:
  virtual int IsBasic() const { return 1; }

  virtual int IsAction() const { return 0; }
  virtual int IsConditional() const { return 0; }
  virtual int IsLoop() const { return 0; }

  virtual void PreCompile(); 
  
};

/*
 *
 * basic action statement interface
 *
 */

class IfActionStatement : public IfBasicStatement {

 public:
  IfActionStatement(IfAction* Action, 
                   unsigned Flags = 0);
  virtual ~IfActionStatement();

 public:
  inline IfAction* GetAction() const 
    { return m_pAction; }
  inline void SetAction(IfAction* Action)
    { m_pAction = Action; }

 public:
  virtual int IsAction() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void SetIndex(int* Index);  
  virtual void Alpha(FILE* file) const;
  virtual void Code(FILE* file) const;

 public:
  virtual int Ask(const IfVariable* Variable) const;
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfAction* m_pAction;

};

/*
 *
 * basic conditional statement interface
 *
 */

class IfConditionalStatement : public IfBasicStatement {

 public:
  IfConditionalStatement(IfExpression* Test,
                         IfBlockStatement* Then,
                         IfBlockStatement* Else,
                         unsigned Flags = 0);
  virtual ~IfConditionalStatement();

 public:
  inline IfExpression* GetTest() const 
    { return m_pTest; }
  inline IfBlockStatement* GetThen() const 
    { return m_pThen; }
  inline IfBlockStatement* GetElse() const 
    { return m_pElse; }

  inline void SetTest(IfExpression* Test)
    { m_pTest = Test; }
  inline void SetThen(IfBlockStatement* Then)
    { m_pThen = Then; }
  inline void SetElse(IfBlockStatement* Else) 
    { m_pElse = Else; }

 public:
  virtual int IsConditional() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void SetIndex(int* Index);  
  virtual void Alpha(FILE* file) const;
  virtual void Code(FILE* file) const;

 public:
  virtual int Ask(const IfVariable* Variable) const;
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfExpression* m_pTest;
  IfBlockStatement* m_pThen;
  IfBlockStatement* m_pElse;

};

/*
 *
 * basic loop statement interface
 *
 */

class IfLoopStatement : public IfBasicStatement {

 public:
  IfLoopStatement(IfExpression* Test,
                  IfBlockStatement* Body,
                  unsigned Flags = 0);
  virtual ~IfLoopStatement();

 public:
  inline IfExpression* GetTest() const 
    { return m_pTest; }
  inline IfBlockStatement* GetBody() const 
    { return m_pBody; }
  inline void SetTest(IfExpression* Test)
    { m_pTest = Test; }
  inline void SetBody(IfBlockStatement* Body) 
    { m_pBody = Body; }

 public:
  virtual int IsLoop() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void SetIndex(int* Index);
  virtual void Alpha(FILE* file) const;
  virtual void Code(FILE* file) const;
  
 public:
  virtual int Ask(const IfVariable* Variable) const;
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfExpression* m_pTest;
  IfBlockStatement* m_pBody;

};
