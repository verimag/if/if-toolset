/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * abstract expression interface
 *
 */

class IfExpression : public IfObject {

 public:
  IfExpression(unsigned Flags = 0);
  virtual ~IfExpression() {}

 public:
  inline int IsTimed() const
    { return GetFlag(TIMED); }
  inline IfType* GetType() const 
    { return m_pType; }  

 public:
  virtual int IsReference() const { return 0; }
  virtual int Match(const IfType*);

 public:
  virtual const char* GetClass() const 
    { return "expression"; } 
  virtual int IsExpression() const
    { return 1; }

  virtual int IsVoid() const { return 0; }
  virtual int IsConstant() const { return 0; }
  virtual int IsVariable() const { return 0; }
  virtual int IsCall() const { return 0; }
  virtual int IsField() const { return 0; }
  virtual int IsInstate() const { return 0; }
  virtual int IsInstanceof() const { return 0; }
  virtual int IsEventObs() const { return 0; }
  virtual int IsIndex() const { return 0; }
  virtual int IsUnary() const { return 0; }
  virtual int IsBinary() const { return 0; }
  virtual int IsTernary() const { return 0; }
  virtual int IsCast() const { return 0; }

  virtual void PostCompile();

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 public:
  struct Operator {
    const char* name;
    const char* code;
    short priority;
    short arity;
  };

 protected:
  IfType* m_pType;

};

/*
 *
 * void expression
 *
 */

class IfVoidExpression : public IfExpression {

 public:
  IfVoidExpression(unsigned Flags = 0);
  virtual ~IfVoidExpression() {}

 public:
  virtual int IsReference() const { return 1; }
  virtual int Match(const IfType*);

 public:
  virtual int IsVoid() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PostCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;

};

/*
 *
 * constant expression
 *
 */

class IfConstantExpression : public IfExpression {

 public:
  IfConstantExpression(IfConstant* Constant,
                       unsigned Flags = 0);
  virtual ~IfConstantExpression();

 public:
  inline IfConstant* GetConstant() const { return m_pConstant; }
  inline void SetConstant(const IfConstant* Constant) 
    { m_pConstant = (IfConstant*) Constant; }

 public:
  virtual int IsConstant() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;

 protected:
  IfConstant* m_pConstant;

};

/*
 *
 * variable expression
 *
 */

class IfVariableExpression : public IfExpression {

 public:
  IfVariableExpression(IfVariable* Variable,
                       unsigned Flags = 0);
  virtual ~IfVariableExpression() {}

 public:
  inline IfVariable* GetVariable() const { return m_pVariable; }
  inline void SetVariable(const IfVariable* Variable) 
    { m_pVariable = (IfVariable*) Variable; }
  
 public:
  virtual int IsReference() const { return 1; }

 public:
  virtual int IsVariable() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfVariable* m_pVariable;

};

/*
 *
 * call expression
 *
 */

class IfCallExpression : public IfExpression {

 public:
  enum EOperator {
    UNKNOWN = 0,
    INSERT, REMOVE, INDEX,
    OBS_QLENGTH, OBS_QGETFIRST, OBS_QGETLAST, OBS_QGETAT,
    OBS_QREMOVEFIRST, OBS_QREMOVELAST, OBS_QREMOVEAT,
    CALL_END_PREDEF, /* unused */
    CONSTRUCTOR,
    FUNCTION
  };

 public:
  IfCallExpression(char* Name,
                   IfList<IfExpression>* Parameters,
                   unsigned Flags = 0);
  virtual ~IfCallExpression();
  
 public:
  inline int IsPredef() const 
    { return INSERT <= m_eOperator && m_eOperator <= CALL_END_PREDEF; }
  inline int IsConstructor() const 
    { return CONSTRUCTOR == m_eOperator; }
  inline int IsFunction() const 
    { return FUNCTION == m_eOperator; }
  inline EOperator GetOperator() const 
    { return m_eOperator; }
  inline IfFunction* GetFunction() const 
    { return m_pFunction; }
  inline IfList<IfExpression>* GetParameters() const
    { return m_pParameters; }

 public:
  virtual int IsCall() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  
 protected:
  int Fulfil(const IfType* Type);
  int Fulfil(const IfFunction* Function);
  
 public:
  static Operator OPERATOR[];
  
 protected:
  EOperator m_eOperator;
  IfList<IfExpression>* m_pParameters;
  IfFunction* m_pFunction;
  
  char* m_pName;
  
};

/*
 *
 * field expression
 *
 */

class IfFieldExpression : public IfExpression {

 public:
  enum EBaseKind {RECORD, PROCESS, NONE};
  
 public:
  IfFieldExpression(IfExpression* Base,
                    IfVariable* Field,
                    unsigned Flags = 0);
  IfFieldExpression(IfExpression* Base,
                    char* FieldName,
                    unsigned Flags = 0);
  virtual ~IfFieldExpression();

 public:
  inline IfExpression* GetBase() const { return m_pBase; }
  inline IfVariable* GetField() const { return  m_pField; }

 public:
  virtual int IsReference() const 
    { return m_eKind == PROCESS || m_pBase->IsReference(); }
  
 public:
  virtual int IsField() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  EBaseKind m_eKind;
  IfExpression* m_pBase;
  IfVariable* m_pField;
  char* m_pFieldName;
};

/*
 *
 * instate expression
 *
 */

class IfInstateExpression : public IfExpression {

 public:
  IfInstateExpression(IfExpression* Process,
                    char* StateName,
                    unsigned Flags = 0);
  virtual ~IfInstateExpression();

 public:
  inline IfExpression* GetProcess() const { return m_pProcess; }
  inline char* GetStateName() const { return  m_pStateName; }

 public:
  virtual int  IsInstate() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;
 protected:
  virtual void _Code(FILE* file, char* procName, IfExpression* process, IfState* rootState) const;

 protected:
  IfExpression* m_pProcess;
  IfState *m_pState;
  char* m_pStateName;
};

/*
 *
 * instanceof expression
 *
 */

class IfInstanceofExpression : public IfExpression {

 public:
  IfInstanceofExpression(IfExpression* term,
                    char* name,
                    unsigned Flags = 0);
  virtual ~IfInstanceofExpression();

 public:
  inline IfExpression* GetTerm() const { return m_pTerm; }
  inline char* GetName() const { return  m_pName; }

 public:
  virtual int  IsInstanceof() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 protected:
  IfExpression* m_pTerm;
  IfSignal *m_pSig;
  IfProcessEntity *m_pProc;
  char* m_pName;
};

/*
 *
 * eventobs expression
 *
 */

class IfEventObsExpression : public IfExpression {

 public:
  enum EEvtKind {INPUT, DISCARD, OUTPUT, FORK, KILL, DELIVER, INFORMAL, PROBABILITY};

 public:
  IfEventObsExpression(EEvtKind k,
                    char* name = NULL,
                    IfExpression* term1 = NULL, // INPUT:in pid,  OUTPUT: ord number, FORK: in pid, KILL: killed pid, DELIVER: from pid, INFORMAL: in pid
                    IfExpression* term2 = NULL, // OUTPUT: from pid, KILL: in pid, 
                    IfExpression* term3 = NULL, // OUTPUT: via pid
                    IfExpression* term4 = NULL, // OUTPUT: to pid
                    unsigned Flags = 0);
  virtual ~IfEventObsExpression();

 public:
  virtual int  IsEventObs() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 protected:
  EEvtKind m_kind;
  IfExpression* m_pTerm1;
  IfExpression* m_pTerm2;
  IfExpression* m_pTerm3;
  IfExpression* m_pTerm4;
  IfSignal *m_pSig;
  IfProcessEntity *m_pProcess;
  char* m_pName;
};

/*
 *
 * index expression
 *
 */

class IfIndexExpression : public IfExpression {

 public:
  IfIndexExpression(IfExpression* Base,
                    IfExpression* Index,
                    unsigned Flags = 0);
  virtual ~IfIndexExpression();

 public:
  inline IfExpression* GetBase() const { return m_pBase; }
  inline IfExpression* GetIndex() const { return m_pIndex; }

 public:
  virtual int IsReference() const 
    { return m_pBase->IsReference(); }

 public:
  virtual int IsIndex() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfExpression* m_pBase;
  IfExpression* m_pIndex;

};

/*
 *
 * unary expression
 *
 */

class IfUnaryExpression : public IfExpression {
  
 public:
  enum EOperator { 
    ACTIVE = 0, TIMEVAL,
    LENGTH,
    PLUS, MINUS, 
    NOT
  };

 public:
  IfUnaryExpression(EOperator Operator,
                    IfExpression* Right,
                    unsigned Flags = 0);
  virtual ~IfUnaryExpression();

 public:
  inline EOperator GetOperator() const { return m_eOperator; }
  inline IfExpression* GetRight() const { return m_pRight; }
  inline void SetOperator(const EOperator Operator) { m_eOperator = Operator; }

 public:
  virtual int IsUnary() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;

 public:
  static Operator OPERATOR[];
  
 protected:
  EOperator m_eOperator;
  IfExpression* m_pRight;

  
};

/*
 *
 * binary expression
 *
 */

class IfBinaryExpression : public IfExpression {
  
 public:
  enum EOperator { 
    MULT = 0, DIV, MOD, 
    PLUS, MINUS, 
    CONCAT,
    LT, LE, GE, GT, 
    EQ, NE, 
    AND, 
    OR, XOR,
    IN
  };

 public:
  IfBinaryExpression(IfExpression* Left,
                     EOperator Operator,
                     IfExpression* Right,
                     unsigned Flags = 0);
  virtual ~IfBinaryExpression();
  
 public:
  inline EOperator GetOperator() const { return m_eOperator; }
  inline IfExpression* GetLeft() const { return m_pLeft; }
  inline IfExpression* GetRight() const { return m_pRight; }
  inline void SetOperator(const EOperator Operator) { m_eOperator = Operator; } 
  
 public:
  virtual int IsBinary() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;

 public:
  static Operator OPERATOR[];
  
 protected:
  EOperator m_eOperator;
  IfExpression* m_pLeft;
  IfExpression* m_pRight;
  
};

/*
 *
 * conditional expression
 *
 */

class IfTernaryExpression : public IfExpression {
 public:
  enum EOperator {
    CONDITIONAL = 0,
    SUBSTRING
  };
  
 public:
  IfTernaryExpression(EOperator Operator,
                      IfExpression* Top,
                      IfExpression* Left,
                      IfExpression* Right,
                      unsigned Flags = 0);
  virtual ~IfTernaryExpression();

 public:
  inline EOperator GetOperator() const { return m_eOperator; }
  inline IfExpression* GetTop() const { return m_pTop; }
  inline IfExpression* GetLeft() const { return m_pLeft; }
  inline IfExpression* GetRight() const { return m_pRight; }
  
 public:
  virtual int IsTernary() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;
  
 public:
  virtual int Use(const IfVariable* Variable) const;

 protected:
  EOperator m_eOperator;
  IfExpression* m_pTop;
  IfExpression* m_pLeft;
  IfExpression* m_pRight;

};

/*
 *
 * cast expression
 *
 */

class IfCastExpression : public IfExpression {

 public:
  enum EKind {
    TYPE = 0, 
    PROCESS, 
    ROUTE
  };

 public:
  IfCastExpression(EKind Kind,
                   IfObject* Cast, 
                   IfExpression* Right);
  IfCastExpression(EKind Kind,
                   char* Name, 
                   IfExpression* Right);
  virtual ~IfCastExpression();

 public:
  inline EKind GetKind() const { return m_eKind; }
  inline IfExpression* GetRight() const { return m_pRight; }
  inline IfObject* GetCast() const { return m_pCast; }

 public:
  inline int IsType() const { return m_eKind == TYPE; }
  inline int IsProcess() const { return m_eKind == PROCESS; }
  inline int IsRoute() const { return m_eKind == ROUTE; }

 public:
  virtual int IsCast() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;

 protected:
  EKind m_eKind;
  IfExpression* m_pRight;
  IfObject* m_pCast;
  char* m_pName;

};

