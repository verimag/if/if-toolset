/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * generic list interface
 *
 */

template <class C> class IfList {

 public:
  IfList(int Owner = 0, 
         int Growth = 4);
  ~IfList();

 public:
  inline int GetGrowth() const { return m_iGrowth; }
  inline int IsOwner() const { return m_iOwner; }
  inline int GetSize() const { return m_iSize; }
  inline int GetCount() const { return m_iCount; }
  inline C* GetAt(int i) const { return m_pObjects[i]; }
  inline void SetAt(int i, C* Object) { m_pObjects[i] = Object; }
  inline C* operator[](int i) const { return m_pObjects[i]; }

 public:
  C* Find(const char* Name) const;
  C* Find(const C* Object) const;
  int Position(const C* Object) const;
  void Add(const C* Object);
  void Add(const IfList<C>* List);
  void Remove(const C* Object);

 public:
  void Push(const C* Object);
  C* Top(int i = 0) const;
  void Pop();

 public:
  void DumpName(FILE* file, 
            const char* Begin, 
            const char* Separator, 
            const char* End) const;
  void Dump(FILE* file,
            const char* Begin, 
            const char* Separator, 
            const char* End) const;

  void Code(FILE*, 
            const char* Begin, 
            const char* Separator, 
            const char* End) const;

  int GetFlag(const unsigned Flag) const;
  void SetIndex(int* Index) const;
  int Compile() const;
  int UniqueNames() const;
  int DisjointNames(const IfList<C>* List) const;

 protected:
  int m_iGrowth;
  int m_iOwner;
  int m_iSize;
  int m_iCount;
  C** m_pObjects;

};
