/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */


%{ // if.lex.l

#ifdef win32native
#include <io.h>
#endif

#define YY_NO_UNISTD_H
#define isatty(X) 0

#define MAXOBSEVERS 16
YY_BUFFER_STATE hbObservers[MAXOBSEVERS]; 
YY_BUFFER_STATE hbModel;
int nModelLine = 0;
int nObservers;
int nCurrentObserver = 0;

#include "if.yacc.H"

%}

%option yylineno

SPACE           [ \t\n\r]
BLANC           {SPACE}+

STAR            ("*")+
COMMENT1        "/*"([^*]|{STAR}[^*/])*{STAR}"/" 
COMMENT2        "//"([^\n])*

DIGIT           [0-9]
LETTER          [a-zA-Z]
ALPHA           {DIGIT}|{LETTER}|_

INTEGER         {DIGIT}+
REAL            {INTEGER}"."{INTEGER}(e[-+]?{INTEGER})?

IDENTIFIER      {LETTER}{ALPHA}*
LITERAL         {ALPHA}+

XSTATE          {LITERAL}("@"{LITERAL})*

OPTION          #[^ \t\r\n]*

STRING          \"[^\"]*\"

CODE            "{#"([^#]|#[^}])*"#}"


%%

{BLANC}         { /* nothing */ } 
{COMMENT1}      { /* nothing */ }
{COMMENT2}      { /* nothing */ }

abstract        { return ABSTRACT_K; }
active          { return ACTIVE_K; }
acquire         { return ACQUIRE_K; }
and             { return (STATUS & WHEN) ? ',' : AND_K; }
array           { return ARRAY_K; }
call            { return CALL_K; }
fork            { return FORK_K; }
const           { return CONST_K; }
deadline        { return DEADLINE_K; }
discard         { return DISCARD_K; }
do              { return DO_K; }
else            { return ELSE_K; }
endabstract     { return ENDABSTRACT_K; }
endenum         { return ENDENUM_K; }
endif           { return ENDIF_K; }
endprocedure    { return ENDPROCEDURE_K; }
endprocess      { return ENDPROCESS_K; }
endrecord       { return ENDRECORD_K; }
endstate        { return ENDSTATE_K; }
endsystem       { return ENDSYSTEM_K; }
endwhile        { return ENDWHILE_K; }
enum            { return ENUM_K; }
env             { return ENV_K; }
false           { return FALSE_K; }
fpar            { return FPAR_K; }
from            { return FROM_K; }
if              { return IF_K; }
in              { return IN_K; }
informal        { return INFORMAL_K; }
input           { return INPUT_K; }
inout           { return INOUT_K; }
instanceof      { return INSTANCEOF_K; }
instate         { return INSTATE_K; }
kill            { return KILL_K; }
length          { return LENGTH_K; }
nextstate       { return NEXTSTATE_K; }
nil             { return NIL_K; }
not             { return NOT_K; }
of              { return OF_K; }
or              { return OR_K; }
out             { return OUT_K; }
output          { return OUTPUT_K; }
priority        { return PRIORITY_K; }
private         { return PRIVATE_K; }
probability     { return PROBABILITY_K; }
procedure       { return PROCEDURE_K; }
process         { return PROCESS_K; }
provided        { return PROVIDED_K; }
public          { return PUBLIC_K; }
range           { return RANGE_K; }
record          { return RECORD_K; }
release         { return RELEASE_K; }
reset           { return RESET_K; }
resource        { return RESOURCE_K; }
returns         { return RETURNS_K; }
save            { return SAVE_K; }
self            { return SELF_K; }
set             { return SET_K; }
signal          { return SIGNAL_K; }
signalroute     { return SIGNALROUTE_K; }
skip            { return SKIP_K; }
state           { return STATE_K; }
stop            { return STOP_K; }
string          { return STRING_K; }
system          { return SYSTEM_K; }
task            { return TASK_K; }
timeval         { return TIMEVAL_K; }
then            { return THEN_K; }
to              { return TO_K; }
tpc             { return TPC_K; }
tree            { return TREE_K; }
true            { return TRUE_K; }
type            { return TYPE_K; }
var             { return VAR_K; }
via             { return VIA_K; }
void            { return VOID_K; }
with            { return WITH_K; }
when            { return WHEN_K; }
while           { return WHILE_K; }
xor             { return XOR_K; }


endobserver     { return ENDOBSERVER_K; }
flush           { return FLUSH_K; }
cut             { return CUT_K; }
observer        { return OBSERVER_K; }
obsoutput       { return OBSOUTPUT_K; }
obsinput        { return OBSINPUT_K; }
obsfork         { return OBSFORK_K; }
obskillpid      { return OBSKILLPID_K; }
obskillprocess  { return OBSKILLPROCESS_K; }
match		{ return MATCH_K; }
deliver		{ return DELIVER_K; }
pure		{ return PURE_K; }
intrusive	{ return INTRUSIVE_K; }

priorityrules       { return PRIORITY_SECTION_K;}
endpriorityrules    { return END_PRIORITY_SECTION_K;}

{REAL}          { return _REAL_; }
{INTEGER}       { return _INTEGER_; }

{IDENTIFIER}    { return _IDENTIFIER_; }
{LITERAL}       { return _LITERAL_; }
{XSTATE}        { return _XSTATE_; }
{OPTION}        { return _OPTION_; }
{STRING}        { return _STRING_; }
{CODE}          { return _CODE_; }

".."            { return DDOT_K; }
":="            { return DOTSEQ_K; }
"<="            { return LE_K; }
">="            { return GE_K; }
"<>"            { return NE_K; }

"("             { return '('; }
")"             { return ')'; }
"["             { return '['; }
"]"             { return ']'; }
"{"             { return '{'; }
"}"             { return '}'; }
"+"             { return '+'; }
"-"             { return '-'; }
"*"             { return '*'; }
"/"             { return '/'; }
"%"             { return '%'; }
"^"             { return '^'; }
"<"             { return '<'; }
">"             { return '>'; }
"="             { return '='; }
"."             { return '.'; }
";"             { return ';'; }
":"             { return ':'; }
","             { return ','; }
"?"             { return '?'; }

.               { fprintf(stderr, "line %d : lexical error (at or near `%s')\n", 
			  yylineno, yytext); 
                  exit(1); }
                  
<<EOF>>		{ 
		    if(nCurrentObserver < nObservers) {
			yy_switch_to_buffer(hbObservers[nCurrentObserver]);
			nCurrentObserver++;
			yylineno = 1;
		    } else {
			yyterminate();
		    }
		}                  

%%

extern "C" int yywrap(){  /* yywrap implementation */
  return 1; 
}
