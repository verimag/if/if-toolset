/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * implementation of generic lists
 *
 */

template <class C>
C* IfList<C>::Find(const char* Name) const {
  C* object = NULL;
  for(int i = 0; i < GetCount(); i++)
    if (! strcmp(Name, GetAt(i)->GetName())){
      object = GetAt(i);
      break;
    }
  return object;
}

template <class C>
C* IfList<C>::Find(const C* Object) const {
  C* object = NULL;
  for(int i = 0; i < GetCount(); i++)
    if ( GetAt(i) == Object){
      object = (C*) Object;
      break;
    }
  return object;
}

template <class C>
int IfList<C>::Position(const C* Object) const {
  int pos = -1;
  for (int i = 0; i < GetCount(); i++)
    if (GetAt(i) == Object){
      pos = i;
      break;
    }
  return pos;
}

template <class C>
void IfList<C>::Add(const C* Object){
  if (m_iSize == m_iCount){
    m_iSize += m_iGrowth;
    m_pObjects = (C**) realloc(m_pObjects, sizeof(C*) * m_iSize);
  }
  m_pObjects[m_iCount++] = (C*) Object;
}

template <class C>
void IfList<C>::Add(const IfList<C>* Objects){
  if (m_iCount + Objects->GetCount() > m_iSize){
    m_iSize += Objects->GetCount();
    m_pObjects = (C**) realloc(m_pObjects, sizeof(C*) * m_iSize);
  }
  for(int i = 0; i < Objects->GetCount(); i++)
    m_pObjects[m_iCount++] = Objects->GetAt(i);
}

template <class C>
void IfList<C>::Remove(const C* Object){
  int i;
  for(i = 0; i < GetCount(); i++)
    if (GetAt(i) == Object)
      break;
  if (i < GetCount()) {
    if (m_iOwner)
      delete Object;
    for(i++; i < GetCount(); i++)
      m_pObjects[i-1] = m_pObjects[i];
    m_iCount--;
  }
}

template <class C>
void IfList<C>::Push(const C* Object){
  Add(Object);
}

template <class C>
C* IfList<C>::Top(int i) const {
  return m_pObjects[m_iCount-1+i];
}

template <class C>
void IfList<C>::Pop() {
  m_iCount--;
}


template <class C>
void IfList<C>::DumpName(FILE* file, 
                         const char* Begin, 
                         const char* Separator, 
                         const char* End) const {
  if (GetCount())
    fprintf(file, "%s", Begin);
  for(int i = 0; i < GetCount(); i++){
    fprintf(file, "%s", GetAt(i)->GetName());
    fprintf(file, "%s", i == GetCount() - 1 ? End : Separator);
  }
}

template <class C>
void IfList<C>::Dump(FILE* file, 
                     const char* Begin, 
                     const char* Separator, 
                     const char* End) const {
  if (GetCount())
    fprintf(file, "%s", Begin);
  for(int i = 0; i < GetCount(); i++){
    GetAt(i)->Dump(file);
    fprintf(file,"%s", i == GetCount() - 1 ? End : Separator);
  }
}

template <class C>
int IfList<C>::GetFlag(const unsigned Flag) const {
  int flag = 0;
  for(int i = 0; i < GetCount(); i++)
    flag |= GetAt(i)->GetFlag(Flag);
  return flag;
}

template <class C>
void IfList<C>::SetIndex(int* Index) const {
  for(int i = 0; i < GetCount(); i++)
    GetAt(i)->SetIndex(Index);
} 

template <class C>
int IfList<C>::Compile() const {
  int ok = 1;
  for(int i = 0; i < GetCount(); i++)
    ok &= GetAt(i)->Compile();
  return ok;
}

template <class C>
int IfList<C>::UniqueNames() const {
  int ok = 1;
  for(int i = 0; i < GetCount() - 1; i++)
    for(int j = i+1; j < GetCount(); j++)
      if (strcmp(GetAt(i)->GetName(), GetAt(j)->GetName()) == 0) {
	GetAt(i)->Protest("name redefined, 1st occurrence");
	GetAt(j)->Protest("name redefined, 2nd occurrence");
	ok = 0;
      }
  return ok;
}

template <class C>
int IfList<C>::DisjointNames(const IfList<C>* list) const {
  int ok = 1;
  for(int i = 0; i < GetCount(); i++)
    for(int j = 0; j < list->GetCount(); j++)
      if (strcmp(GetAt(i)->GetName(), list->GetAt(j)->GetName()) == 0) {
	GetAt(i)->Protest("name redefined, 1st occurrence");
	list->GetAt(j)->Protest("name redefined, 2nd occurrence");
	ok = 0;
      }
  return ok;
}

template <class C>
void IfList<C>::Code(FILE* file, 
                     const char* Begin, 
                     const char* Separator, 
                     const char* End) const {
  if (GetCount())
    fprintf(file, "%s", Begin);
  for(int i = 0; i < GetCount(); i++){
    GetAt(i)->Code(file);
    fprintf(file,"%s", i == GetCount() - 1 ? End : Separator);
  }
}

template <class C>
IfList<C>::IfList(const int Owner, const int Growth){
  m_iOwner = Owner ? 1 : 0;
  m_iCount = 0;
  m_iGrowth = (Growth > 0) ? Growth : 4;
  m_iSize = m_iGrowth;
  m_pObjects = (C**) malloc(sizeof(C*) * m_iSize);
}

template <class C>
IfList<C>::~IfList(){
  if (m_iOwner)
    for(int i = 0; i < m_iCount; i++)
      delete m_pObjects[i];
  free(m_pObjects);
}
