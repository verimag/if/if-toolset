/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * IF model interface
 *
 */

#ifndef MODEL_H
#define MODEL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

template <class C> class IfList;

class IfObject;
class IfConstant;
class IfFunction;
class IfType;
  class IfBasicType;
class IfVariable;
class IfExpression;
class IfConstraint;
class IfResource;
class IfSignal;
class IfSignalroute;
class IfAction;
class IfStatement;
  class IfBasicStatement;
  class IfBlockStatement;
class IfTransition;
class IfState;
class IfEntity;
  class IfAutomatonEntity;
    class IfProcessEntity;
    class IfObserverEntity;
    class IfPriorityRuleEntity;
  class IfMacroEntity;

#include "list.h"
#include "list.i"

#include "object.h"
#include "constant.h"
#include "type.h"
#include "variable.h"
#include "expression.h"
#include "constraint.h"
#include "resource.h"
#include "signal.h"
#include "signalroute.h"
#include "procedure.h"
#include "action.h"
#include "statement.h"
#include "transition.h"
#include "state.h"
#include "entity.h"
#include "priorityrule.h"

IfSystemEntity* Load(FILE* file, FILE** p_pfObservers, int p_nObservers);

char* indent(const int);
char* filename(const char*, const char* = NULL, const char* = NULL);

/* various options */
extern int opt_m4codegen; /* controls model instrumentation for code generation (esp. for observers) */

#endif
