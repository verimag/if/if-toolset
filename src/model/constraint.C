/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * implementation of constraints
 *
 */

#include "model.h"

IfConstraint::IfConstraint(unsigned Flags) 
  : IfObject(NULL, Flags) {
}

IfConstraint::~IfConstraint() {
}

int IfConstraint::Use(const IfVariable* Variable) const {
  // pure virtual
  return 0;
}

int IfConstraint::Def(const IfVariable* Variable) const {
  return 0;
}

/*
 *
 * basic constraint interface
 *
 */

IfExpression::Operator IfBasicConstraint::OPERATOR[] = {
  { "<", "lt", 0, 2}, 
  {"<=", "le", 0, 2},
  { "=", "eq", 0, 2},
  {">=", "ge", 0, 2},
  { ">", "gt", 0, 2}
};

IfBasicConstraint::IfBasicConstraint(IfExpression* X,
                                     IfExpression* Y,
                                     EOperator Operator,
                                     IfExpression* K,
                                     unsigned Flags)
  : IfConstraint(Flags) {
  // grammar patch - confusing expressions...
  if (X != NULL && Y == NULL && 
      X->IsBinary()) {
    IfBinaryExpression* x = (IfBinaryExpression*) X; 
    if (x->GetOperator() == IfBinaryExpression::MINUS) {
      X = x->GetLeft();
      Y = x->GetRight();
    }
  }
  // end grammar patch
  m_pX = X;
  m_pY = Y;
  m_eOperator = Operator;
  m_pK = K;
}

IfBasicConstraint::~IfBasicConstraint() {
  if (m_pX) delete m_pX;
  if (m_pY) delete m_pY;
  if (m_pK) delete m_pK;
}

void IfBasicConstraint::Dump(FILE* file) const {
  if (m_pX)
    m_pX->Dump(file);
  if (m_pY) {
    fprintf(file, "-");
    m_pY->Dump(file);
  }
  fprintf(file, "%s", OPERATOR[m_eOperator].name);
  if (m_pK)
    m_pK->Dump(file);
}

void IfBasicConstraint::PreCompile() {
  IfConstraint::PreCompile();

  IfType* integer = TYPES.GetAt(IfBasicType::INTEGER);
  IfType* real = TYPES.GetAt(IfBasicType::REAL);
  IfType* clock = TYPES.GetAt(IfBasicType::CLOCK);

  int ok = 1;

  if (m_pX) { 
    ok &= m_pX->Compile();
    ok &= m_pX->IsReference();
    if (m_pX->GetType())
      ok &= m_pX->GetType()->Match(clock);
  }
  if (m_pY) { 
    ok &= m_pY->Compile();
    ok &= m_pY->IsReference();
    if (m_pY->GetType())
      ok &= m_pY->GetType()->Match(clock);
  }
  if (m_pK) {
    ok &= m_pK->Compile();
    ok &= !m_pK->IsTimed();
    if (m_pK->GetType()) {
      ok &= m_pK->GetType()->Match(integer, real);
      if (m_pK->GetType()->Match(real))
        Protest("real-value in clock constraint");
    }
  }

  ok &= (m_pX || m_pY) && m_pK;

  SetFlag(CORRECT, ok);
  SetFlag(TIMED, 1);
  
}

void IfBasicConstraint::Code(FILE* file) const {
  if (m_eOperator == LT || m_eOperator == LE || m_eOperator == EQ) {
    fprintf(file, "constraint(");
    if (m_pX) m_pX->Code(file); else fprintf(file, "0");
    fprintf(file, ",");
    if (m_pY) m_pY->Code(file); else fprintf(file, "0");
    fprintf(file, ",2*((int) "); m_pK->Code(file); fprintf(file, ")");
    if (m_eOperator != LT) fprintf(file, "+1");
    fprintf(file, ")");
  }
  if (m_eOperator == EQ)
    fprintf(file, ",");
  if (m_eOperator == EQ || m_eOperator == GE || m_eOperator == GT) {
    fprintf(file, "constraint(");
    if (m_pY) m_pY->Code(file); else fprintf(file, "0");
    fprintf(file, ",");
    if (m_pX) m_pX->Code(file); else fprintf(file, "0");
    fprintf(file, ",-2*("); m_pK->Code(file); fprintf(file, ")");
    if (m_eOperator != GT) fprintf(file, "+1");
    fprintf(file, ")");
  }
}

int IfBasicConstraint::Use(const IfVariable* Variable) const {
  int use = 0;
  if (m_pX) use |= m_pX->Use(Variable);
  if (m_pY) use |= m_pY->Use(Variable);
  if (m_pK) use |= m_pK->Use(Variable);
  return use;
}

/*
 *
 * composed constraints implementation
 *
 */

IfComposedConstraint::IfComposedConstraint(IfList<IfBasicConstraint>* Constraints,
                                           unsigned Flags) 
  : IfConstraint(Flags) {
  m_pConstraints = Constraints;
}

IfComposedConstraint::~IfComposedConstraint() {
  delete m_pConstraints;
}

void IfComposedConstraint::Dump(FILE* file) const {
  m_pConstraints->Dump(file, "", " and ", "");
}

void IfComposedConstraint::PreCompile() {
  IfConstraint::PreCompile();
  
  int ok = m_pConstraints->Compile();

  SetFlag(CORRECT, ok);
  SetFlag(TIMED, 1);
}

void IfComposedConstraint::Code(FILE* file) const {
  m_pConstraints->Code(file, "", ",", "");
}

int IfComposedConstraint::Use(const IfVariable* Variable) const {
  int use = 0;
  for(int i = 0; i < m_pConstraints->GetCount() && !use; i++)
    use |= m_pConstraints->GetAt(i)->Use(Variable);
  return use;
}
