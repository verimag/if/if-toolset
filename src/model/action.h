/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * abstract action interface 
 *
 */

class IfAction : public IfObject {

 public:
  IfAction(unsigned Flags = 0);
  virtual ~IfAction() {}

 public:
  virtual const char* GetClass() const 
    { return "action"; }
  virtual int IsAction() const
    { return 1; }

  virtual void PreCompile();
  virtual void Code(FILE* file) const;
          void Alpha(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 public:
  virtual int IsSkip() const { return 0; }
  virtual int IsInformal() const { return 0; }
  virtual int IsTask() const { return 0; }
  virtual int IsSet() const { return 0; }
  virtual int IsReset() const { return 0; }
  virtual int IsInput() const { return 0; }
  virtual int IsOutput() const { return 0; }
  virtual int IsAcquire() const { return 0; }
  virtual int IsRelease() const { return 0; }
  virtual int IsFork() const { return 0; }
  virtual int IsKill() const { return 0; }
  virtual int IsCall() const { return 0; }
  virtual int IsNextstate() const { return 0; }
  virtual int IsStop() const { return 0; }
  virtual int IsFlush() const { return 0; }
  virtual int IsCut() const { return 0; }

  virtual int IsMatch() const { return 0; }
  virtual int IsMatchInput() const { return 0; }
  virtual int IsMatchDiscard() const { return 0; }
  virtual int IsMatchOutput() const { return 0; }
  virtual int IsMatchFork() const { return 0; }
  virtual int IsMatchKill() const { return 0; }
  virtual int IsMatchDeliver() const { return 0; }
  virtual int IsMatchInformal() const { return 0; }
  virtual int IsMatchProbability() const { return 0; }

 public:
  static IfTransition* TRANSITION;
  
};

/*
 * 
 * skip action 
 *
 */

class IfSkipAction : public IfAction {

 public:
  IfSkipAction(unsigned Flags = 0);
  virtual ~IfSkipAction() {}

 public:
  virtual int IsSkip() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

};

/*
 * 
 * informal action 
 *
 */

class IfInformalAction : public IfAction {

 public:
  IfInformalAction(char* Informal,
                   unsigned Flags = 0);
  virtual ~IfInformalAction();

 public:
  const char* GetInformal() const 
    { return m_pInformal; }

 public:
  virtual int IsInformal() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  char* m_pInformal;
};

/*
 * 
 * task action 
 *
 */

class IfTaskAction : public IfAction {

 public:
  IfTaskAction(IfExpression* Lhs,
               IfExpression* Rhs,
               unsigned Flags = 0);
  virtual ~IfTaskAction();

 public:
  inline IfExpression* GetLhs() const { return m_pLhs; }
  inline IfExpression* GetRhs() const { return m_pRhs; }

 public:
  virtual int IsTask() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfExpression* m_pLhs;
  IfExpression* m_pRhs;

};

/*
 * 
 * set action 
 *
 */

class IfSetAction: public IfAction {

 public:
  IfSetAction(IfExpression* Lhs,
              IfExpression* Rhs,
              unsigned Flags = 0);
  virtual ~IfSetAction();

 public:
  inline IfExpression* GetLhs() const { return m_pLhs; }
  inline IfExpression* GetRhs() const { return m_pRhs; }

 public:
  virtual int IsSet() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfExpression* m_pLhs;
  IfExpression* m_pRhs;

};

/*
 * 
 * reset action 
 *
 */

class IfResetAction : public IfAction {

 public:
  IfResetAction(IfExpression* Lhs,
                unsigned Flags = 0);
  virtual ~IfResetAction();

 public:
  inline IfExpression* GetLhs() const { return m_pLhs; }

 public:
  virtual int IsReset() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfExpression* m_pLhs;

};

/*
 * 
 * input action 
 *
 */

class IfInputAction : public IfAction {

 public:
  IfInputAction(IfSignal* Signal,
                IfList<IfExpression>* Parameters,
                unsigned Flags = 0);
  virtual ~IfInputAction();
  
 public:
  inline IfSignal* GetSignal() const { return m_pSignal; }
  inline IfList<IfExpression>* GetParameters() const { return m_pParameters; } 

 public:
  virtual int IsInput() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfSignal* m_pSignal;
  IfList<IfExpression>* m_pParameters;

};

/*
 * 
 * output action 
 *
 */

class IfOutputAction : public IfAction {

 public:
  IfOutputAction(IfSignal* Signal,
                 IfList<IfExpression>* Parameters,
                 IfExpression* Via,
                 IfExpression* To,
                 unsigned Flags = 0);
  virtual ~IfOutputAction();

 public:
  inline IfSignal* GetSignal() const { return m_pSignal; }
  inline IfList<IfExpression>* GetParameters() const { return m_pParameters; } 
  inline IfExpression* GetVia() const { return m_pVia; }
  inline IfExpression* GetTo() const { return m_pTo; }

 public:
  virtual int IsOutput() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:  
  IfSignal* m_pSignal;
  IfList<IfExpression>* m_pParameters;
  IfExpression* m_pVia;
  IfExpression* m_pTo;

};

/*
 *
 * acquire action
 *
 */

class IfAcquireAction : public IfAction {

 public:
  IfAcquireAction(IfList<IfResource>* Resources,
                  unsigned Flags = 0);
  virtual ~IfAcquireAction();

 public:
  inline IfList<IfResource>* GetResources() const { return m_pResources; }

 public:
  virtual int IsAcquire() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfList<IfResource>* m_pResources;

};

/*
 *
 * release action
 *
 */

class IfReleaseAction : public IfAction {

 public:
  IfReleaseAction(IfList<IfResource>* Resources,
                  unsigned Flags = 0);
  virtual ~IfReleaseAction();

 public:
  inline IfList<IfResource>* GetResources() const { return m_pResources; }

 public:
  virtual int IsRelease() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfList<IfResource>* m_pResources;

};

/*
 * 
 * fork action 
 *
 */

class IfForkAction : public IfAction {

 public:
  IfForkAction(IfExpression* Lhs,
               char* Name,
               IfList<IfExpression>* Parameters,
               unsigned Flags = 0);
  virtual ~IfForkAction();

 public:
  inline IfExpression* GetLhs() const { return m_pLhs; }
  inline IfProcessEntity* GetProcess() const { return m_pProcess; }
  inline IfSignalroute* GetSignalroute() const { return m_pSignalroute; }
  inline IfList<IfExpression>* GetParameters() const { return m_pParameters; } 
  inline void SetLhs(IfExpression* Lhs) { m_pLhs = Lhs; } 

 public:
  virtual int IsFork() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfExpression* m_pLhs;
  IfProcessEntity* m_pProcess;
  IfSignalroute* m_pSignalroute;
  IfList<IfExpression>* m_pParameters;
  char* m_pName;

};

/*
 * 
 * kill action 
 *
 */

class IfKillAction : public IfAction {

 public:
  IfKillAction(IfExpression* Rhs,
               unsigned Flags = 0);
  virtual ~IfKillAction();

 public:
  inline IfExpression* GetRhs() const { return m_pRhs; }

 public:
  virtual int IsKill() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfExpression* m_pRhs;
};

/*
 * 
 * call action 
 *
 */

class IfCallAction : public IfAction {

 public:
  IfCallAction(IfExpression* Lhs,
               IfProcedure* Procedure,
               IfList<IfExpression>* Parameters,
               unsigned Flags = 0);
  virtual ~IfCallAction();

 public:
  inline IfExpression* GetLhs() const { return m_pLhs; }
  inline IfProcedure* GetProcedure() const { return m_pProcedure; }
  inline IfList<IfExpression>* GetParameters() const { return m_pParameters; }

 public:
  virtual int IsCall() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfExpression* m_pLhs;
  IfProcedure* m_pProcedure;
  IfList<IfExpression>* m_pParameters;

};

/*
 * 
 * nextstate action 
 *
 */

class IfNextstateAction : public IfAction {

 public:
  IfNextstateAction(IfState* State,
                    unsigned Flags = 0);
  IfNextstateAction(char* StateName,
                    unsigned Flags = 0);
  virtual ~IfNextstateAction();

 public:
  inline IfState* GetState() const { return m_pState; }
  inline void SetState(const IfState* State) 
    { m_pState = (IfState*) State; }

 public:
  virtual int IsNextstate() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;
  
 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfState* m_pState;
  char* m_pStateName;

};

/*
 * 
 * stop action 
 *
 */

class IfStopAction : public IfAction {

 public:
  IfStopAction(unsigned Flags = 0);
  virtual ~IfStopAction() {}

 public:
  virtual int IsStop() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void Code(FILE* file) const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

};

/*
 * 
 * flush action 
 *
 */

class IfFlushAction : public IfAction {

 public:
  IfFlushAction(unsigned Flags = 0);
  virtual ~IfFlushAction() {}

 public:
  virtual int IsFlush() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

};

/*
 * 
 * cut action 
 *
 */

class IfCutAction : public IfAction {

 public:
  IfCutAction(unsigned Flags = 0);
  virtual ~IfCutAction() {}

 public:
  virtual int IsCut() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

};






/*
 *
 * Match Actions
 *
 */

class IfMatchAction : public IfAction {

public:
    IfMatchAction(unsigned Flags = 0) : IfAction(Flags) {}
    virtual ~IfMatchAction() {};

    virtual int IsMatch() const { return 1; }

public:
    virtual IfExpression* GetObsGuard() const = 0;

};

/*
 * 
 * match input action 
 *
 */

class IfMatchInputAction : public IfMatchAction {

 public:
  IfMatchInputAction(IfSignal* Signal,
                IfList<IfExpression>* Parameters,
                IfExpression* sigvar = NULL,
                IfExpression* in = NULL,
                unsigned isDiscard = 0,
                unsigned Flags = 0);
  virtual ~IfMatchInputAction();
  
 public:
  inline IfSignal* GetSignal() const { return m_pSignal; }
  inline IfList<IfExpression>* GetParameters() const { return m_pParameters; } 

 public:
  virtual int IsMatchInput() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

  virtual IfExpression* GetObsGuard() const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfSignal* m_pSignal;
  IfList<IfExpression>* m_pParameters;
  IfExpression* m_pSigvar;
  IfExpression* m_pIn;
  unsigned m_nIsDiscard;

};

/*
 * 
 * match output action 
 *
 */

class IfMatchOutputAction : public IfMatchAction {

 public:
  IfMatchOutputAction(IfSignal* Signal,
                IfList<IfExpression>* Parameters,
                IfExpression* sigvar = NULL,
                IfExpression* ord = NULL,
                IfExpression* from = NULL,
                IfExpression* via = NULL,
                IfExpression* to = NULL,
                unsigned Flags = 0);
  virtual ~IfMatchOutputAction();
  
 public:
  inline IfSignal* GetSignal() const { return m_pSignal; }
  inline IfList<IfExpression>* GetParameters() const { return m_pParameters; } 

 public:
  virtual int IsMatchOutput() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

  virtual IfExpression* GetObsGuard() const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfSignal* m_pSignal;
  IfList<IfExpression>* m_pParameters;
  IfExpression* m_pSigvar;
  IfExpression* m_pOrd;
  IfExpression* m_pFrom;
  IfExpression* m_pVia;
  IfExpression* m_pTo;

};

/*
 * 
 * match fork action 
 *
 */

class IfMatchForkAction : public IfMatchAction {

 public:
  IfMatchForkAction(char* name = NULL,
                IfExpression* var = NULL,
                IfExpression* in = NULL,
                unsigned Flags = 0);
  virtual ~IfMatchForkAction();
  
 public:
  virtual int IsMatchFork() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

  virtual IfExpression* GetObsGuard() const;

 protected:
  IfProcessEntity *m_pProcess;
  char* m_pName;
  IfExpression* m_pIn;
  IfExpression* m_pVar;

};

/*
 * 
 * match kill action 
 *
 */

class IfMatchKillAction : public IfMatchAction {

 public:
  IfMatchKillAction(char* name = NULL,
                IfExpression* var = NULL,
                IfExpression* in = NULL,
                unsigned Flags = 0);
  virtual ~IfMatchKillAction();
  
 public:
  virtual int IsMatchKill() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

  virtual IfExpression* GetObsGuard() const;

 protected:
  char *m_pName;
  IfProcessEntity *m_pProcess;
  IfExpression* m_pIn;
  IfExpression* m_pVar;

};

/*
 * 
 * match deliver action 
 *
 */

class IfMatchDeliverAction : public IfMatchAction {

 public:
  IfMatchDeliverAction(IfSignal* Signal,
                IfList<IfExpression>* Parameters,
                IfExpression* sigvar = NULL,
                IfExpression* from = NULL,
                unsigned Flags = 0);
  virtual ~IfMatchDeliverAction();
  
 public:
  inline IfSignal* GetSignal() const { return m_pSignal; }
  inline IfList<IfExpression>* GetParameters() const { return m_pParameters; } 

 public:
  virtual int IsMatchDeliver() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

  virtual IfExpression* GetObsGuard() const;

 public:
  virtual int Use(const IfVariable* Variable) const;
  virtual int Def(const IfVariable* Variable) const;

 protected:
  IfSignal* m_pSignal;
  IfList<IfExpression>* m_pParameters;
  IfExpression* m_pSigvar;
  IfExpression* m_pFrom;

};

/*
 * 
 * match informal action 
 *
 */

class IfMatchInformalAction : public IfMatchAction {

 public:
  IfMatchInformalAction(char* name,
                IfExpression* in = NULL,
                unsigned Flags = 0);
  virtual ~IfMatchInformalAction();
  
 public:
  virtual int IsMatchInformal() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

  virtual IfExpression* GetObsGuard() const;

 protected:
  char* m_pName;
  IfExpression* m_pIn;

};

/*
 * 
 * match probability action 
 *
 */

class IfMatchProbabilityAction : public IfMatchAction {

 public:
  IfMatchProbabilityAction(IfExpression* var = NULL,
                unsigned Flags = 0);
  virtual ~IfMatchProbabilityAction();
  
 public:
  virtual int IsMatchProbability() const { return 1; }
  virtual void Dump(FILE* file) const;
  virtual void PreCompile();
  virtual void Code(FILE* file) const;

  virtual IfExpression* GetObsGuard() const;

 protected:
  IfExpression* m_pVar;

};

