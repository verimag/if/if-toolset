/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * variable interface
 *
 */

class IfVariable : public IfObject {
  
 public:
  enum EKind 
    { VAR, FPAR, FIELD, EXISTS, FORALL };
  enum EScope 
    { PUBLIC, PRIVATE, NOSCOPE};
  enum EMode 
    { IN, INOUT, OUT, NOMODE};
  enum CFlag
    { IMPORTED = 1024 };

 public:
  IfVariable(EKind Kind,
             EScope Scope,
             EMode Mode,
             IfType* Type,
             IfExpression* Initializer,
             char* Name, 
             unsigned Flags = 0);
  virtual ~IfVariable();

 public:
  inline EKind GetKind() const { return m_eKind; }
  inline int IsVar() const { return m_eKind == VAR; }
  inline int IsFpar() const { return m_eKind == FPAR; }
  inline int IsField() const { return m_eKind == FIELD; }
  inline int IsExists() const { return m_eKind == EXISTS; }
  inline int IsForall() const { return m_eKind == FORALL; }

  inline EScope GetScope() const { return m_eScope; }
  inline int IsPrivate() const { return m_eScope == PRIVATE; }
  inline int IsPublic() const { return m_eScope == PUBLIC; }
  inline void SetScope(EScope Scope) { m_eScope = Scope; }

  inline EMode GetMode() const { return m_eMode; }
  inline int IsIn() const { return m_eMode == IN; }
  inline int IsInout() const { return m_eMode == INOUT; }
  inline int IsOut() const { return m_eMode == OUT; }
  
  inline IfType* GetType() const { return m_pType; }
  inline IfExpression* GetInitializer() const { return m_pInitializer; }
  
 public:
  virtual const char* GetClass() const { return "variable"; } 
  virtual void Dump(FILE* file) const;
  virtual void PreCompile(); 
  virtual void Code(FILE* file) const;

 public:
  static const char* MODE[];
  static const char* SCOPE[];

 protected:
  EKind m_eKind;
  EMode m_eMode;
  EScope m_eScope;
  IfExpression* m_pInitializer;
  IfType* m_pType;

};
