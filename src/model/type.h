/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * abstract type interface
 *
 */

class IfType : public IfObject {
 public:
  enum CFlag { PREDEFINED = 1024 };
  
 public:
  IfType(char* Name, 
         unsigned Flags = 0);
  virtual ~IfType() {}

 public:
  virtual const char* GetClass() const { return "type"; }

  virtual int IsBasic() const { return 0; }
  virtual int IsEnum() const { return 0; }
  virtual int IsRange() const { return 0; }
  virtual int IsRecord() const { return 0; }
  virtual int IsUnion() const { return 0; }
  virtual int IsSigUnion() const { return 0; }
  virtual int IsArray() const { return 0; }
  virtual int IsString() const { return 0; }
  virtual int IsTree() const { return 0; }
  virtual int IsAbstract() const { return 0; }
  virtual int IsOther() const { return 0; }

  virtual int IsScalar() const { return 0; }

 public:
  virtual IfType* GetBase() const 
    { return (IfType*) this; }
  int Match(const IfType* Type) const
    { return GetBase() == Type->GetBase(); }
  int Match(const IfType* Type1, const IfType* Type2) const
    { return Match(Type1) || Match(Type2); }
  int Match(const IfType* Type1, const IfType* Type2, const IfType* Type3) const
    { return Match(Type1) || Match(Type2) || Match(Type3); }

 public:
  virtual void PreCompile();

 protected:
  IfEntity* m_pContainer;

};


/*
 *
 * basic type interface
 *
 */

class IfBasicType : public IfType {

 public:
  enum EKind {BOOLEAN = 0, INTEGER, REAL, CLOCK, PID, VOID};

 public:
  IfBasicType(EKind Kind,
              char* Name, 
              unsigned Flags = 0);
  virtual ~IfBasicType() {}

 public:
  inline EKind GetKind() const { return m_eKind; }

 public:
  virtual int IsBasic() const { return 1; }
  virtual int IsScalar() const { return m_eKind != VOID; }

 protected:
  EKind m_eKind; 

};

/*
 *
 * enum type interface
 *
 */

class IfEnumType : public IfType {

 public:
  IfEnumType(IfList<IfEnumConstant>* Literals, 
             char* Name, 
             unsigned Flags = 0);
  virtual ~IfEnumType();

 public:
  inline IfList<IfEnumConstant>* GetLiterals() const { return m_pLiterals; }

 public:
  virtual int IsEnum() const { return 1; }
  virtual int IsScalar() const { return 1; }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 protected:
  IfList<IfEnumConstant>* m_pLiterals;

};

/*
 *
 * enum type interface
 *
 */

class IfRangeType : public IfType {

 public:
  IfRangeType(IfConstant* Lower,
              IfConstant* Upper,
	      IfConstant* Step,
              char* Name, 
              unsigned Flags = 0);
  virtual ~IfRangeType();

 public:
  inline IfConstant* GetLower() const { return m_pLower; }
  inline IfConstant* GetUpper() const { return m_pUpper; }
  inline IfConstant* GetStep() const { return m_pStep; }
  inline void SetLower(const IfConstant* Lower) 
    { m_pLower = (IfConstant*) Lower; }
  inline void SetUpper(const IfConstant* Upper) 
    { m_pUpper = (IfConstant*) Upper; } 
  inline void SetStep(const IfConstant* Step)
    { m_pStep = (IfConstant*) Step; }

 public:
  virtual int IsRange() const { return 1; }
  virtual int IsScalar() const { return 1; }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 public:
  virtual IfType* GetBase() const 
    { return m_pLower->GetType(); }

 protected:
  IfConstant* m_pLower;
  IfConstant* m_pUpper;
  IfConstant* m_pStep;

};

/*
 *
 * record type interface
 *
 */

class IfRecordType : public IfType {
  
 public:
  IfRecordType(IfList<IfVariable>* Fields,
               char* Name, 
               unsigned Flags = 0);
  virtual ~IfRecordType();

 public:
  inline IfList<IfVariable>* GetFields() const { return m_pFields; }

 public:
  virtual int IsRecord() const { return 1; }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 protected:
  IfList<IfVariable>* m_pFields;

};

/*
 *
 * union type interface
 *
 */

class IfUnionType : public IfType {
  
 public:
  IfUnionType(IfList<IfVariable>* Fields,
               char* Name, 
               unsigned IsSigUnion,
               unsigned Flags = 0);
  virtual ~IfUnionType();

 public:
  inline IfList<IfVariable>* GetFields() const { return m_pFields; }

 public:
  virtual int IsUnion() const { return 1; }
  virtual int IsSigUnion() const { return m_bIsSigUnion; }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 protected:
  IfList<IfVariable>* m_pFields;
  unsigned m_bIsSigUnion;

};

/*
 *
 * array type interface
 *
 */

class IfArrayType : public IfType {

 public:
  IfArrayType(IfConstant* Length,
              IfType* Element,
              char* Name, 
              unsigned Flags = 0);
  virtual ~IfArrayType();

 public:
  inline IfConstant* GetLength() const { return m_pLength; }
  inline IfType* GetElement() const { return m_pElement; }
  inline void SetLength(const IfConstant* Length) 
    { m_pLength = (IfConstant*) Length; } 

 public:
  virtual int IsArray() const { return 1; }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 protected:
  IfConstant* m_pLength;
  IfType* m_pElement;

};

/*
 *
 * string type interface
 *
 */

class IfStringType : public IfType {

 public:
  IfStringType(IfConstant* Length,
               IfType* Element,
               char* Name, 
               unsigned Flags = 0);
  virtual ~IfStringType();

 public:
  inline IfConstant* GetLength() const { return m_pLength; }
  inline IfType* GetElement() const { return m_pElement; }

 public:
  virtual int IsString() const { return 1; }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 protected:
  IfConstant* m_pLength;
  IfType* m_pElement;

};

/*
 *
 * tree type interface
 *
 */

class IfTreeType : public IfType {
  
 public:
  IfTreeType(IfConstant* Width,
             IfConstant* Height,
             IfType* Element,
             char* Name,
             unsigned Flags = 0);
  virtual ~IfTreeType();

 public:
  inline IfConstant* GetWidth() const { return m_pWidth; }
  inline IfConstant* GetHeight() const { return m_pHeight; }
  inline IfType* GetElement() const { return m_pElement; }

 public:
  virtual int IsTree() const { return 1; }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 protected:
  IfConstant* m_pWidth;
  IfConstant* m_pHeight;
  IfType* m_pElement;

};

/*
 *
 * abstract type interface
 *
 */

class IfFunction : public IfObject {

 public:
  IfFunction(IfList<IfType>* Parameters, 
             IfType* Results, 
             char* Name, 
             unsigned Flags = 0);
  virtual ~IfFunction();

 public:
  virtual void Link(IfType* Type);
  
 public:
  inline IfList<IfType>* GetParameters() const { return m_pParameters; }
  inline IfType* GetResult() const { return m_pResult; }

 public:
  virtual const char* GetClass() const { return "function"; }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;
  
 protected:
  IfList<IfType>* m_pParameters;
  IfType* m_pResult;
 
};


class IfAbstractType : public IfType {

 public:
  IfAbstractType(IfList<IfFunction>* Functions,
                 char* Name, 
                 unsigned Flags = 0);
  virtual ~IfAbstractType();

 public:
  inline IfList<IfFunction>* GetFunctions() const { return m_pFunctions; }

 public:
  virtual void Link();

 public:
  virtual int IsAbstract() const { return 1; }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 protected:
  IfList<IfFunction>* m_pFunctions;

};

/*
 *
 * other type interface [rename]
 *
 */

class IfOtherType : public IfType {

 public:
  IfOtherType(IfType* Type,
              char* Name, 
              unsigned Flags = 0);
  virtual ~IfOtherType();

 public:
  inline IfType* GetOther() const { return m_pOther; }

 public:
  virtual int IsOther() const { return 1; }
  virtual int IsScalar() const { return m_pOther->IsScalar(); }
  virtual void Dump(FILE*) const;
  virtual void PreCompile();
  virtual void Code(FILE*) const;

 public:
  virtual IfType* GetBase() const 
    { return m_pOther->GetBase(); }

 protected:
  IfType* m_pOther; 

};
