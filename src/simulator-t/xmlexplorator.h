/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * proxy for open explorator, controlled via XML messaging
 *
 */

#ifndef _WINSOCKAPI_
    #define SOCKET int
#else
    #define socklen_t int
#endif

#define XMLBUFGROWTH 256
#define XMLDEFAULTPORT 15555

// APACHE Xerces headers
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>

XERCES_CPP_NAMESPACE_USE

class IfXMLExploratorFacade {

public:
  IfXMLExploratorFacade(IfOpenExplorator* pExplorator);
  ~IfXMLExploratorFacade();
  void setupXMLParser();
  
  typedef enum { S_OKAY, S_CONERR, S_PROTERR, S_PROTDISCONNECT } SOCK_STATUS;
  SOCK_STATUS serve(int port = XMLDEFAULTPORT);
  
protected:
  SOCK_STATUS init(int port);
  SOCK_STATUS run();
  SOCK_STATUS readXML();
  void executeXML();
  void parseXML();
  void executeXML0();
  void executeFire(DOMElement* c);
  void executeGetState(DOMElement* c);
  void executeListTransitions(DOMElement* c);
  
  void sendXMLAnswer(const char* xml);
  void sendOK();
  
public:
  static const char* XMLHEADER;
  
protected:
  IfOpenExplorator* m_pExplorator;
  SOCKET m_nSocket;
  
  char* m_psXMLBuf;
  int m_nXMLBufMax;
  
  static int XML_PLATFORM_INITIALIZED;
  DOMLSParser *m_pDOMParser;
  XERCES_CPP_NAMESPACE::DOMDocument *m_pDocument;
};

class DOMExploratorErrorHandler : public DOMErrorHandler
{
public:
  DOMExploratorErrorHandler();
  ~DOMExploratorErrorHandler();
  
  bool getSawErrors() const;
  bool handleError(const DOMError& domError);
  void resetErrors();
  
private :
  // This is set if we get any errors,
  // and is queryable via a getter method. 
  bool    fSawErrors;
};

class IfXMLException
{
public:
  IfXMLException(const char* desc, bool copy=true) { m_psDesc = (desc?strdup(desc) : NULL); }
  IfXMLException(IfXMLException& other) { m_psDesc = (other.getDesc()?strdup(other.getDesc()) : NULL); }
  ~IfXMLException() { if(m_psDesc) free(m_psDesc); }
  
  const char* getDesc() const { return m_psDesc; }
  
private:
  IfXMLException() { }
  
protected:
  char* m_psDesc;
};

/*
 *
 * Conversion from XMLCh to const char* 
 *
 */

#ifdef _XS //Windows - clash with useless macro 
#undef _XS
#endif

class _XS
{
public :
    _XS(const XMLCh* const toTranscode) {
        fLocalForm = XMLString::transcode(toTranscode);
    }
    ~_XS() {
        XMLString::release(&fLocalForm);
    }
    const char* localForm() const {
        return fLocalForm;
    }
    operator const char* () const {
        return fLocalForm;
    }

private :
    char*   fLocalForm;
};

/*
 *
 *  Conversion from const char* to XMLCh 
 *
 */

class _SX
{
public :
    _SX(const char* const toTranscode) {
        fLocalForm = XMLString::transcode(toTranscode);
    }
    ~_SX() {
        XMLString::release(&fLocalForm);
    }
    XMLCh* localForm() const {
        return fLocalForm;
    }
    operator XMLCh* () const {
        return fLocalForm;
    }

private :
    XMLCh*   fLocalForm;
};
