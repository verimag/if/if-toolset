/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

#include <vector>

/*
 *
 * explorator : defines abstract graph exploration 
 *
 */

class IfExplorator : public IfDriver {

 public:
  IfExplorator(const IfEngine* engine, 
               const char* q_filename = NULL, 
               const char* t_filename = NULL, 
               const char* prism_filename = NULL, 
               const unsigned flags = 0,
               const unsigned long evFilter = 0);
  virtual ~IfExplorator();

  virtual void run();

 public:
  enum Flag { 
    PO = 1,     // partial order reduction
    SP = 2,
    ME = 4,     // mark transitions entering error states with &error
    MS = 8,     // mark transitions entering success states with &success
    FB = 16     // mark transitions entering success states with &success
  };
  
 protected:
  enum Mark {
    REACHED = 0x80000000,
    STACKED = 0x40000000,
    INDEXMASK = 0x0FFFFFFF
  };
  
 protected:
  virtual void visit(const IfConfig* source, const IfLabel* label, 
                     const IfConfig* target);
  virtual void visit(const IfConfig* source);
  
 protected:
  virtual void status() const;

  virtual void alarm();

  static IfExplorator* XP;
#ifdef win32native
  static HANDLE m_hTimerThread;
  friend DWORD WINAPI TimerCycle(LPVOID lpParameter);
#else 
  static inline void cb_alarm(int);
#endif

 protected:
  FILE* q_file;                              // state file
  FILE* t_file;                         // transition file 
  FILE* prism_file;                         // PRISM file -> Markov decision process for the IF model

  unsigned q_count;                       // state counter
  unsigned t_count;                  // transition counter
  
  unsigned m_flags;                               // flags
  unsigned m_time;                       // execution time
  unsigned m_evFilter;                               // flags

};

/* 
 *
 * bfs_explorator : breadth-first search exploration
 *
 */ 

class IfBfsExplorator : public IfExplorator {

 public:
  IfBfsExplorator(const IfEngine* engine, 
                  const char* q_filename = NULL, 
                  const char* t_filename = NULL, 
                  const char* prism_filename = NULL, 
                  const unsigned flags = 0,
                  const unsigned long evFilter = 0);
  virtual ~IfBfsExplorator();
  
 public:
  virtual void explore(IfConfig* source, IfLabel* label, 
                       IfConfig* target);
  virtual void run();
  virtual void run(IfConfig* start);

 protected:
  struct {
    unsigned size;
    IfConfig* *queue;
    unsigned head, tail;
  } bfs;
 
};

/*
 *
 * dfs_explorator : depth-first search exploration 
 *
 */

class IfDfsExplorator : public IfExplorator {

 public:
  IfDfsExplorator(const IfEngine* engine, 
                  const char* q_filename = NULL, 
                  const char* t_filename = NULL, 
                  const char* prism_filename = NULL, 
                  const unsigned flags = 0,
                  const unsigned ind_flags = 0,
                  const unsigned long evFilter = 0);
  virtual ~IfDfsExplorator();
 
 public:
  virtual void explore(IfConfig* source, IfLabel* label, 
                       IfConfig* target);
  virtual void run();
  virtual void run(IfConfig* start);

 protected:
  virtual void status() const;

  unsigned m_ind_flags;
  void restrict(); // partial order reduction
  void restrictFeeds(); // feeds limit reduction
  
  virtual void visit(const IfConfig* source);
  virtual void visit(const IfConfig* source, const IfLabel* label, const IfConfig* target) // C++ bug
    { IfExplorator::visit(source,label,target); }

 protected:
  struct Successor {
    IfConfig* state;
    IfLabel* label;
    Successor* next;
  };
  
  struct Stack {
    unsigned feeds;
    IfConfig* state;
    IfLabel* label;
    Successor* pending;
    Stack* tail;
  };

 protected:
  struct {
    unsigned depth;
    Stack* stack;
  } dfs;

 private:
  void printScn(const char*) const;
  void printStackRec(std::ostream&, const Stack*) const;
  void dumpBranches() const;
  unsigned length(const Successor*) const;

};

/*
 *
 * random explorator : random exploration
 *
 */

class IfRandomExplorator : public IfExplorator {
  
 public:
  IfRandomExplorator(const IfEngine* engine, 
                  const char* q_filename = NULL, 
                  const char* t_filename = NULL, 
                  const char* prism_filename = NULL, 
                  const unsigned flags = 0,
                  const unsigned long evFilter = 0);
  virtual ~IfRandomExplorator();

 public:
  virtual void explore(IfConfig* source, IfLabel* label, 
                       IfConfig* target);
  virtual void run();
  
 protected:
  virtual void status() const;
  
 protected:
  struct {
    IfConfig* source;
    IfLabel* label;
    IfConfig* target;
    unsigned count;
  } random;
  
};

/*
 *
 * interactive explorator
 *
 */

class IfInteractiveExplorator : public IfExplorator {
  
 public:
  IfInteractiveExplorator(const IfEngine* engine, 
                  const char* q_filename = NULL, 
                  const char* t_filename = NULL, 
                  const char* prism_filename = NULL, 
                  const unsigned flags = 0,
                  const unsigned long evFilter = 0);
  virtual ~IfInteractiveExplorator();

 public:
  virtual void explore(IfConfig* source, IfLabel* label, 
                       IfConfig* target);
  virtual void run();
  
 protected:
  virtual void status() const;
  
 protected:
    IfConfig* m_source;
    IfLabel** m_labels;
    IfConfig** m_targets;
    IfLinkedList<IfConfig*> m_path;
    unsigned m_nMaxSuccessors;
    unsigned m_nCurrentSuccessor;
};

/*
 *
 * open explorator
 *
 */

class IfOpenExplorator : public IfExplorator {

public:
    IfOpenExplorator(const IfEngine* engine, 
        const char* q_filename = NULL, 
        const char* t_filename = NULL, 
        const char* prism_filename = NULL, 
        const unsigned flags = 0,
        const unsigned long evFilter = 0);
    virtual ~IfOpenExplorator();

    virtual void explore(IfConfig* source, IfLabel* label, 
                        IfConfig* target);

public:
    virtual void init();
    virtual const IfConfig* getState();
    virtual const std::vector<IfLabel*>& listTransitions();
    virtual void fire(int trno);
    virtual void undo();
    virtual void redo();
    virtual void rewind();
    virtual void replay();

protected:
  virtual void status() const;
  virtual void printScenario(std::ostream& file);

protected:
    std::vector<IfConfig*>  m_vPath;
    std::vector<IfLabel*>   m_vScenario;
    int m_nPathCurrent;

    std::vector<IfConfig*>  m_vSuccessors;
    std::vector<IfLabel*>   m_vSuccLabels;
};

/*
 * 
 * debugger 
 *
 */

class IfDebugger: public IfExplorator {
  
 public:
  IfDebugger(const IfEngine* engine, 
             const char* q_filename = NULL, 
             const char* t_filename = NULL, 
             const char* prism_filename = NULL, 
             const unsigned flags = 0,
             const unsigned long evFilter = 0);
  virtual ~IfDebugger();
  
 public:
  virtual void explore(IfConfig* source, IfLabel* label,
                       IfConfig* target);
  virtual void run();
  
 protected:
  struct Successor {
    IfConfig* state;
    IfLabel* label;
    Successor* next;
  };
  
  struct Stack {
    IfConfig* state;
    Successor* pending;
    Successor* running;
    Stack* tail;
  };

 protected:
  virtual void status() const;
  virtual void readKey() const;
  
  virtual void init();
  virtual void push(Successor* su);
  virtual void pop();
  
  virtual void pending();
  virtual void next(const int);
  virtual void backward(const int);
  virtual void forward(const int);
  virtual void where() const;
  virtual void trace() const;
  virtual void help() const;
  
 protected:
  struct {
    Stack* stack;
    unsigned depth;
  } dfs;
  
};
