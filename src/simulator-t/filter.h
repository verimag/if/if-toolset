/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * time filter interface
 *
 */

class IfTimeFilter : public IfEngine, public IfDriver {

 public:
  IfTimeFilter(const IfEngine* engine, const IfDriver* driver = NULL);
  virtual ~IfTimeFilter();

 public:
  virtual IfConfig* start();
  virtual void run(const IfConfig* source);

 public:
  virtual void explore(IfConfig* source, IfLabel* label, IfConfig* target);

 protected:
  inline IfTime* getTime(const IfConfig* source) const 
    { return (IfTime*) source->get(if_pid_mk(PROCMAXNUM-1,0)); }
  
 protected:
  struct successor {
    IfLabel* label;
    IfConfig* target;
    successor* next;
  } *m_head, *m_tail;

};

/*
 *
 * priority filter interface (abstract)
 *
 */

class IfPriorityFilter : public IfEngine, public IfDriver {

 public:
  // priority rule function type, returns
  // != 0 : x has greater priority than y in source
  // == 0 : x and y are not comparable in source
  typedef int (*Rule)(IfConfig* source, if_pid_type x, if_pid_type y);

 public:
  // priority rules (this member could be made private but as long
  // as we will consider only one global set of rules, it works 
  static Rule *RULE; 

 public:
  IfPriorityFilter(const IfEngine* engine, const IfDriver* driver = NULL);
  virtual ~IfPriorityFilter();

 public:
  virtual IfConfig* start();
  virtual void run(const IfConfig* source);

 public:
  virtual void explore(IfConfig* source, IfLabel* label, IfConfig* target);
  
 protected:
  struct successor {
    IfLabel* label;
    IfConfig* target;
    successor* next;
  } *m_head, *m_tail;

  // problem : consider the situation where there are only two 
  // transitions, one eager another lazy, and the lazy one having greater 
  // priority.  The filtering will remove the eager one so we end up with one 
  // lazy successor.  Do we need the tick transition here ? I think so,
  // but it was previously removed because of the eager (non prioritary) 
  // transition.  So, the model is too restricted.

  // remark: 2) the problem above does not exist: if the lazy ones has
  // priority over the eager one this does not mean that time could ellapse!

  // remark: 1) if the model does not contain lazy or 
  // delayable transitions, the problem does not occur

  // solution: if ones detect that there are only lazy transitions
  // enabled after filtering, and there is no tick-transition, we have to 
  // add it.  We have to mark (simulator) transitions in order to see
  // their urgency (eager, lazy).
  
};

unsigned if_pid_obs_queue_length(IfConfig *q, const if_pid_type pid);
IfMessage* if_pid_obs_queue_get_first(IfConfig *q, if_pid_type pid);
IfMessage* if_pid_obs_queue_get_last(IfConfig *q, if_pid_type pid);
IfMessage* if_pid_obs_queue_get_at(IfConfig *q, if_pid_type pid, if_integer_type i);
IfMessage* if_pid_obs_queue_remove_first(IfConfig *q, if_pid_type pid);
IfMessage* if_pid_obs_queue_remove_last(IfConfig *q, if_pid_type pid);
IfMessage* if_pid_obs_queue_remove_at(IfConfig *q, if_pid_type pid, if_integer_type i);
