/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * basic type definitions
 *
 */

enum if_deadline { EAGER = 0, DELAYABLE, LAZY };         // deadlines

typedef if_integer_type if_time_value;          // clock values k

typedef if_integer_type if_time_bound;        // clock bounds # k

/* 
 * 
 * time : defines abstract clock state
 *
 */

#define CLKMAXNUM 512

class IfTime : public IfInstance {

 public:
  struct Constraint {      // defines clock constraints x - y # k
    if_clock_type x;
    if_clock_type y;
    if_time_bound k;
    unsigned active;
    int delta;
    
    inline int isEmpty() const 
      { return x == 0 && y == 0; }
  };
  
 public:
  IfTime();
  IfTime(const IfTime&);
  virtual ~IfTime();

 public:
  inline int dimension() const 
    { return m_clocks.getLength(); }

 public:
  virtual int active(const if_clock_type) const;
  virtual int timeval(const if_clock_type) const;
  virtual int guard(Constraint*, const if_deadline);
  virtual void set(if_clock_type*, const if_time_value, 
                   const if_pid_type);
  virtual void resetClock(if_clock_type*);
  virtual void splitByPrio(IfIterator* iterator) {}

 public:
  virtual int getPriority() const 
    { return -1; }

 public:
  virtual int include(const IfTime*) const;
  
 public:
  static const char* NAME;
  
 protected:
  int allocate(if_clock_type*, const if_pid_type);

 protected:
  IfVector<if_clock_type> m_clocks;

 private:
  static if_pid_type ALLOCATOR[CLKMAXNUM];

};


/*
 * 
 * discrete_time : defines discrete / discrete / enumerative time
 *
 */

#include "ifzone.h"

class IfDiscreteTime : public IfTime {

 public:
  IfDiscreteTime();
  IfDiscreteTime(const IfDiscreteTime&);
  virtual ~IfDiscreteTime();

 public:
  virtual int timeval(const if_clock_type) const;
  virtual int guard(Constraint*, const if_deadline);
  virtual void set(if_clock_type*, const if_time_value,
                   const if_pid_type);
  virtual void resetClock(if_clock_type*);  

 public:
  virtual void iterate(IfIterator*);
  virtual void copy(const IfInstance*);

 public:
  virtual int include(const IfTime*) const;

 public:
  DECLARE_STORABLE(IfInstance)

 protected:
  IfVector<if_time_value> m_values;

 private:
  static int DELTA;

};

/*
 *
 * continuous_time : defines continuous / dense / symbolic time
 *
 */

class IfDbmTime : public IfTime {

 public:
  IfDbmTime();
  IfDbmTime(const IfDbmTime&);
  virtual ~IfDbmTime();

  static void restrictExtrapolationZone(int cmax);

 public:
  virtual int timeval(const if_clock_type) const;
  virtual int guard(Constraint*, const if_deadline);
  virtual void set(if_clock_type*, const if_time_value,
                   const if_pid_type);
  virtual void resetClock(if_clock_type*);  

 public:
  virtual void splitByPrio(IfIterator* iterator);
  virtual void iterate(IfIterator*);
  virtual void copy(const IfInstance*);
  virtual int include(const IfTime*) const;

 public:
  DECLARE_STORABLE(IfInstance)

 protected:
    if_type_zone m_dbm;

 private:
  static if_type_zone_list m_restrictions;
  static if_type_zone_list m_split;

};

