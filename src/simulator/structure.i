/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * string : implementats null-terminated pointer strings
 *
 */

template <class C>
IfString<C>::IfString(const int len) {
  m_objects = new C* [len];
  for(int i = 0; i < len; i++)
    m_objects[i] = NULL;
}

template <class C>
IfString<C>::IfString(const IfString<C>& string) {
  int len = string.getLength();
  m_objects = new C* [len + 1];
  for(int i = 0; i <= len; i++)
    m_objects[i] = string.m_objects[i];
}

template <class C>
IfString<C>::~IfString() {
  delete[] m_objects;
}

template <class C>
int IfString<C>::getLength() const {
  int len = 0;
  while (m_objects[len])
    len++;
  return len;
}

template <class C>
int IfString<C>::compare(const IfString<C>* string) const {
  int i = 0;
  while (m_objects[i] && m_objects[i] == string->m_objects[i])
    i++;
  return (long) m_objects[i] - (long) string->m_objects[i];
}

template <class C>
unsigned long IfString<C>::hash(const unsigned long base) const {
  unsigned long hash = 0;
  for(int i = 0; m_objects[i]; i++)
    hash += ((unsigned long) m_objects[i]) >> 4;
  return hash % base;
}

template <class C>
IfString<C>* IfString<C>::copy() const {
  return new IfString<C>(*this);
}

template <class C>
void IfString<C>::print(FILE*) const {
  // pure virtual 
}

template <class C>
void IfString<C>::printXML(std::ostream&) const {
  // pure virtual 
}

/* 
 *
 * chunk : implements fixed-length pointer strings 
 *
 */

template <class C, unsigned L>
IfChunk<C,L>::IfChunk() {
  for(unsigned i = 0; i < L; i++)
    m_objects[i] = NULL;
}

template <class C, unsigned L>
IfChunk<C,L>::IfChunk(const IfChunk<C,L>& chunk) {
  for(unsigned i = 0; i < L; i++)
    m_objects[i] = chunk.m_objects[i];
}

template <class C, unsigned L>
IfChunk<C,L>::~IfChunk() {
}

template <class C, unsigned L>
int IfChunk<C,L>::compare(const IfChunk<C,L>* chunk) const {
  long cmp = 0;
  for(unsigned i = 0; i < L && !cmp; i++)
    cmp = (long) m_objects[i] - (long) chunk->m_objects[i];

  return cmp;
}

template <class C, unsigned L>
unsigned long IfChunk<C,L>::hash(const unsigned long base) const {
  unsigned long hash = 0;
  for(unsigned i = 0; i < L; i++)
    hash += ((unsigned long) m_objects[i]) >> 4;
  return hash % base;
}

template <class C, unsigned L>
IfChunk<C,L>* IfChunk<C,L>::copy() const {
  return new IfChunk<C,L>(*this);
}

template <class C, unsigned L>
void IfChunk<C,L>::print(FILE*) const {
  // pure virtual
}

template <class C, unsigned L>
void IfChunk<C,L>::xmlize(FILE*) const {
  // pure virtual
}

/*
 *
 * vect : implements variable-length vectors
 *
 */

template <typename T>
IfVector<T>::IfVector(const int length) {
  m_length = length;
  m_data = new T[m_length];
}

template <typename T>
IfVector<T>::IfVector(const IfVector<T>& vect) {
  m_length = vect.m_length;
  m_data = new T[m_length];
  for(int i = 0; i < m_length; i++)
    m_data[i] = vect.m_data[i];
}

template <typename T>
IfVector<T>::~IfVector() {
  delete[] m_data;
}

template <typename T>
int IfVector<T>::find(const T t) const {
  int pos = -1;
  for(int i = 0; i < m_length && pos == -1; i++)
    if (m_data[i] == t)
      pos = i;
  return pos;
}

template <typename T>
void IfVector<T>::insert(const int k, const T t) {
  if (0 <= k && k <= m_length) {
    T* data = m_data;
    m_data = new T[++m_length];
    for(int i = 0, j = 0; i < m_length; i++)
      m_data[i] = (i == k) ? t : data[j++]; 
    delete[] data;
  }
}

template <typename T>
void IfVector<T>::remove(const int k) {
  if (0 <= k && k < m_length) {
    T* data = m_data;
    m_data = new T[--m_length];
    for(int i = 0, j = 0; i < m_length; i++, j++) {
      if (j == k) j++;
      m_data[i] = data[j];
    }
    delete[] data;
  }
}

template <typename T>
int IfVector<T>::compare(const IfVector<T>* vect) const {
  int cmp = m_length - vect->m_length;
  for(int i = 0; i < m_length && !cmp; i++)
    cmp = (int) m_data[i] - (int) vect->m_data[i];
  return cmp;
}

template <typename T>
unsigned long IfVector<T>::hash(const unsigned long base) const {
  unsigned long key = 0;
  for(int i = 0; i < m_length; i++)
    key += ((unsigned long) m_data[i]) << (i % 17);
  return key % base;
}

template <typename T>
IfVector<T>* IfVector<T>::copy() const {
  return new IfVector<T>(*this);
}

template <typename T>
void IfVector<T>::print(FILE* file) const {
}

template <typename T>
void IfVector<T>::printXML(std::ostream& buf) const {
}

template <typename T>
void IfVector<T>::copy(const IfVector<T>* vect) {
  if (m_length != vect->m_length) {
    delete[] m_data;
    m_length = vect->m_length;
    m_data = new T[m_length];
  }
  for(int i = 0; i < m_length; i++)
    m_data[i] = vect->m_data[i];
}

/*
 *
 * linked list : implement double-linked lists
 *
 */

template <class C>
IfLinkedList<C>::IfLinkedList() {
  m_first = NULL;
  m_last = NULL;
}

template <class C>
IfLinkedList<C>::~IfLinkedList() {
  purge();
}

template <class C>
void IfLinkedList<C>::insert(const C* object, Node* next) {
  Node* node = new Node;
  node->m_object = (C*) object;
  node->m_prev = node->m_next = NULL;
  if (next == NULL)
    next = m_first;
  if (next) {
    node->m_prev = next->m_prev;
    node->m_next = next;
    next->m_prev = node;
    if (node->m_prev != NULL)
      node->m_prev->m_next = node;
    else
      m_first = node;
  }
  else 
    m_first = m_last = node;
}

template <class C>
void IfLinkedList<C>::append(const C* object, Node* prev) {
  Node* node = new Node;
  node->m_object = (C*) object;
  node->m_prev = node->m_next = NULL;
  if (prev == NULL)
    prev = m_last;
  if (prev) {
    node->m_next = prev->m_next;
    node->m_prev = prev;
    prev->m_next = node;
    if (node->m_next != NULL)
      node->m_next->m_prev = node;
    else 
      m_last = node;
  }
  else
    m_first = m_last = node;
}

template <class C>
void IfLinkedList<C>::remove(Node* node) {
  if (node->m_next)
    node->m_next->m_prev = node->m_prev;
  else 
    m_last = node->m_prev;
  if (node->m_prev)
    node->m_prev->m_next = node->m_next;
  else
    m_first = node->m_next;
  delete node->m_object;
  delete node;
}

template <class C>
void IfLinkedList<C>::purge() {
  for(Node* node = m_first; node != NULL; 
      node = m_first) {
    m_first = node->m_next;
    delete node->m_object;
    delete node;
  }
  m_last = NULL;
}

/*
 *
 * heap : implements dynamic heaps
 *
 */

// warnings 
// - object indexing must start at 1, not at 0
// - count denotes here the last used position

template <class C>
IfHeap<C>::IfHeap() {
  m_length = 4;
  m_objects = (C**) malloc(sizeof(C*) * m_length);
  m_count = 0;
}

template <class C> 
IfHeap<C>::~IfHeap() {
  free(m_objects);
}

template <class C> 
C* IfHeap<C>::get() {
  if (m_count == 0)
    return NULL;
  
  C* top = m_objects[1];
  m_objects[1] = m_objects[m_count--];
  
  // balance
  for(int j = 1; j <= m_count; ) {
    int i = j * 2;
    if (i > m_count)
      break;
    if (i + 1 <= m_count && 
        m_objects[i+1]->getPriority() > m_objects[i]->getPriority())
      i = i + 1;
    if (m_objects[j]->getPriority() >= m_objects[i]->getPriority())
      break;
    
    C* tmp = m_objects[j];
    m_objects[j] = m_objects[i];
    m_objects[i] = tmp;
    
    j = i;
  }
  
  return top;
}

template <class C>
void IfHeap<C>::put(const C* object) {
  if (m_count == m_length - 1) {
    m_length += m_length;
    m_objects = (C**) realloc(m_objects, sizeof(C*) * m_length);
  }
  
  m_objects[++m_count] = (C*) object;
  
  // balance 
  for(int i = m_count; i > 1;) {
    int j = i / 2;

    if (m_objects[j]->getPriority() >= m_objects[i]->getPriority())
      break;

    C* tmp = m_objects[j];
    m_objects[j] = m_objects[i];
    m_objects[i] = tmp;

    i = j;
  }
}

/* 
 *
 * bit vect : implements fixed-length bitvectors 
 *
 */

template <unsigned L>
IfBitVector<L>::IfBitVector() {
  for(unsigned k = 0; k < L; k++)
    m_data[k] = 0;
}

template <unsigned L>
IfBitVector<L>::IfBitVector(const IfBitVector<L>& bvector) {
  for(unsigned k = 0; k < L; k++)
    m_data[k] = bvector.m_data[k];
} 

template <unsigned L>
IfBitVector<L>::~IfBitVector() {
}

template <unsigned L>
int IfBitVector<L>::in(const int idx) const {
  if (0<=idx && idx<(int)L*8)
    return (m_data[idx/8]<<(idx%8))&128;
  else 
    return 0;
}

template <unsigned L>
void IfBitVector<L>::set(const int idx) {
  if (0<=idx && idx<(int)L*8)
    m_data[idx/8] |=  (128 >> (idx%8));
}

template <unsigned L>
void IfBitVector<L>::reset(const int idx) {
  if (0<=idx && idx<(int)L*8)
    m_data[idx/8] &= ~(128 >> (idx%8));
}

template <unsigned L>
unsigned IfBitVector<L>::cardinal() const {
  unsigned card = 0;
  for(unsigned k = 0; k < L; k++)
    for(unsigned char c = m_data[k]; c; c <<= 1)
      if (c & 128) card++;
  return card;
}

template <unsigned L>
int IfBitVector<L>::ord(const int idx) const {
  int ord = -1;
  if (in(idx)) {
    ord = 0;
    for(int k = 0; k < idx/8; k++)
      for(unsigned char c = m_data[k]; c; c <<= 1) 
        if (c & 128) ord++;
    for(int l = 0; l < idx%8; l++) {
      unsigned char c = m_data[idx/8 + 1];
      if ((c << l) & 128) ord++;
    }
  }
  return ord;
}

template <unsigned L>
int IfBitVector<L>::first() const {
  int idx = -1;
  for(unsigned k = 0; k < L; k++)
    if (m_data[k] != 255) {
      idx = k*8;
      for(unsigned char c = m_data[k]; c & 128; c <<= 1)
        idx++;
      break;
    }
  return idx;
}

template <unsigned L>
int IfBitVector<L>::last() const {
  int idx = -1;
  for(int k = L - 1; k >= 0; k--)
    if (m_data[k] != 0) {
      idx = k*8 + 7;
      for(unsigned char c = m_data[k]; !(c & 1); c >>= 1)
        idx--;
      break;
    }
  return idx;
}

template <unsigned L>
void IfBitVector<L>::copy(const IfBitVector<L>* bvector) {
  for(unsigned k = 0; k < L; k++)
    m_data[k] = bvector->m_data[k];
}

template <unsigned L>
int IfBitVector<L>::compare(const IfBitVector<L>* bvector) const {
  int cmp = 0;
  for(unsigned k = 0; k < L && !cmp; k++)
    cmp = m_data[k] - bvector->m_data[k];
  return cmp;
}

template <unsigned L>
IfBitVector<L>* IfBitVector<L>::copy() const {
  return new IfBitVector<L>(*this);
}

template <unsigned L>
unsigned long IfBitVector<L>::hash(const unsigned long base) const {
  unsigned long key = 0;
  for(unsigned k = 0; k < L; k++)
    key += m_data[k] << (3 * (k % 6));
  return key % base;
}

template <unsigned L>
void IfBitVector<L>::print(FILE* file) const {
  for(unsigned k = 0; k < L; k++)
    fprintf(file, "%x", m_data[k]);
}

template <unsigned L>
void IfBitVector<L>::printXML(std::ostream& buf) const {
  buf << "<IfBitVector>\n";         
  for(unsigned k = 0; k < L; k++)
    buf <<  m_data[k];
  buf << "</IfBitVector>\n";        
}

