/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/* 
 *
 * message : defines abstract messages
 *
 */

#define SIGMAXNUM 256

class IfMessage : public IfObject { 

 public:
  IfMessage(const unsigned sid,
            const if_pid_type destination = 0,
            const if_clock_type wait = -1);
  IfMessage(const IfMessage&);
  virtual ~IfMessage();

 public:
  inline unsigned getSid() const 
    { return m_sid; }
  inline if_pid_type getDestination() const 
    { return m_destination; }
  inline if_clock_type getWait() const 
    { return m_wait; }

  inline void setDestination(const if_pid_type destination) 
    { m_destination = destination; }
  inline void setWait(const if_clock_type wait) 
    { m_wait = wait; }
  
 public:
  DECLARE_STORABLE(IfMessage)
  virtual void sprint() const;
  DECLARE_STORE(IfMessage)

 public:
  virtual int GetParSize() const { return 0; }
  virtual void* GetParAddress() const { return (void*)this; }
 public:
  static const char* SIGNAME[SIGMAXNUM];

 protected:
  long unsigned m_sid;
  if_pid_type m_destination; // buffer use
  if_clock_type m_wait; // buffer use

};

/*
 *
 * queue : define message queues
 *
 */

class IfQueue : public IfObject {

 public:
  static IfQueue* NIL; 

 public:
  IfQueue(const IfMessage* = NULL, const IfQueue* = NULL);
  IfQueue(const IfQueue&);
  virtual ~IfQueue();

 public:
  inline const IfMessage* getHead() const 
    { return m_head; }
  inline const IfQueue* getTail() const 
    { return m_tail; }
  const IfMessage* getTailMessage() const;
  const IfMessage* getMessageAt(int i) const;

 public:
  IfQueue* putHead(const IfMessage*) const;
  IfQueue* putTail(const IfMessage*) const;

 public:
  IfQueue* remove(const IfMessage*) const;

 public:
  int GetLength() const;
  IfQueue* insert(const IfMessage*) const;
  
 public:
  DECLARE_STORABLE(IfQueue);
  DECLARE_STORE(IfQueue);

 protected:
  const IfMessage* m_head;
  const IfQueue* m_tail;

};
