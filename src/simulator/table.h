/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * table : defines abstract tables
 *
 */

template <class C> 
class IfTable {

 public:
  IfTable(const char* = NULL);
  virtual ~IfTable();

 public:
  inline unsigned getCount() const
    { return m_count; }

 public:
  virtual void purge();
  virtual C* store(const C* object);
  virtual void remove(const C* object);
  virtual void status(FILE* = stderr) const;
  virtual void print(FILE* = stderr) const;

 protected:
  const char* m_name;
  unsigned long m_count;

};

/*
 *
 * hash_table : defines hash collision list tables 
 *
 */

#define HASHBLOCKSIZE 4 

#define HASHTABLECOLS 32 

template <class C> 
class IfHashTable : public IfTable<C> {

 public:
  IfHashTable(const char* = NULL);
  virtual ~IfHashTable();

 public:
  virtual void purge();
  virtual C* store(const C* object);
  virtual void remove(const C* object);
  virtual void status(FILE* = stderr) const;
  virtual void print(FILE* = stderr) const;

 protected:
  void resize();  

 protected:
  struct Block : public IfChunk<C,HASHBLOCKSIZE> {
    Block* m_next;
    inline int isFull() const 
      { return this->getAt(HASHBLOCKSIZE-1) != NULL; }
  };
  
 protected:
  unsigned long m_size;
  Block* *m_blocks;
  
};

/*
 *
 * SplayTree : defines binary splay trees 
 *
 */

template <class C> 
class IfSplayTree : public IfTable<C> {

 public:
  IfSplayTree(const char* = NULL);
  virtual ~IfSplayTree();
  
 public:
  virtual void purge();
  virtual C* store(const C*);
  virtual void status(FILE* = stderr) const;
  virtual void print(FILE* = stderr) const;

 protected:
  struct Node {
    C* object;
    Node* left;
    Node* right;
  };

 protected:
  void purge(Node*);
  void splay(const C*);
  void print(FILE*, Node*, int) const;

 protected:
  Node* m_root;
  
};

/*
 *
 * If23Tree : defines {2,3} balanced trees
 *
 */

#ifdef BLAH

template <class C> 
class If23Tree : public IfTable<C> {

 public:
  If23Tree(const char* = NULL);
  virtual ~If23Tree();
  
 public:
  virtual void purge();
  virtual C* store(const C*);
  virtual void status(FILE* = stderr) const;
  virtual void print(FILE* = stderr) const;

 protected:
  enum NodeType { 
    UNARY_LEAF = 0, BINARY_LEAF, BINARY_NODE, TERNARY_NODE };
  struct Node {
    NodeType tp;
    Node(NodeType t) 
      :tp(t) {}
  };
  struct UnaryLeaf : public Node {
    C *k1;
    UnaryLeaf(C* v1) 
      : Node( UNARY_LEAF ), k1(v1) {}
  };
  struct BinaryLeaf : public Node {
    C *k1, *k2;
    BinaryLeaf(C* v1, C* v2) 
      : Node( BINARY_LEAF ), k1(v1), k2(v2) {}
  };
  struct BinaryNode : public Node {
    C *k1; 
    Node *l, *r;
    BinaryNode(C* v1, Node* x, Node* y) 
      : Node( BINARY_NODE), k1(v1), l(x), r(y) {}
  };
  struct TernaryNode : public Node {
    C *k1, *k2;
    Node *l, *m, *r;
    TernaryNode(C* v1, C* v2, Node* x, Node* y, Node* z) 
      : Node( TERNARY_NODE), k1(v1), k2(v2), l(x), m(y), r(z) {}
  };

 protected:
  Node* insert( Node* n, const C* k );
  Node* insert( UnaryLeaf* n, const C* k );
  Node* insert( BinaryLeaf* n, const C* k );
  Node* insert( BinaryNode* n, const C* k );
  Node* insert( TernaryNode* n, const C* k );

  void purge( Node* n );
  void purge( UnaryLeaf* n );
  void purge( BinaryLeaf* n );
  void purge( BinaryNode* n );
  void purge( TernaryNode* n );
  
 protected:
  Node* m_root;

  C* m_key;
  BinaryNode m_split;

};

#endif

