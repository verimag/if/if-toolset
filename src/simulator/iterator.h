/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * abstract handler interface
 *
 */

class IfHandler {

 public:
  enum Flag {
    MOVED = 1,              // stored version != current version
    DEAD = 2,                 // the current instance was killed
    VISIBLE = 4,       // the instance does some visible actions
    TIME = 8                   // the discrete / continuous time
  };

 public:
  IfHandler();
  IfHandler(const IfHandler&);
  virtual ~IfHandler();

 public:
  inline int testFlag(const unsigned flag) const
    { return m_flags & flag; }
  inline void setFlag(const unsigned flag) 
    { m_flags |= flag; }
  inline void resetFlag(const unsigned flag) 
    { m_flags &= (~flag); }

 public:
  inline unsigned getVersion() const 
    { return m_version; }
  inline void setVersion(const unsigned version) 
    { m_version = version; }

 public:
  DECLARE_RECORDABLE(IfHandler)

 protected:
  unsigned m_flags;
  unsigned m_version;

};

/* 
 *
 * label handler : defines label handler
 *
 */

#define LABELMAXSIZE 16384

class IfLabelHandler : public IfHandler {

 public:
  IfLabelHandler();
  virtual ~IfLabelHandler(); 

 public:
  inline IfLabel* getLabel() const
    { return (IfLabel*) &m_label; }
  inline int getLength() const 
    { return m_length; }

 public:
  int trace(const IfEvent*);

 public:
  DECLARE_RECORDABLE(IfLabelHandler)

 protected:
  IfLabel m_label;                               // traced label 
  long unsigned m_length;                      // traced label length
  long unsigned m_tab[LABELMAXSIZE];             // event level table

};

/*
 *
 * instance handler : defines instance handler 
 *
 */

class IfInstanceHandler : public IfHandler {

 public:
  IfInstanceHandler(IfInstance*);
  IfInstanceHandler(const IfInstanceHandler&);
  virtual ~IfInstanceHandler();

 public:
  inline IfInstance* getCurrent() const 
    { return m_current; }
  inline IfInstance* getStore() const 
    { return m_store; }
  inline IfInstanceHandler* getPrev() const 
    { return m_prev; }

  inline if_pid_type getPid() const
    { return m_current->getPid(); }
  inline int getPriority() const
    { return m_current->getPriority(); }

 public:
  inline void store() 
    { m_store = m_current->store(); }

  inline void deliver(const IfMessage* message) 
    { m_current->deliver(message); }
  inline void kill() 
    { m_current->reset(); }
  inline void iterate(IfIterator* iterator) 
    { m_current->iterate(iterator); }
  inline void splitByPrio(IfIterator* iterator) 
    { ((IfTime*) m_current)->splitByPrio(iterator); }

 public:
  inline int guard(IfTime::Constraint* constraints,
                   const if_deadline deadline)
    { return ((IfTime*) m_current)->guard(constraints, deadline); }
  inline void set(if_clock_type* clock, 
                  const if_time_value value,
                  const if_pid_type pid)
    { ((IfTime*) m_current)->set(clock, value, pid); }
  inline void reset(if_clock_type* clock)
    { ((IfTime*) m_current)->resetClock(clock); }
  inline int active(const if_clock_type clock)
    { return ((IfTime*) m_current)->active(clock); }
  inline int timeval(const if_clock_type clock)
    { return ((IfTime*) m_current)->timeval(clock); }
  
 public:
  inline int acquire(const if_pid_type locker, 
                     const int* resources) 
    { return ((IfMonitor*) m_current)->acquire(locker, resources); } 
  inline int release(const if_pid_type locker,
                     const int* resources) 
    { return ((IfMonitor*) m_current)->release(locker, resources); }

 public:
  DECLARE_RECORDABLE(IfInstanceHandler)

 protected:
  IfInstance* m_current;                     // current instance
  IfInstance* m_store;                    // last stored version 
  IfInstanceHandler* m_prev;                      // record list

};

/* 
 *
 * config handler : defines configuration handler
 *
 */

#define CONFIGMAXSIZE 4096
        /* in Chunks, up to 6000 instances */
#define PIDTMAXSIZE 8192
        // enough ??

class IfConfigHandler : public IfHandler {

 public:
  IfConfigHandler();
  virtual ~IfConfigHandler();

 public:
  int deliver(const if_pid_type, 
              const IfMessage*);
  int fork(IfInstance*, 
           if_pid_type*);
  int kill(const if_pid_type);
  int trace(const IfEvent* event);

 public:
  int guard(IfTime::Constraint*,
            const if_deadline);
  int set(if_clock_type*, 
          const if_time_value,
          const if_pid_type);
  int reset(if_clock_type*);
  int active(const if_clock_type);
  int timeval(const if_clock_type);

 public:
  int acquire(const if_pid_type, 
              const int*);
  int release(const if_pid_type,
              const int*);

 public:
  IfInstance* import(const if_pid_type);
  
 public:
  inline if_pid_type getPid(const int i) const 
    { return m_pidt[i]; }
  int fetchPid(const if_pid_type);

 public:
  inline IfInstanceHandler* getRunning() const 
    { return m_running; }

 public:
  void put(const IfConfig*);
  IfConfig* get() const;
  
 public:
  void iterate(IfIterator*);

 public:
  DECLARE_RECORDABLE(IfConfigHandler)

 protected:
  typedef IfLinkedList<IfInstanceHandler>::Node Node;

 protected:
  Node* retrieve(const if_pid_type);
  Node* retrieveTime();
  Node* retrieveMonitor();

 protected:
  IfLinkedList<IfInstanceHandler> m_instances;

  IfHeap<IfInstanceHandler> m_heap;
  IfInstanceHandler* m_running;

  if_pid_type m_pidt[PIDTMAXSIZE];

};

/*
 *
 * iterator : interface of iteration loop on configurations
 *
 */


class IfIterator : public IfEngine {

 public:
  enum ECutFlags {
    ON_ERROR = 1,
    ON_SUCCESS = 2, 
  };

 public:
  IfIterator();
  IfIterator(const IfDriver* driver);
  virtual ~IfIterator();
  
 public:
  inline int trace(const IfEvent* event) 
    {
      m_config.trace(event);
      return m_label.trace(event);
    }
  inline int trace(const IfEvent::Kind kind, 
                   const if_pid_type pid, 
                   const char* value,
                   const if_pid_type auxPid = 0,
                   IfObject* auxObj = NULL) 
  { IfEvent event(kind, pid, value, auxPid, auxObj); 
    return trace(&event); }
  inline int trace(const IfEvent::Kind kind, 
                   const if_pid_type pid, 
                   const char* value,
                   const double proba) 
    { IfEvent event(kind, pid, value, proba); 
      return trace(&event); }
 
 public:
  inline int deliver(const if_pid_type pid, 
                     const IfMessage* message) 
    { return m_config.deliver(pid, message); }
  inline int fork(IfInstance* instance, 
                  if_pid_type* pid) 
    { return m_config.fork(instance, pid); }
  inline int kill(const if_pid_type pid) 
    { return m_config.kill(pid); }

 public:
  inline int guard(IfTime::Constraint* constraints, 
                   const if_deadline deadline)
    { return m_config.guard(constraints, deadline); }
  inline int set(if_clock_type* clock, 
                 const if_time_value value,
                 const if_pid_type pid) 
    { return m_config.set(clock,value, pid); }
  inline int reset(if_clock_type* clock) 
    { return m_config.reset(clock); }
  inline int active(const if_clock_type clock)
    { return m_config.active(clock); }
  inline int timeval(const if_clock_type clock)
    { return m_config.timeval(clock); }

 public:
  inline int acquire(const if_pid_type locker, 
                     const int* resources) 
    { return m_config.acquire(locker, resources); }
  inline int release(const if_pid_type locker,
                     const int* resources)
    { return m_config.release(locker, resources); }

 public:
  inline IfInstance* import(const if_pid_type pid) 
    { trace(IfEvent::IMPORT, m_config.getRunning()->getPid(), "", pid);
      return m_config.import(pid); }
  inline IfInstance* lightRetrieve(const if_pid_type pid) 
    { return m_config.import(pid); }

 public:
  inline int fetchPid(const if_pid_type proc) 
    { return m_config.fetchPid(proc); }
  inline if_pid_type getPid(const int i) const
    { return m_config.getPid(i); }

 public:
  inline void setPriority(const int priority) 
    { m_priority = priority; }
  void step();

 public:
     inline void setCutOnError()        { m_nCutFlags |= ON_ERROR; }
     inline void setCutOnSuccess()      { m_nCutFlags |= ON_SUCCESS; }

 public:
  DECLARE_RECORDABLE(IfIterator)

 public:
  virtual IfConfig* start();
  virtual void run(const IfConfig* source);

 protected:
  IfConfig* m_source;                    // source configuration
  IfConfigHandler m_config;     // running configuration handler
  IfLabelHandler m_label;               // running trace handler

  long int m_priority;
  long int m_complete;

  long unsigned m_nCutFlags;
  
};

/*
 *
 * If2HopsEngine : interface of iteration loop on configurations
 *   drives another Engine, and returns a successor only if in the source state 
 *   there is no "tick" transition or there is only a self "tick" 
 *
 */


class If2HopsEngine : public IfEngine, public IfDriver {

 public:
  If2HopsEngine();
  If2HopsEngine(const IfEngine* engine, const IfDriver* driver = NULL);
  ~If2HopsEngine();

  // Engine part
 public:
  virtual IfConfig* start();
  virtual void run(const IfConfig* source);

  // Driver part
 public:
  virtual void explore(IfConfig* source,
                       IfLabel* label,
                       IfConfig* target);

 protected:
  struct Successor {
    IfConfig* state;
    IfLabel* label;
    Successor* next;

    Successor(IfConfig* s, IfLabel* l, Successor* n) { state = s; label = l; next = n; }
    ~Successor() { if(next) delete next; }
  };
  
  Successor* m_lSuccessors;
};

