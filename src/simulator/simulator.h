/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * IF simulator interface
 *
 */

#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <sstream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/*
 *
 * generic components
 *
 */

template <class C> 
  class IfString;
template <class C, unsigned L> 
  class IfChunk;
template <typename T>
  class IfVector;
template <class C>
  class IfLinkedList;
template <unsigned L> 
  class IfBitVector;

template <class C> 
  class IfTable;
template <class C> 
  class IfTreeTable;
template <class C> 
  class IfHashTable;

/*
 *
 * specific components
 *
 */

template <unsigned L, typename T> 
  struct if_state;

class IfObject;                       
  class IfMessage;
  class IfQueue;
  class IfInstance;                      
    class IfTime;
      class IfDiscreteTime;
      class IfContinuousTime;
    class IfMonitor;
    class IfBuffer;
    class IfObserverInstance;
  class IfPacket;
  class IfConfig;                       
  class IfEvent;
  class IfLabel;

class IfHandler;
  class IfInstanceHandler; 
  class IfConfigHandler;
  class IfLabelHandler;


class IfEngine;
  class IfIterator;                                    

class IfDriver;

class IfDynamicError {
        // exception class
public:
        const static int IAF = 1 ;

protected:
        int m_code;

public:
        IfDynamicError(int p_code);
};

/*
 *
 * macros  
 *
 */

#define DECLARE_STORABLE(Class) \
  virtual int compare(const Class*) const; \
  virtual unsigned long hash(const unsigned long) const; \
  virtual Class* copy() const; \
  virtual void print(FILE*) const; \
  virtual void printXML(std::ostream&) const;

#define DECLARE_STORE(Class) \
  static IfHashTable<Class> STORE; \
  virtual Class* store() const;

#define IMPLEMENT_STORE(Class,Name) \
  IfHashTable<Class> Class::STORE(Name); \
  Class* Class::store() const { \
    return STORE.store(this); \
  }

#define DECLARE_RECORDABLE(Class) \
  virtual void record(); \
  virtual void restore(); \
  virtual void forget(); \
  virtual void print(FILE*) const;


#ifdef DEBUG
#define TRACE(block) block
#else
#define TRACE(block) 
#endif

/*
 *
 * functions
 *
 */

unsigned long if_hash(const char*, 
                 const unsigned, 
                 const unsigned long);

unsigned long if_nextprime(const unsigned long);

/*
 *
 * execution options
 *
 */

extern unsigned opt_use_priorities;  // for observers, for the moment
extern unsigned opt_gen_lineno;  // for observers, for the moment
extern unsigned opt_discard;  // for observers, for the moment

/*
 *
 * header files
 *
 */

#include "base.h"
#include "base.i"
#include "structure.h"
#include "structure.i"
#include "table.h"
#include "table.i"

#include "object.h"
#include "queue.h"
#include "instance.h"
#include "observer.h"
#include "xtime.h"
#include "monitor.h"
#include "buffer.h"
#include "config.h"
#include "label.h"

#include "engine.h"
#include "iterator.h"

#endif
