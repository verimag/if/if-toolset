/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */



enum { // same as in signalroute.h

  FIFO        = 0x00000001,               // queueing policies
  MULTISET    = 0x00000002,
  
  RELIABLE    = 0x00000100,                     // reliability 
  LOSSY       = 0x00000200,
  
  PEER        = 0x00010000,            // dispacthing policies
  MULTICAST   = 0x00020000,
  UNICAST     = 0x00040000,
  
  URGENT      = 0x01000000,                  // delaying modes
  DELAY       = 0x02000000,
  RATE        = 0x04000000
};

/*
 *
 * buffer : defines abstract communication buffers
 *
 */

class IfBuffer : public IfInstance {
 
 public:
  IfBuffer(const if_pid_type, const if_pid_type, 
           const unsigned = 0, 
           const int = -1, const int = -1,
           const if_pid_type = 0,
           const IfQueue* = IfQueue::NIL,
           const double lossProba = 0);
  IfBuffer(const IfBuffer&);
  virtual ~IfBuffer();
  
 public:
  inline if_pid_type getFrom() const 
    { return m_from; }
  inline if_pid_type getTo() const 
    { return m_to; }
  inline int getLower() const 
    { return m_lower; }
  inline int getUpper() const
    { return m_upper; }
  inline int is(const unsigned flag) const 
    { return m_flags & flag; }
  inline double getLossProba() const
    { return m_lossProba; }

 public:
  virtual int guard(const IfMessage*) const;
  virtual void dispatch(const IfMessage*) const;
  virtual void copy(const IfInstance*);
  
 public:
  virtual void deliver(const IfMessage*);

 public:
  virtual void reset();
    
 protected:
  if_pid_type m_from;                       // from process class 
  if_pid_type m_to;                           // to process class
  long unsigned m_flags;                               // buffer flags
  long int m_lower;                        // transit time lower bound
  long int m_upper;                        // transit time upper bound

  double m_lossProba;
  
};


/*
 *
 * fifo buffer : defines fifo communication buffers
 *
 */

class IfFifoBuffer : public IfBuffer {
  
 public:
  IfFifoBuffer(const if_pid_type, const if_pid_type, 
               const unsigned = 0, 
               const int = -1, const int = -1,
               const if_pid_type = 0,
               const IfQueue* = IfQueue::NIL,
               const double lossProba = 0);
  IfFifoBuffer(const IfFifoBuffer&);
  virtual ~IfFifoBuffer();

 public:
  virtual void deliver(const IfMessage*);
  virtual void iterate(IfIterator*);
  
};

/*
 *
 * multiset buffer : defines multiset communication buffers (bag)
 *
 */

class IfMultisetBuffer : public IfBuffer {

 public:
  IfMultisetBuffer(const if_pid_type, const if_pid_type, 
                   const unsigned = 0, 
                   const int = -1, const int = -1,
                   const if_pid_type = 0,
                   const IfQueue* = IfQueue::NIL,
                   const double lossProba = 0);
  IfMultisetBuffer(const IfMultisetBuffer&); 
  virtual ~IfMultisetBuffer();

 public:
  virtual void deliver(const IfMessage*);
  virtual void iterate(IfIterator*);

};
