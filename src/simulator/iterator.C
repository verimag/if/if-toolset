/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

#include "simulator.h"

/*
 *
 * abstract handler implementation
 *
 */

IfHandler::IfHandler() {
  m_flags = 0;
  m_version = 0;
}

IfHandler::IfHandler(const IfHandler& handler) {
  m_flags = handler.m_flags;
  m_version = handler.m_version;
}

IfHandler::~IfHandler() {
}

void IfHandler::record() {
  // abstract
}

void IfHandler::restore() {
  // abstract
}

void IfHandler::forget() {
  // abstract
}

void IfHandler::print(FILE* file) const {
  fprintf(file, "\n%c%c%c%c %2d ", 
          testFlag(MOVED) ? 'M' : '-',
          testFlag(DEAD) ? 'D' : '-',
          testFlag(TIME) ? 'T' : '-',
          testFlag(VISIBLE) ? 'O' : '-',
          m_version);
}

/* 
 *
 * label handler : implements running trace handler
 *
 */

IfLabelHandler::IfLabelHandler() 
  : IfHandler(), m_label(LABELMAXSIZE) {
  m_length = 0;
  for(int i = 0; i < LABELMAXSIZE; i++) {
    m_label.setAt(i, NULL);
    m_tab[i] = 0;
  }
}

IfLabelHandler::~IfLabelHandler() {
}

int IfLabelHandler::trace(const IfEvent* event) {
  int ok = 1;
  if (m_length < LABELMAXSIZE - 1) {
    m_label.setAt(m_length, event->store());
    m_tab[m_length] = m_version;
    m_length++;
  }
  else {
    // truncate
    ok = 0;
  }
  return ok;
}

void IfLabelHandler::record() {
  m_version++;
}

void IfLabelHandler::restore() {
  while (m_length && m_tab[m_length - 1] == m_version) {
    m_label.setAt(m_length - 1, NULL);
    m_tab[m_length - 1] = 0;
    m_length--;
  }
}

void IfLabelHandler::forget() {
  restore();
  m_version--;
}

void IfLabelHandler::print(FILE* file) const {
  IfHandler::print(file);
  m_label.print(file);
}

/*
 *
 * instance handler : implements running instance handler
 *
 */

IfInstanceHandler::IfInstanceHandler(IfInstance* instance) 
  : IfHandler() {
  m_current = instance->copy();
  m_store = instance;
  m_prev = NULL;
}

IfInstanceHandler::IfInstanceHandler(const IfInstanceHandler& handler) 
  : IfHandler(handler) {
  m_current = handler.m_current->copy();
  m_store = handler.m_store;
  m_prev = handler.m_prev;
}

IfInstanceHandler::~IfInstanceHandler() {
  if (m_current) delete m_current;
}

void IfInstanceHandler::record() {
  m_prev = new IfInstanceHandler(*this);
  // warning : the version number is not set here
}

void IfInstanceHandler::restore() {
  m_flags = m_prev->m_flags;
  m_current->copy(m_prev->m_current);
  m_store = m_prev->m_store;
}

void IfInstanceHandler::forget() {
  IfInstanceHandler* prev = m_prev;
  restore();
  m_version = m_prev->m_version;
  m_prev = m_prev->m_prev;
  delete prev;
}

void IfInstanceHandler::print(FILE* file) const {
  IfHandler::print(file);
  m_current->print(file);
}

/*
 *
 * config handler : implements running configuration handler
 *
 */

IfConfigHandler::IfConfigHandler() 
  : IfHandler() {
  m_running = NULL;
}

IfConfigHandler::~IfConfigHandler() {
}

int IfConfigHandler::deliver(const if_pid_type pid, 
                             const IfMessage* message) {
  Node* node = retrieve(pid);

  if (node) {
    m_instances.getAt(node)->deliver(message);
    m_instances.getAt(node)->setFlag(MOVED);
  }

  return node ? 1 : 0;
}

int IfConfigHandler::fork(IfInstance* instance, 
                          if_pid_type* pid) {
  Node* node = NULL;
  IfInstanceHandler* handler = NULL;

  *pid = instance->getPid() & PROCMASK;

  for(node = m_instances.getFirst(); node != NULL; 
      node = m_instances.getNext(node)) {
    handler = m_instances.getAt(node);

    if (handler->getPid() < *pid)
      continue;
    if (handler->getPid() == *pid) {
      if (!handler->testFlag(DEAD))
        (*pid)++;
      continue;
    }
    if (handler->getPid() > *pid)
      break;
  }

  instance->setPid(*pid);

  handler = new IfInstanceHandler(instance->store());
  handler->setVersion(m_version);
  
  if (node)
    m_instances.insert(handler, node);
  else
    m_instances.append(handler);
  
  return 1;
}

int IfConfigHandler::kill(const if_pid_type pid) {
  Node* node = retrieve(pid);

  if (node) {
    m_instances.getAt(node)->kill();
    m_instances.getAt(node)->setFlag(DEAD);
  }

  return node ? 1 : 0;
}

int IfConfigHandler::trace(const IfEvent* event) {
    
    // filter uninteresting events
    if(m_running->getCurrent()->getPriority() == OBSERVER_PRIORITY)
      return 1;
    switch(event->getKind()) {
    case IfEvent::INFORMAL:
    case IfEvent::INPUT:
    case IfEvent::DISCARD:
    case IfEvent::OUTPUT:
    case IfEvent::FORK:
    case IfEvent::KILL:
    case IfEvent::DELIVER:
    case IfEvent::PROBABILITY:
        break;
    default: return 1;
    }

    Node* node = NULL;
    IfInstanceHandler* handler = NULL;
    event = event->store();
    
    for(node = m_instances.getFirst(); node != NULL; 
    node = m_instances.getNext(node)) {
        handler = m_instances.getAt(node);
        
        if (handler->getPriority() == OBSERVER_PRIORITY) {
            if (handler->getVersion() < m_version) {
                handler->record();
                handler->setVersion(m_version);    
            }

            ((IfObserverInstance*)handler->getCurrent())->trace(event);
            handler->setFlag(MOVED);
        }
    }
    
    return 1;
}

int IfConfigHandler::guard(IfTime::Constraint* constraints, 
                           const if_deadline deadline) {
  Node* node = retrieveTime();
  int enabled = 0;
  
  if (node) {
    enabled = m_instances.getAt(node)->guard(constraints, deadline);
    m_instances.getAt(node)->setFlag(MOVED);
  }

  return enabled;
}

int IfConfigHandler::set(if_clock_type* clock, 
                         const if_time_value value,
                         const if_pid_type pid) {
  Node* node = retrieveTime();
  
  if (node) {
    m_instances.getAt(node)->set(clock, value, pid);
    m_instances.getAt(node)->setFlag(MOVED);
  }

  return node ? 1 : 0;
}

int IfConfigHandler::reset(if_clock_type* clock) {
  Node* node = retrieveTime();
  
  if (node) {
    m_instances.getAt(node)->reset(clock);
    m_instances.getAt(node)->setFlag(MOVED);
  }

  return node ? 1 : 0;
}

int IfConfigHandler::active(const if_clock_type clock) {
  Node* node = retrieveTime();
  
  return node ? m_instances.getAt(node)->active(clock) : 0;
}

int IfConfigHandler::timeval(const if_clock_type clock) {
  Node* node = retrieveTime();

  return node ? m_instances.getAt(node)->timeval(clock) : -1;
}

int IfConfigHandler::acquire(const if_pid_type locker,
                             const int* resources) {
  Node* node = retrieveMonitor();
  int available = 0;

  if (node) {
    available = m_instances.getAt(node)->acquire(locker, resources);
    m_instances.getAt(node)->setFlag(MOVED);
  }
  
  return available;
}

int IfConfigHandler::release(const if_pid_type locker,
                             const int* resources) {
  Node* node = retrieveMonitor();
  
  if (node) {
    m_instances.getAt(node)->release(locker, resources);
    m_instances.getAt(node)->setFlag(MOVED);
  }

  return 1;
}

IfInstance* IfConfigHandler::import(const if_pid_type pid) {
  Node* node = retrieve(pid);

  if (node == NULL) {
    // unrecoverable simulation error:
    //   [direct] instance adressing fault
    fprintf(stderr, "\nerror #1: instance addressing fault!\n");
    if_pid_print(pid, stderr);
throw IfDynamicError(IfDynamicError::IAF);
//      exit(1);
  }

  m_instances.getAt(node)->setFlag(MOVED);

  return m_instances.getAt(node)->getCurrent();
}

int IfConfigHandler::fetchPid(const if_pid_type proc) {
  Node* node = NULL;
  IfInstanceHandler* handler = NULL;
  if_pid_type pid = 0;
  int n = 0;

  for(node = m_instances.getFirst(); node != NULL; 
      node = m_instances.getNext(node)) {
    handler = m_instances.getAt(node);
    pid = handler->getPid();
    
    if ((pid & PROCMASK) == proc) {
      assert(n < PIDTMAXSIZE-1);
      m_pidt[n++] = pid;
    }
    else if ((pid & PROCMASK) > proc)
      break;
  }
  
  return n;
}

void IfConfigHandler::put(const IfConfig* source) {
  IfConfig::Chunk* chunk = NULL;
  IfInstance* instance = NULL;
  
  m_instances.purge();

  for(int i = 0; (chunk = source->getAt(i)); i++) 
    for(int j = 0; j < CONFIGCHUNKSIZE && 
          (instance = chunk->getAt(j)); j++) 
      m_instances.append(new IfInstanceHandler(instance));
}

IfConfig* IfConfigHandler::get() const {
  Node* node = NULL; 
  IfInstanceHandler* handler = NULL;

  IfConfig target(CONFIGMAXSIZE);
  IfConfig::Chunk chunk;
  int i = 0, j = 0;

  for(node = m_instances.getFirst(); node != NULL;
      node = m_instances.getNext(node)) {
    handler = m_instances.getAt(node);

    if (handler->testFlag(DEAD))
      continue;
    if (handler->testFlag(MOVED)) {
      handler->store();
      handler->resetFlag(MOVED);
    }
    chunk.setAt(j++, handler->getStore());
    if (j == CONFIGCHUNKSIZE) {
      if (i < CONFIGMAXSIZE)
        target.setAt(i++, chunk.store());
      j = 0;
    }
  }
  if (j != 0) {
    for(; j < CONFIGCHUNKSIZE;)
      chunk.setAt(j++, NULL);
    if (i < CONFIGMAXSIZE)
      target.setAt(i++, chunk.store());
  }
  if (i < CONFIGMAXSIZE)
    target.setAt(i++, NULL);
  if (i >= CONFIGMAXSIZE) {
    // unrecoverable simulation error:
    //   [direct] configuration size exceeds pre-allocated buffer
    fprintf(stderr, "\nerror #2: configuration overflow!\n");
    exit(1);
  }

  if (m_running) // is null in start()
    m_running->setFlag(MOVED);

  return target.store();
}

void IfConfigHandler::iterate(IfIterator* iter) {
  Node* node = NULL;
  int last_level = OBSERVER_PRIORITY;
  
  for(node = m_instances.getFirst(); node != NULL;
  node = m_instances.getNext(node))
    m_heap.put(m_instances.getAt(node));
  
  while ((m_running = m_heap.get()) != NULL) {
    /*!!!*/
    if(opt_use_priorities && m_running->getPriority() < last_level) {
      last_level = m_running->getPriority();
      if(m_running->getPriority() > -1) {
        Node* node = retrieveTime();
        
        if (node) {
          IfInstanceHandler* tmp = m_running;
          
          m_running = m_instances.getAt(node);
          iter->setPriority(OBSERVER_PRIORITY); // split has maximal priority
          m_running->setFlag(MOVED); 
          iter->record();
          
          for(node = m_instances.getFirst(); node != NULL; node = m_instances.getNext(node)) 
            if(m_instances.getAt(node)->getPriority() == OBSERVER_PRIORITY) {
              ((IfObserverInstance*)m_instances.getAt(node)->getCurrent())->flushLabel();
              m_instances.getAt(node)->setFlag(MOVED);
            }
            
          m_running->splitByPrio(iter);
          
          iter->forget();
          m_running->resetFlag(MOVED);
          
          m_running = tmp;
        }
      }
    }
    
    iter->setPriority(m_running->getPriority());
    m_running->setFlag(MOVED); 
    iter->record();
    
    if(opt_use_priorities && m_running->getPriority() < OBSERVER_PRIORITY)
      for(node = m_instances.getFirst(); node != NULL; node = m_instances.getNext(node)) 
        if(m_instances.getAt(node)->getPriority() == OBSERVER_PRIORITY) {
          ((IfObserverInstance*)m_instances.getAt(node)->getCurrent())->flushLabel();
          m_instances.getAt(node)->setFlag(MOVED);
        }
        
    m_running->iterate(iter);
    iter->forget();
    m_running->resetFlag(MOVED);
  }
}

void IfConfigHandler::record() {
  m_version++;
  m_running->record();
  m_running->setVersion(m_version);
}

void IfConfigHandler::restore() {
  Node *node = NULL, *next = NULL;
  IfInstanceHandler* handler = NULL;

  for(node = m_instances.getFirst(); node != NULL; 
      node = next) {
    next = m_instances.getNext(node);
    handler = m_instances.getAt(node);

    if (handler->getVersion() == m_version) {
      if (handler->getPrev()) // some previous level recorded
        handler->restore();   
      else // just created at current record level
        m_instances.remove(node);
    }
  }
}

void IfConfigHandler::forget() {
  Node *node = NULL, *next = NULL;
  IfInstanceHandler* handler = NULL;

  for(node = m_instances.getFirst(); node != NULL; 
      node = next) {
    next = m_instances.getNext(node);
    handler = m_instances.getAt(node);

    if (handler->getVersion() == m_version) {
      if (handler->getPrev()) // some previous level recorded
        handler->forget();
      else // just created at current record level
        m_instances.remove(node);
    }
  }

  m_version--;
}

IfConfigHandler::Node* IfConfigHandler::retrieve(const if_pid_type pid) {
  Node* node = NULL;
  IfInstanceHandler* handler = NULL;

  for(node = m_instances.getFirst(); node != NULL; 
      node = m_instances.getNext(node)) {
    handler = m_instances.getAt(node);

    if (handler->getPid() == pid && 
        !handler->testFlag(DEAD))
      break;
    if (handler->getPid() > pid) {
      node = NULL;
      handler = NULL;
      break;
    }
  }

  if (handler && 
      handler->getVersion() < m_version) {
    handler->record();
    handler->setVersion(m_version);    
  }

  return node;
}

IfConfigHandler::Node* IfConfigHandler::retrieveTime() {
  Node* node = m_instances.getLast();
  IfInstanceHandler* handler = m_instances.getAt(node);

  if (handler &&
      handler->getVersion() < m_version) {
    handler->record();
    handler->setVersion(m_version);
  }
  
  return node;
}

IfConfigHandler::Node* IfConfigHandler::retrieveMonitor() {
  Node* last = m_instances.getLast();
  Node* node = m_instances.getPrev(last);
  IfInstanceHandler* handler = m_instances.getAt(node);
  
  if (handler &&
      handler->getVersion() < m_version) {
    handler->record();
    handler->setVersion(m_version);
  }

  return node;
}

void IfConfigHandler::print(FILE* file) const {
  Node* node = NULL;
  IfInstanceHandler* handler = NULL;

  IfHandler::print(file);

  for(node = m_instances.getFirst(); node != NULL;
      node = m_instances.getNext(node)) {
    handler = m_instances.getAt(node);
    handler->print(file);
  }
}

/*
 *
 * iter : implements iteration loop on configurations
 *
 */

IfIterator::IfIterator() 
  : IfEngine() {
  m_source = NULL;
  m_nCutFlags = 0;
}

IfIterator::IfIterator(const IfDriver* driver) 
  : IfEngine(driver) {
  m_source = NULL;
}

IfIterator::~IfIterator() {
}

void IfIterator::record() {
  m_config.record();
  m_label.record();
}

void IfIterator::restore() {
  m_config.restore();
  m_label.restore();
}

void IfIterator::forget() {
  m_config.forget();
  m_label.forget();
}

void IfIterator::print(FILE* file) const {
  m_config.print(file);
  m_label.print(file);
}

void IfIterator::run(const IfConfig* source) {
  if( (m_nCutFlags & ON_ERROR) && source->isObsError() )
      return;
  if( (m_nCutFlags & ON_SUCCESS) && source->isObsSuccess() )
      return;
  if( source->isCut() ) 
      return;
  m_source = (IfConfig*) source;
  m_config.put(m_source);
  
  m_complete = -1;
  m_config.iterate(this);
}

void IfIterator::step() { 
  if (m_label.getLength() == 0)
    trace(IfEvent::SKIP, m_config.getRunning()->getPid(), "");
  
  if (m_complete == -1)
    m_complete = m_priority;
  if (m_priority == m_complete ||
      (m_priority == -1 /* time */ && m_complete != OBSERVER_PRIORITY))
    m_driver->explore(m_source, m_label.getLabel(), m_config.get());

  IfInstance::STEP++;
}

/*
 *
 * 2HopsIterator : interface of iteration loop on configurations
 *   drives another Engine, and returns a (discrete) successor only if in the source state 
 *   there is no "tick" transition or there is only a self "tick" 
 *
 */

If2HopsEngine::If2HopsEngine()
  : IfEngine(), IfDriver()
{
  m_lSuccessors = NULL;
}

If2HopsEngine::If2HopsEngine(const IfEngine* engine, const IfDriver* driver)
  : IfEngine(driver), IfDriver(engine)
{
  m_lSuccessors = NULL;
}

If2HopsEngine::~If2HopsEngine()
{
}

IfConfig* If2HopsEngine::start()
{
  return m_engine->start();
}

void If2HopsEngine::run(const IfConfig* source)
{
  if(m_lSuccessors) delete m_lSuccessors;
  m_lSuccessors = NULL;

  m_engine->run(source);

  // tests whether there are TIKC successors that are not self
  int explSucc = 1;
  Successor* tmp = m_lSuccessors;
  while(tmp) {
    if(tmp->label->getLength() && 
      (tmp->label->getAt(0)->getKind() == IfEvent::TICK) &&
      strcmp(tmp->label->getAt(0)->getValue(), "split") &&
      source->compare(tmp->state) )   // it's a TICK, but not a self TICK
    {
      explSucc = 0;
      break;
    }

    tmp = tmp->next;
  }

  // explore discrete successors only if explSucc
  tmp = m_lSuccessors;
  while(tmp) {
    if(explSucc) {
      m_driver->explore((IfConfig*)source, tmp->label, tmp->state);
    } else {
      if(tmp->label->getLength() && (tmp->label->getAt(0)->getKind() == IfEvent::TICK) )
              m_driver->explore((IfConfig*)source, tmp->label, tmp->state);
    }
    if(tmp->label) delete tmp->label;

    tmp = tmp->next;
  }
}

void If2HopsEngine::explore(IfConfig* source,
                     IfLabel* label,
                     IfConfig* target)
{
  m_lSuccessors = new Successor(target, label?label->copy():NULL, m_lSuccessors);
}
