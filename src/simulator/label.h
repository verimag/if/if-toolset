/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/* 
 *
 * event : defines events 
 *
 */

// SOLARIS GCC 3.2 Macro
#ifdef TICK
#undef TICK
#endif

#ifdef DEBUG
#undef DEBUG
#endif

extern int labeldebug;
#include <iostream>
using namespace std;

class IfEvent : public IfObject {

 public:
    static IfEvent* NIL;

 public:
  enum Kind {
    NONE     =  (unsigned)-1,
    SKIP     =  0,
    INFORMAL =  1,
    TASK     =  2,
    SET      =  3,
    RESET    =  4,
    INPUT    =  5,
    OUTPUT   =  6,
    CALL     =  7, 
    FORK     =  8, 
    KILL     =  9,
    TICK     = 10,
    DELIVER  = 11,
    IMPORT   = 12,
    LOSS     = 13,
    PROBABILITY   = 14,
    ACQUIRE  =  15,
    RELEASE  =  16,
    DELTA    = 17,
    DEBUG    = 18,
    DISCARD    = 19,
    FEED    = 20,
  };
  enum Flag {
    OWN      = 0x00010000
  };

 public:
  IfEvent(const unsigned,
          const if_pid_type, 
          const char*,
          const if_pid_type auxPid = 0,
          IfObject * const auxObj = NULL);
  IfEvent(const unsigned,
          const if_pid_type, 
          const char*,
          const double auxProbability);
  IfEvent(const IfEvent&);
  virtual ~IfEvent();

 public:
  inline unsigned getKind() const
    { return m_data & KINDMASK; }
  inline unsigned is(const unsigned flag) const 
    { return m_data & flag; }
  inline if_pid_type getPid() const 
    { return m_pid; }
  inline char* getValue() const 
    { return m_value; }

 public:
  inline IfObject* getAuxObj() const
    { return m_auxData.objpid.obj; }
  inline if_pid_type getAuxPid() const
    { return m_auxData.objpid.pid; }
  inline if_pid_type getPartner() const // synonim
    { return m_auxData.objpid.pid; }

  inline double getAuxProbability() const
    { return m_auxData.proba; }

 public:
  inline int isIndependent(unsigned ind_flags) const
    { return 
        getKind() == DELTA ||
        getKind() == DEBUG ||
        getKind() == SKIP ||
        getKind() == TASK ||
        getKind() == SET ||
        getKind() == RESET ||
        getKind() == INPUT ||
        getKind() == FEED ||
        getKind() == RELEASE ||
        getKind() == CALL ||
        (getKind() == IMPORT && (ind_flags & (1 << getKind()))) ||
        (getKind() == OUTPUT && (ind_flags & (1 << getKind()))) ||
        (getKind() == INFORMAL && !strncmp(m_value, "--", 2)) || 
        getKind() == FORK; 
    }

  /*inline int isIndependent(const IfString<IfObject>& ls, const IfLabel* myLabel) const;*/


 public:
  DECLARE_STORABLE(IfEvent)
  DECLARE_STORE(IfEvent)
    
 protected:
  static const int KINDMASK;
  static const int FLAGMASK;

  static const char* SYMBOL[];
  static const char* NAME[];

 protected:
  long unsigned m_data;
  if_pid_type m_pid;
  char* m_value;

  // auxiliary data
  union {
    struct {
      IfObject* obj;
      if_pid_type pid;
    } objpid;

    double proba;
  } m_auxData;
};

/* 
 *
 * label : defines labels as sequences of events 
 *
 */

class IfLabel : public IfObject {

 public:
  static IfLabel* NIL; 

 protected:
  static unsigned long EvPrintFilter;
 public:
  enum EvPrintFlag {
    NL = 31,
  };

 public:
  IfLabel(const unsigned = 0);
  IfLabel(const IfLabel&);
  virtual ~IfLabel();

 public:
  inline int getLength() const 
    { return m_events.getLength(); }
  inline IfEvent* getAt(const int i) const 
    { return m_events.getAt(i); }
  void setAt(const int i, const IfEvent* event) 
    { m_events.setAt(i, (IfEvent*) event); }
  static void setEvPrintFilter(const unsigned long pEvPrintFilter) 
    { EvPrintFilter = pEvPrintFilter; }

 public:
  inline if_pid_type getPid() const 
    { return getAt(0)->getPid(); }

  int isIndependent(unsigned ind_flags) const;
  //int isIndependent(const IfString<IfObject>& es) const;

 public:
  void strip(int (*Visible)(const IfEvent*));
  unsigned count(const IfEvent::Kind) const;

 public:
  DECLARE_STORABLE(IfLabel);
  DECLARE_STORE(IfLabel);

 protected:
  IfString<IfEvent> m_events;

};

