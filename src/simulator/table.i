/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * Table : implements abstract table
 *
 */

template <class C>
IfTable<C>::IfTable(const char* name) {
  m_name = name;
  m_count = 0;
}

template <class C>
IfTable<C>::~IfTable() {
}

template <class C>
void IfTable<C>::purge() {
  // abstract
}

template <class C>
C* IfTable<C>::store(const C* object) {
  // abstract
  return NULL;
}

template <class C>
void IfTable<C>::remove(const C* object) {
  // abstract
}

template <class C>
void IfTable<C>::status(FILE* file) const {
  // abstract
}

template <class C>
void IfTable<C>::print(FILE* file) const {
  // abstract
}

/* 
 *
 * HashTable : implements hash collision list table 
 *
 */

template <class C>
IfHashTable<C>::IfHashTable(const char* name) 
  : IfTable<C>(name) {
  m_size = 13;
  m_blocks = new Block*[m_size];
  for(unsigned long i = 0; i < m_size; i++)
    m_blocks[i] = NULL;
}

template <class C>
IfHashTable<C>::~IfHashTable() {
  status(stderr);
  purge();
}

template <class C>
void IfHashTable<C>::purge() {
  for(unsigned long i = 0; i < m_size; i++) {
    Block* block = m_blocks[i];
    while (block != NULL) {
      Block* next = block->m_next;
      for(unsigned long j = 0; j < HASHBLOCKSIZE; j++)
        if (block->getAt(j) != NULL)
          delete block->getAt(j);
      delete block;
      block = next;
    }
  }
  delete[] m_blocks;
}

template <class C>
C* IfHashTable<C>::store(const C* object) {
  C* store = NULL;
  unsigned long k = object->hash(m_size);
  Block* block = m_blocks[k];
  while (block != NULL) {
    for(unsigned long i = 0; i < HASHBLOCKSIZE; i++)
      if (block->getAt(i) && 
          block->getAt(i)->compare(object) == 0) {
        store = block->getAt(i);
        break;
      }
    block = store ? NULL : block->m_next; 
  }
  if (store == NULL) {
    this->m_count++;
    store = (C*) object->copy();
/* left this for future reference :
      if(store->hash(m_size) != k) {
          printf("simulator internal warning: non-optimal hashing.\n");
          object->print(stdout);
      }
 */
    block = m_blocks[k];
    if (block == NULL || block->isFull()) {
      Block* b = new Block;
      b->m_next = block;
      block = m_blocks[k] = b;
    }
    for(unsigned long i = 0; i < HASHBLOCKSIZE; i++)
      if (block->getAt(i) == NULL) {
        block->setAt(i, store);
        break;
      }
    if (this->m_count / m_size > HASHTABLECOLS)
      resize();
  } 
  return store;
}

template <class C>
void IfHashTable<C>::remove(const C* object) {

  unsigned long k = object->hash(m_size);

  Block *bo = NULL;
  unsigned long io = 0;
  
  // locate the object in the collision list
  for(Block* block = m_blocks[k]; block != NULL && bo == NULL; 
      block = block->m_next) 
    for(unsigned long i = 0; i < HASHBLOCKSIZE; i++)
      if (block->getAt(i) == object) {
        bo = block; io = i; 
        break;
      }
  
  if (bo == NULL)
    return;

  // locate the last object in the collision list
  Block *bl = m_blocks[k];
  unsigned long il = HASHBLOCKSIZE - 1;

  while (bl->m_next != NULL &&
         bl->getAt(0) != NULL)
    bl = bl->m_next;
  while (bl->getAt(il) == NULL)
    il--;

  // swap, then delete object

  bo->setAt(io, bl->getAt(il));
  bl->setAt(il, NULL);

  delete object;

  this->m_count--;
}

template <class C>
void IfHashTable<C>::status(FILE* file) const {
  unsigned long min = 1000000, max = 0;//, bc = 0;
  for(unsigned long i = 0; i < m_size; i++) {
    unsigned long length = 0;
    for(Block* block = m_blocks[i]; block; 
        block = block->m_next)
      length ++;
    if (min > length) min = length;
    if (max < length) max = length;
    // bc += length;
  }
  fprintf(file, "%8s table : %7ld items %6ld/entry %2ld/min %2ld/max %5.2f/avg\n",
          this->m_name, this->m_count, m_size, min * HASHBLOCKSIZE, max * HASHBLOCKSIZE, 
          (float) this->m_count / (float) m_size); 
}

template <class C>
void IfHashTable<C>::print(FILE* file) const {
  for(unsigned long i = 0; i < m_size; i++) {
    fprintf(file, "\nentry %lu:\n", i);
    for(Block* block = m_blocks[i]; block; 
        block = block->m_next) {
      for(int j = 0; j < HASHBLOCKSIZE && block->getAt(j); j++) 
        { block->getAt(j)->print(file); fprintf(file, "\n"); }
    }
  }
}

template <class C>
void IfHashTable<C>::resize() {
  unsigned long size = if_nextprime(2 * m_size + 1);
  unsigned long i;

  Block* *blocks = new Block* [size];
  for(i = 0; i < size; i++)
    blocks[i] = NULL;
  for(i = 0; i < m_size; i++) {
    Block* block = m_blocks[i];
    while (block != NULL) {
      Block* next = block->m_next;
      for(unsigned long j = 0; j < HASHBLOCKSIZE; j++)
        if (block->getAt(j) != NULL) {
          unsigned long k = block->getAt(j)->hash(size);
          if (blocks[k] == NULL || blocks[k]->isFull()) {
            Block* b = new Block;
            b->m_next = blocks[k];
            blocks[k] = b;
          }
          for(unsigned long l = 0; l < HASHBLOCKSIZE; l++)
            if (blocks[k]->getAt(l) == NULL) {
              blocks[k]->setAt(l, block->getAt(j));
              break;
            }
        }
      delete block;
      block = next;
    }
  }
  delete[] m_blocks;
  m_blocks = blocks;
  m_size = size;
}


/*
 *
 * SplayTree : implements splay trees
 *
 */

template <class C>
IfSplayTree<C>::IfSplayTree(const char* name) 
  : IfTable<C>(name) {
  m_root = NULL;
}

template <class C>
IfSplayTree<C>::~IfSplayTree() {
  status(stderr);
  purge();
}

template <class C>
void IfSplayTree<C>::purge() {
  if (m_root) purge(m_root);
}

template <class C>
C* IfSplayTree<C>::store(const C* object) {

  if (m_root == NULL) {
    m_root = new Node;
    m_root->object = (C*) object->copy();
    m_root->left = m_root->right = NULL;
    this->m_count = 1;
  }

  else {
    int cmp = 0;
    splay(object);
    cmp = object->compare(m_root->object);
    if (cmp < 0) {
      Node* n = new Node;
      n->object = (C*) object->copy();
      n->left = m_root->left;
      n->right = m_root;
      m_root->left = NULL;
      m_root = n;
      this->m_count++;
    }
    else if (cmp > 0) {
      Node* n = new Node;
      n->object = (C*) object->copy();
      n->right = m_root->right;
      n->left = m_root;
      m_root->right = NULL;
      m_root = n;
      this->m_count++;
    }
  }

  return m_root->object;
}

template <class C>
void IfSplayTree<C>::status(FILE* file) const {
  fprintf(file, "%8s table : %7d items\n", 
          this->m_name, this->m_count);
}

template <class C>
void IfSplayTree<C>::print(FILE* file) const {
  fprintf(file, "%s table :\n", this->m_name);
  print(file, m_root, 0);
}

template <class C>
void IfSplayTree<C>::purge(Node* t) {
  if (t->left) purge(t->left);
  if (t->right) purge(t->right);
  delete t->object;
  delete t;
}

template <class C>
void IfSplayTree<C>::splay(const C* object) {
  // simple top down splay, not requiring object to be in the tree
  Node N, *l, *r, *y, *t = m_root;
  int cmp = 0;

  N.left = N.right = NULL;
  l = r = &N;

  for (;;) {
    cmp = object->compare(t->object);
    if (cmp < 0) {
      if (t->left == NULL) break;
      cmp = object->compare(t->left->object);
      if (cmp < 0) {
        y = t->left;                           // rotate right
        t->left = y->right;
        y->right = t;
        t = y;
        if (t->left == NULL) break;
      }
      r->left = t;                               // link right
      r = t;
      t = t->left;
    } 
    else if (cmp > 0) {
      if (t->right == NULL) break;
      cmp = object->compare(t->right->object);
      if (cmp > 0) {
        y = t->right;                           // rotate left
        t->right = y->left;
        y->left = t;
        t = y;
        if (t->right == NULL) break;
      }
      l->right = t;                               // link left
      l = t;
      t = t->right;
    } 
    else {
      break;
    }
  }

  l->right = t->left;                              // assemble
  r->left = t->right;
  t->left = N.right;
  t->right = N.left;

  m_root = t;
}

template <class C>
void IfSplayTree<C>::print(FILE* file, Node* t, int depth) const {
  if (t) 
    print(file, t->left, depth + 1);
  for(int i = 0; i < depth; i++)
    fprintf(file, " ");
  if (t) 
    t->object->print(file);
  else
    fprintf(file, "-");
  fprintf(file, "\n");
  if (t)
    print(file, t->right, depth + 1);
}

/*
 *
 * B23Tree : implements {2,3} balanced trees
 * 
 */

#ifdef IF23TREE

template <class C>
If23Tree<C>::If23Tree(const char* name) 
  : IfTable<C>(name), m_root(NULL), m_key(NULL), m_split(NULL, NULL, NULL) {
}

template <class C> 
If23Tree<C>::~If23Tree() {
  status(stderr);
  purge();
}

template <class C>
void If23Tree<C>::purge() {
  if (m_root)
    purge( m_root );
  m_root = NULL;
}

template <class C>
C* If23Tree<C>::store( const C* k ) {

  if (m_root == NULL) {
    m_key = (C*) k->copy();
    m_root = new UnaryLeaf( m_key );
    this->m_count ++;

  } else {
    m_root = insert( m_root, k );
    if ( m_root == NULL ) 
      m_root = new BinaryNode( m_split.k1, m_split.l, m_split.r );
  }

  return m_key;
}

template <class C>
void If23Tree<C>::status(FILE* file) const {
  fprintf(file, "%8s table : %7d items\n", 
          this->m_name, this->m_count);
}

template <class C> 
void If23Tree<C>::print(FILE* file) const {
  // not yet implemented
}

template <class C>
If23Tree<C>::Node* If23Tree<C>::insert( Node* n, const C* k ) {
  switch ( n->tp ) {
  case UNARY_LEAF : return insert( (UnaryLeaf*) n, k );
  case BINARY_LEAF : return insert( (BinaryLeaf*) n, k );
  case BINARY_NODE : return insert( (BinaryNode*) n, k );
  case TERNARY_NODE : return insert( (TernaryNode*) n, k);
  }
  return NULL;
} 

template <class C>
If23Tree<C>::Node* If23Tree<C>::insert( UnaryLeaf* n, const C* k) {
  Node* result = n;
  int c1 = k->compare( n->k1 );

  if ( c1 == 0 ) 
    m_key = n->k1; 
  
  else {
    this->m_count++;
    m_key = (C*) k->copy(); 
    if (c1 < 0)
      result = new BinaryLeaf( m_key, n->k1);
    else
      result = new BinaryLeaf( n->k1, m_key );
    delete n;
  }
  
  return result;  
}

template <class C>
If23Tree<C>::Node* If23Tree<C>::insert( BinaryLeaf* n, const C* k) {
  Node* result = n;
  int c1 = k->compare( n->k1 );
  int c2 = k->compare( n->k2 );
  
  if (c1 == 0) 
    m_key = n->k1;
    
  else if ( c2 == 0 )
    m_key = n->k2;
    
  else {
    this->m_count++;
    m_key = (C*) k->copy();
    // split initialize
    if (c1 < 0) {
      m_split.k1 = n->k1;
      m_split.l = new UnaryLeaf( m_key );
      m_split.r = new UnaryLeaf( n->k2 );
    } else if ( c2 < 0) {
      m_split.k1 = m_key;
      m_split.l = new UnaryLeaf( n->k1 );
      m_split.r = new UnaryLeaf( n->k2 );
    } else {
      m_split.k1 = n->k2;
      m_split.l = new UnaryLeaf( n->k1 );
      m_split.r = new UnaryLeaf( m_key );
    }
    delete n;
    result = NULL;
  }
  
  return result;
}

template <class C>
If23Tree<C>::Node* If23Tree<C>::insert( BinaryNode* n, const C* k) {
  Node* result = n;
  int c1 = k->compare( n->k1 );
 
  if (c1 == 0) {
    m_key = n->k1;
    
  } else {
    Node* t = NULL;
    
    if ( c1 < 0 ) {
      t = n->l = insert (n->l, k);
      if ( t == NULL)  // split on left branch, absortion
        result = new TernaryNode( m_split.k1, n->k1,
                                  m_split.l, m_split.r, n->r);
    }
    else {
      t = n->r = insert (n->r, k);
      if ( t == NULL ) // split on right branch, absortion
        result = new TernaryNode( n->k1, m_split.k1,
                                  n->l, m_split.l, m_split.r);
    }
    
    if ( t == NULL )
      delete n;
  }
  
  return result;
}

template <class C>
If23Tree<C>::Node* If23Tree<C>::insert( TernaryNode* n, const C* k) {
  Node* result = n;
  int c1 = k->compare( n->k1 );
  int c2 = k->compare( n->k2 );
  
  if ( c1 == 0 ) 
    m_key = n->k1;

  else if ( c2 == 0 )
    m_key = n->k2; 

  else {
    Node* t = NULL;

    if (c1 < 0) {
      t = n->l = insert (n->l, k);
      if ( t == NULL ) { // split on left branch, propagate
        m_split.l = new BinaryNode( m_split.k1, m_split.l, m_split.r);
        m_split.k1 = n->k1;
        m_split.r = new BinaryNode( n->k2, n->m, n->r);
      }
    } else if ( c2 < 0 ) {
      t = n->m = insert (n->m, k);
      if ( t == NULL ) { // split on middle branch, propagate
        m_split.l = new BinaryNode( n->k1, n->l, m_split.l);
        m_split.r = new BinaryNode( n->k2, m_split.r, n->r);
      }
    } else {
      t = n->r = insert (n->r, k);
      if ( t == NULL ) { // split on right branch, propagate
        m_split.r = new BinaryNode( m_split.k1, m_split.l, m_split.r);
        m_split.k1 = n->k2;
        m_split.l = new BinaryNode ( n->k1, n->l, n->m);
      }
    }

    if ( t == NULL ) { // split
      delete n;
      result = NULL;
    }
  }

  return result;
}

template <class C>
void If23Tree<C>::purge( Node* n) {
  switch ( n->tp ) {
  case UNARY_LEAF : purge( (UnaryLeaf*) n); break;
  case BINARY_LEAF : purge( (BinaryLeaf*) n); break;
  case BINARY_NODE : purge( (BinaryNode*) n); break;
  case TERNARY_NODE : purge( (TernaryNode*) n); break;
  }
} 

template <class C>
void If23Tree<C>::purge( UnaryLeaf* n ) {
  delete n->k1; delete n;
}

template <class C>
void If23Tree<C>::purge( BinaryLeaf* n ) {
  delete n->k1; delete n->k2; delete n;
}

template <class C>
void If23Tree<C>::purge( BinaryNode* n ) {
  purge( n->l ); purge( n->r );
  delete n->k1; delete n;
}

template <class C>
void If23Tree<C>::purge( TernaryNode* n ) {
  purge( n->l ); purge(n->m); purge( n->r );
  delete n->k1; delete n->k2; delete n;
}

#endif
