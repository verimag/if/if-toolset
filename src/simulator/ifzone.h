/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * Zone / Clock DBM Interface 
 *
 */

#ifndef IFZONE_H
#define IFZONE_H



/*
-----------------------------------------------------------------------
Bound Definition
-----------------------------------------------------------------------
*/

#define IF_SIGN_LE \
  (1)

#define IF_SIGN_LT \
  (0)


typedef int if_type_bound;

#define if_make_bound(value,sign) \
  (((value) << 1) | (sign))

#define if_value_bound(bound) \
  ((bound) >> 1)

#define if_sign_bound(bound) \
  ((bound) & 1)

#define if_complement_bound(bound) \
  if_make_bound(-if_value_bound((bound)),!if_sign_bound((bound)))

#define if_min_bound(bound1,bound2) \
  ((bound1) < (bound2) ? (bound1) : (bound2))

#define if_add_bound(bound1,bound2) \
  ((((bound1) & ~1) + ((bound2) & ~1)) | ((bound1) & (bound2) & 1))

void if_print_bound(if_type_bound, FILE*);

#define IF_BOUND_INFTY \
  (0x10000000)




/*
-----------------------------------------------------------------------
Vector / Vector Table Definition
-----------------------------------------------------------------------
*/

typedef if_type_bound* if_type_vector;

typedef struct if_struct_vector_list {
  if_type_vector vector;
  struct if_struct_vector_list * next;
} * if_type_vector_list;

/*  *  */

if_type_vector if_create_vector(unsigned);

if_type_vector if_duplicate_vector(if_type_vector, unsigned);

void if_delete_vector(if_type_vector, unsigned);

void if_copy_vector(if_type_vector, if_type_vector, unsigned);

int if_compare_vector(if_type_vector, if_type_vector, unsigned);

unsigned long if_hash_vector(if_type_vector, unsigned, unsigned);

void if_print_vector(if_type_vector, unsigned, FILE*);

/*  *  */

void if_init_vector_table();

void if_exit_vector_table();

void if_info_vector_table(FILE*);

if_type_vector if_put_vector(if_type_vector, unsigned);




/*
-----------------------------------------------------------------------
Matrix / Matrix Table Definition
-----------------------------------------------------------------------
*/

typedef if_type_vector* if_type_matrix;

typedef struct if_struct_matrix_list {
  if_type_matrix matrix;
  struct if_struct_matrix_list * next;
} * if_type_matrix_list;

/*  *  */

if_type_matrix if_create_matrix(unsigned);

void if_delete_matrix(if_type_matrix, unsigned);

void if_copy_matrix(if_type_matrix, if_type_matrix, unsigned);

int if_compare_matrix(if_type_matrix, if_type_matrix, unsigned);

unsigned long if_hash_matrix(if_type_matrix, unsigned, unsigned);

void if_print_matrix(if_type_matrix, unsigned, FILE*);

/*  *  */

void if_init_matrix_table();

void if_exit_matrix_table();

void if_info_matrix_table(FILE*);

if_type_matrix if_put_matrix(if_type_matrix, unsigned);




/*
-----------------------------------------------------------------------
Zone Definition
-----------------------------------------------------------------------
*/

typedef if_type_matrix if_type_zone;

typedef struct if_struct_zone_list {
  if_type_zone zone;
  struct if_struct_zone_list * next;
} * if_type_zone_list;


#define if_copy_zone(dst,src) \
  (dst)=(src)

#define if_compare_zone(zone1,zone2) \
  ((zone1)!=(zone2))


if_type_zone if_empty_zone();

if_type_zone if_universal_zone();



int if_is_empty_zone(if_type_zone);

int if_is_universal_zone(if_type_zone zone);

int if_clock_in_zone(if_type_zone, int);


if_type_zone if_clkcmp_zone(int, int, int, int);

if_type_zone if_clklt_zone(int, int, int);

if_type_zone if_clkle_zone(int, int, int);

if_type_zone if_clkge_zone(int, int, int);

if_type_zone if_clkgt_zone(int, int, int);

if_type_zone if_clkeq_zone(int, int, int);


int if_normalize_zone(if_type_zone);            /* internal use only */

int if_extrapolate_zone(if_type_zone);          /* internal use only */


if_type_zone if_intersect_zone(if_type_zone, if_type_zone);

if_type_zone if_exists_zone(if_type_zone, int);

if_type_zone if_set_zone(if_type_zone, int, int);


if_type_zone if_round_zone(if_type_zone, int, int); 

if_type_zone if_open_right_zone(if_type_zone);

if_type_zone if_close_right_zone(if_type_zone);

if_type_zone if_open_left_zone(if_type_zone);

if_type_zone if_close_left_zone(if_type_zone);

if_type_zone if_open_zone(if_type_zone);

if_type_zone if_close_zone(if_type_zone);


if_type_zone if_backward_zone(if_type_zone);

if_type_zone if_forward_zone(if_type_zone);


if_type_zone if_since_zone(if_type_zone, if_type_zone);


if_type_zone if_diamond_zone(if_type_zone, int);


int if_include_zone(if_type_zone, if_type_zone);


void if_print_zone(if_type_zone, FILE*);

unsigned long if_hash_zone(if_type_zone, unsigned);



/*
-----------------------------------------------------------------------
Zone Extrapolation
-----------------------------------------------------------------------
*/

void if_restrict_extrapolation_zone(if_type_zone);

void if_restrict_extrapolation_zone(int);



/*
-----------------------------------------------------------------------
Time Progression
-----------------------------------------------------------------------
*/

void if_init_tpc(if_type_zone_list*);

void if_clear_tpc(if_type_zone_list*);


void if_limit_tpc(if_type_zone_list*, if_type_zone);

void if_restrict_tpc(if_type_zone_list*, if_type_zone);

void if_guard_tpc(if_type_zone_list*, if_type_zone, int);

void if_guard_split(if_type_zone_list*, if_type_zone);  // builds the intersection of the negation of guards -> for priority-based state splits


int if_simple_is_universal_zone_list(if_type_zone_list);


/*
-----------------------------------------------------------------------
Init / Exit Zone Module
-----------------------------------------------------------------------
*/

void if_init_zone_module();

void if_exit_zone_module();

#endif
