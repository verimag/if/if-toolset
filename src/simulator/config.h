/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/* 
 *
 * config : defines configurations of arbitrary size 
 *
 */

#define CONFIGCHUNKSIZE 6

class IfConfig : public IfObject {

 public:
  IfConfig(const unsigned = 0);
  IfConfig(const IfConfig&);
  virtual ~IfConfig();

 public:
  struct Chunk : public IfChunk<IfInstance,CONFIGCHUNKSIZE> {
    Chunk() : IfChunk<IfInstance,CONFIGCHUNKSIZE>() {}

    DECLARE_STORE(Chunk)
  };

 public:
  inline Chunk* getAt(const int i) const 
    { return m_chunks.getAt(i); }
  inline void setAt(const int i, const Chunk* chunk) 
    { m_chunks.setAt(i, (Chunk*) chunk); }
  inline unsigned getMark() const 
    { return m_mark; }
  inline void setMark(const unsigned mark) 
    { m_mark = mark; }

  IfInstance* get(const if_pid_type) const;
  IfTime* getTime() const;  
  
 public:
  int isObsSuccess() const;
  int isObsError() const;
  int isCut() const;

 public:
  DECLARE_STORABLE(IfConfig)
  DECLARE_STORE(IfConfig)

 protected:
  IfString<Chunk> m_chunks;
  long unsigned m_mark;

};

