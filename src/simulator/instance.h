/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/* 
 *
 * instance : defines abstract process instances 
 *
 */

#define PROCMAXNUM 1024

class IfInstance : public IfObject {

 public:
  IfInstance(if_pid_type = 0, IfQueue* = IfQueue::NIL);
  IfInstance(const IfInstance&);
  virtual ~IfInstance();

 public:
  inline if_pid_type getPid() const
    { return m_pid; }
  inline void setPid(if_pid_type pid) 
    { m_pid = pid;}

 public:
  virtual int getPriority() const
    { return 0; }
  virtual void deliver(const IfMessage*);
  inline IfQueue* GetQueue() const { return m_queue; }
  virtual void RemoveFromQueue(const IfMessage*);
   
 public:
  virtual int isObsSuccess() const { return 0; }
  virtual int isObsError() const { return 0; }
  virtual int isCut() const { return 0; }

 public:
  virtual void copy(const IfInstance*);
  virtual void iterate(IfIterator*);
  virtual void reset();
  
 public:
  DECLARE_STORABLE(IfInstance)
  DECLARE_STORE(IfInstance)

 public:
  enum { CREATE = 1 };

 public:
  static IfIterator* ITERATOR;
  static const char* PROCNAME[PROCMAXNUM];
  static unsigned STATUS;
  static unsigned STEP;

 protected:
  if_pid_type m_pid;
  IfQueue* m_queue;

};


/* 
 *
 * state : defines abstract state informations
 *
 */

enum {
  UNSTABLE = 1,                          // the state is unstable
  TSIG = 2,                       // signal-triggered transitions
  TNONE = 4,                        // none-triggered transitions
  OSUCCESS = 8,                         // observer success state
  OERROR = 16,                          // observer success state
  CONTROL = 64              // the top state of a parallel region
};

template <unsigned L, typename T> 
struct if_state {
  const char* name;                                 // state name
  long unsigned parent;                                // parent state 
  long unsigned entry;                                  // entry state 
  long unsigned flags;                          // stable, queue, none, success, error
  char sigtab[L];                                 // saved, input
  T dispatch;                               // message dispatcher
};
