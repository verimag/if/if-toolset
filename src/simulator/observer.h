/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * IfObserverInstance : base class for user observer instances
 *
 */ 

#define OBSERVER_PRIORITY 0xFFFF

class IfObserverInstance : public IfInstance {

 public:
  IfObserverInstance(if_pid_type = 0, IfQueue* = IfQueue::NIL);
  IfObserverInstance(const IfObserverInstance&);
  virtual ~IfObserverInstance();

 public:
  virtual int getPriority() const
    { return OBSERVER_PRIORITY; }

 public:
  virtual void copy(const IfInstance*);

 public:
  DECLARE_STORABLE(IfInstance)

 protected:
  void clearEventsUpTo(int);
  unsigned if_pid_obs_queue_length(const if_pid_type pid) const;

 protected:
  IfLabel* m_curLabel;
  
 public:
  int trace(const IfEvent* event);
  void flushLabel();
  void cut() { m_bCut = 1; }

  virtual int isCut() const { return m_bCut; }
  IfMessage* if_pid_obs_queue_get_first(if_pid_type pid);
  IfMessage* if_pid_obs_queue_get_last(if_pid_type pid);
  IfMessage* if_pid_obs_queue_get_at(if_pid_type pid, if_integer_type i);
  IfMessage* if_pid_obs_queue_remove_first(if_pid_type pid);
  IfMessage* if_pid_obs_queue_remove_last(if_pid_type pid);
  IfMessage* if_pid_obs_queue_remove_at(if_pid_type pid, if_integer_type i);
  IfMessage* if_obs_input(unsigned sid, if_pid_type pid);
  IfMessage* if_obs_input_ext(unsigned sid, if_pid_type* pid);
  IfMessage* if_obs_discard(unsigned sid, if_pid_type pid);
  IfMessage* if_obs_discard_ext(unsigned sid, if_pid_type* pid);
  IfMessage* if_obs_output(unsigned sid, unsigned index, if_pid_type from, if_pid_type via, if_pid_type to);
  IfMessage* if_obs_output_ext(unsigned sid, unsigned index, if_pid_type* from, if_pid_type* via, if_pid_type* to);
  if_boolean_type if_obs_fork(unsigned process, if_pid_type in);
  void if_obs_fork_ext(unsigned process, if_pid_type* var, if_pid_type* in);
  if_boolean_type if_obs_kill(unsigned process, if_pid_type pid, if_pid_type in);
  void if_obs_kill_ext(unsigned process, if_pid_type* pid, if_pid_type* in);
  IfMessage* if_obs_deliver(unsigned sid, if_pid_type from);
  IfMessage* if_obs_deliver_ext(unsigned sid, if_pid_type* from);
  if_boolean_type if_obs_informal(char* name, if_pid_type in);
  void if_obs_informal_ext(char* name, if_pid_type* in);
  if_boolean_type if_obs_probability();
  if_real_type if_obs_probability_ext();

public:
  long unsigned m_bCut;
  
};

