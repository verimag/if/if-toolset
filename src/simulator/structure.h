/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/*
 *
 * string : defines null-terminated pointer strings
 *
 */

template <class C> 
class IfString {

 public:
  IfString(const int = 0);
  IfString(const IfString<C>&);
  virtual ~IfString();

 public:
  inline C* getAt(const int i) const 
    { return m_objects[i]; }
  inline void setAt(const int i, const C* object)
    { m_objects[i] = (C*) object; }
  int getLength() const;

 public:
  DECLARE_STORABLE(IfString<C>)

 protected:
  C* *m_objects;
  
};

/* 
 *
 * chunk : defines fixed-length pointer chunks 
 *
 */

template <class C, unsigned L> 
class IfChunk {
  
 public:
  IfChunk();
  IfChunk(const IfChunk<C,L>&);
  virtual ~IfChunk();

 public:
  inline C* getAt(const int i) const 
    { return m_objects[i]; }
  inline void setAt(const int i, const C* object)
    { m_objects[i] = (C*) object; }
  inline int getLength() const
    { return (int) L; }

 public:
  // DECLARE_STORABLE(IfChunk<C,L>) 
  virtual int compare(const IfChunk<C,L>*) const;
  virtual unsigned long hash(const unsigned long) const;
  virtual IfChunk<C,L>* copy() const;
  virtual void print(FILE*) const;
  virtual void xmlize(FILE*) const;

 protected:
  C* m_objects[L];

};

/*
 *
 * vector : defines variable-length vectors
 *
 */

template <typename T> 
class IfVector {

 public:
  IfVector(const int = 0);
  IfVector(const IfVector<T>&);
  virtual ~IfVector();
  
 public:
  inline int getLength() const 
    { return m_length; }
  inline T getAt(const int i) const 
    { return m_data[i]; }
  inline void setAt(const int i, const T t) 
    { m_data[i] = t; }

 public:
  int find(const T) const;
  void insert(const int, const T);
  void remove(const int);

 public:
  DECLARE_STORABLE(IfVector<T>);
  void copy(const IfVector<T>*);

 protected:
  long int m_length;
  T* m_data;

};

/*
 *
 * list : defines double-linked dynamic lists
 *
 */

template <class C> 
class IfLinkedList {

 public:
  IfLinkedList();
  virtual ~IfLinkedList();

 public:
  struct Node {
    C* m_object;
    Node* m_prev;
    Node* m_next;
  };

 public:
  inline C* getAt(const Node* node) const 
    { return node->m_object; }
  inline void setAt(Node* node, const C* object) const 
    { node->m_object = object; }

 public:
  inline Node* getFirst() const 
    { return m_first; }
  inline Node* getLast() const 
    { return m_last; }
  inline Node* getNext(const Node* node) const
    { return node->m_next; }
  inline Node* getPrev(const Node* node) const 
    { return node->m_prev; }

 public:
  void insert(const C*, Node* = NULL);
  void append(const C*, Node* = NULL);
  void remove(Node*);
  void purge();
  
 protected:
  Node* m_first;
  Node* m_last;

};

/*
 *
 * heap : defines dynamic heaps
 *
 */

template <class C> 
class IfHeap {
  
 public:
  IfHeap();
  virtual ~IfHeap();
  
 public:
  inline int getCount() const
    { return m_count; }
  
 public:
  C* get();
  void put(const C*);
  
 protected:
  long m_length;
  C* *m_objects;
  
  long int m_count;

};

/*
 *
 * bit vector : defines fixed-length bit vectors
 *
 */

template <unsigned L> 
class IfBitVector {
  
 public:
  IfBitVector();
  IfBitVector(const IfBitVector<L>&);
  virtual ~IfBitVector();

 public:
  int in(const int) const;                             // in test
  void set(const int);                                 // setting
  void reset(const int);                             // resetting

 public:
  unsigned cardinal() const;                          // cardinal
  int ord(const int) const;                      // index ordinal
  int first() const;                          // first zero index
  int last() const;                             // last one index

 public:
  DECLARE_STORABLE(IfBitVector<L>)
  void copy(const IfBitVector<L>*);

 protected:
  unsigned char m_data[L];

};
