/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */

/* 
 *
 * boolean : boolean implementation 
 * 
 */

#define if_boolean_false 0

#define if_boolean_true 1


#define if_boolean_copy(x,y) x=y

#define if_boolean_compare(x,y)  (x)-(y)

#define if_boolean_print(x,f) fprintf(f,"%c",x?'t':'f')

#define if_boolean_print_xml(x,b) b << "<boolean value='" << (x?"true":"false") << "' />"

#define if_boolean_reset(x) x=0

#define if_boolean_iterate(x) for(x=0;x<2;x++)


#define if_boolean_not(x) !(x)

#define if_boolean_and(x,y) (x)&&(y) 

#define if_boolean_or(x,y) (x)||(y)

#define if_boolean_xor(x,y) ((x)&&!(y))||(!(x)&&(y))


#define if_boolean_eq(x,y) (x)==(y)

#define if_boolean_ne(x,y) (x)!=(y)


/*
 *
 * integer : integer implementation 
 *
 */

#define if_integer_copy(x,y) x=y

#define if_integer_compare(x,y)  (x)-(y)

#define if_integer_print(x,f) fprintf(f,"%ld",x)

#define if_integer_print_xml(x,b) b << "<integer value='" << x << "' />"

#define if_integer_reset(x) x=0

// #define if_integer_iterate(x) : undefined


#define if_integer_mult(x,y) (x)*(y)

#define if_integer_div(x,y) (x)/(y)

#define if_integer_mod(x,y) (x)%(y)

#define if_integer_plus(x,y) (x)+(y)

#define if_integer_minus(x,y) (x)-(y)


#define if_integer_uplus(x) +(x)

#define if_integer_uminus(x) -(x)


#define if_integer_lt(x,y) (x)<(y)

#define if_integer_le(x,y) (x)<=(y)

#define if_integer_ge(x,y) (x)>=(y)

#define if_integer_gt(x,y) (x)>(y)


#define if_integer_eq(x,y) (x)==(y)

#define if_integer_ne(x,y) (x)!=(y)


/* 
 *
 * real : real implementation 
 *
 */

#define if_real_copy(x,y) x=y

#define if_real_compare(x,y)  ((x)==(y))?0:((x)<(y))?-1:1

#define if_real_print(x,f) fprintf(f,"%f",x)

#define if_real_print_xml(x,b) b << "<real value='" << x << "' />"

#define if_real_reset(x) x=0.0

// #define if_real_iterate(x) : undefined


#define if_real_mult(x,y) (x)*(y)

#define if_real_div(x,y) (x)/(y)

#define if_real_mod(x,y) (x)%(y)

#define if_real_plus(x,y) (x)+(y)

#define if_real_minus(x,y) (x)-(y)


#define if_real_uplus(x) +(x)

#define if_real_uminus(x) -(x)


#define if_real_lt(x,y) (x)<(y)

#define if_real_le(x,y) (x)<=(y)

#define if_real_ge(x,y) (x)>=(y)

#define if_real_gt(x,y) (x)>(y)


#define if_real_eq(x,y) (x)==(y)

#define if_real_ne(x,y) (x)!=(y)


/* 
 *
 * pid : pid implementation 
 *
 */

#define PROCMASK 0xFFFF0000

#define if_pid_proc(x) (x)>>16

#define if_pid_idet(x) ((x)&0xFFFF)


#define if_pid_nil 0


#define if_pid_copy(x,y) x=y

#define if_pid_compare(x,y) (x)-(y)

#define if_pid_print(x,f) \
  fprintf(f,"{%s}%lu", IfInstance::PROCNAME[if_pid_proc(x)],if_pid_idet(x))

#define if_pid_print_xml(x,b) \
  b << "<pid name='" << IfInstance::PROCNAME[if_pid_proc(x)] << "' no='" << if_pid_idet(x) << "' />"

#define if_pid_sprint(x,s) \
  sprintf(s,"{%s}%lu", IfInstance::PROCNAME[if_pid_proc(x)],if_pid_idet(x))

#define if_pid_reset(x) x=0

#define if_pid_iterate(x) \
    for(x=0; x<1; x++)


#define if_pid_eq(x,y) (x)==(y)

#define if_pid_ne(x,y) (x)!=(y)


#define if_pid_mk(p,x) ((p)<<16)|(x)


/* 
 *
 * clock : clock implementation 
 *
 */

#define if_clock_copy(x,y) x=y

#define if_clock_compare(x,y) (x)-(y)

#define if_clock_print(x,f) fprintf(f,"%ld",x)

#define if_clock_print_xml(x,b) b << "<clock id='" << x << "' />"

#define if_clock_sprint(x,f) sprintf(f,"%ld",x)

#define if_clock_reset(x) \
  if (IfInstance::STATUS & IfInstance::CREATE) \
    x=-1; \
  else \
    IfInstance::ITERATOR->reset(&x)

// #define if_clock_iterate(x) : undefined


/* 
 *
 * void : void implementation 
 *
 */

#define if_void_copy(x,y) x=0

#define if_void_compare(x,y) 0

#define if_void_print(x,f) fprintf(f,"{}")

#define if_void_print_xml(x,b) b << "<void/>"

#define if_void_reset(x) x=0

#define if_void_iterate(x)

#define if_void_eq(x,y) 1

#define if_void_ne(x,y) 0

