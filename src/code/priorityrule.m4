#
#
#  IF-Toolset - Copyright (C) UGA - CNRS - G-INP
#
#  by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
#        Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
# 
#  This  software is  a computer  program whose  purpose is  to simulate,
#  explore and model-check  real-time systems represented using  the IF -
#  intermediate  format  -  language.   This  software  package  contains
#  sources,  documentation,  and  examples.
# 
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the  rules of distribution of free software.   You can use,
#  modify  and/ or  redistribute  the  software under  the  terms of  the
#  CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
#  URL "http://www.cecill.info".
#
#  As a counterpart to the access to  the source code and rights to copy,
#  modify and  redistribute granted  by the  license, users  are provided
#  only with a limited warranty and  the software's author, the holder of
#  the economic  rights, and the  successive licensors have  only limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using, modifying  and/or developing or  reproducing the
#  software by the user in light of its specific status of free software, 
#  that may  mean that  it is  complicated to  manipulate, and  that also
#  therefore means  that it  is reserved  for developers  and experienced
#  professionals having in-depth computer  knowledge. Users are therefore
#  encouraged  to load  and test  the software's  suitability as  regards
#  their  requirements  in  conditions  enabling the  security  of  their
#  systems and/or  data to  be ensured  and, more  generally, to  use and
#  operate it in the same conditions as regards security.
# 
#  The fact that  you are presently reading this means  that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
#



# --------------------------------------------------------------- #
#                                                                 #
# Priority Rule Definitions                                       #
#                                                                 #
# --------------------------------------------------------------- #

# temporarily redefine imp0rt and getStateNo
# --------------------------------------------------------------- #
# priority_rule(procvar1, procvar2, expression, index)            #

define(`priority_rule_begin_impl',`
IfPriorityFilter::Rule* IfPriorityFilter::RULE = new IfPriorityFilter::Rule[$1];
')

define(`priority_rule_impl', 
`
int prio_rule_function_$4(IfConfig* q,if_pid_type $2, if_pid_type $1){
    if($3) {
        return 1;
    } else {
        return 0;
    }
}

IfPriorityFilter::Rule P$4 = IfPriorityFilter::RULE[$4] = &prio_rule_function_$4 ;
')

define(`priority_rule_null_impl',`IfPriorityFilter::Rule P$1 = IfPriorityFilter::RULE[$1] = NULL;
')
define(`priority_rule_begin_decl',`')dnl
define(`priority_rule_decl',`')dnl
define(`priority_rule_null_decl',`')dnl
