#
#
#  IF-Toolset - Copyright (C) UGA - CNRS - G-INP
#
#  by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
#        Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
# 
#  This  software is  a computer  program whose  purpose is  to simulate,
#  explore and model-check  real-time systems represented using  the IF -
#  intermediate  format  -  language.   This  software  package  contains
#  sources,  documentation,  and  examples.
# 
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the  rules of distribution of free software.   You can use,
#  modify  and/ or  redistribute  the  software under  the  terms of  the
#  CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
#  URL "http://www.cecill.info".
#
#  As a counterpart to the access to  the source code and rights to copy,
#  modify and  redistribute granted  by the  license, users  are provided
#  only with a limited warranty and  the software's author, the holder of
#  the economic  rights, and the  successive licensors have  only limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using, modifying  and/or developing or  reproducing the
#  software by the user in light of its specific status of free software, 
#  that may  mean that  it is  complicated to  manipulate, and  that also
#  therefore means  that it  is reserved  for developers  and experienced
#  professionals having in-depth computer  knowledge. Users are therefore
#  encouraged  to load  and test  the software's  suitability as  regards
#  their  requirements  in  conditions  enabling the  security  of  their
#  systems and/or  data to  be ensured  and, more  generally, to  use and
#  operate it in the same conditions as regards security.
# 
#  The fact that  you are presently reading this means  that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
#



# --------------------------------------------------------------- #
#                                                                 #
# Message Definitions                                             #
#                                                                 #
# --------------------------------------------------------------- #


define(`signal_name', `if_$1_signal')            


# --------------------------------------------------------------- #
# message_name(m)                                                 #

define(`message_name', `if_$1_message')            


# --------------------------------------------------------------- #
# message_decl(m,k,(x1,T1,... xn,Tn))                             #

define(`message_decl',`
define(`_MESSAGE_',`message_name($1)')dnl
/* 
 * $1 message interface 
 *
 */

`#define' signal_name($1) $2

type_decl(record,$1_par,$3)

class _MESSAGE_ : public IfMessage {

public:
  ifelse($3,(),,_MESSAGE_`'();)
  _MESSAGE_`'(var_par_decl$3);
  _MESSAGE_`'(const _MESSAGE_&);

  virtual int `compare'(const IfMessage*) const;
  virtual unsigned long `hash'(const unsigned long) const;
  virtual IfMessage* `copy'() const;
  virtual void `print'(FILE*) const;
  virtual void `printXML'(std::ostream&) const;

public:
  static const char* NAME;

public:
  declare(m_par,$1_par);

public:
  virtual int GetParSize() const { return sizeof(m_par); }
  virtual void* GetParAddress() const { return (void*)&m_par; }

};
')


# --------------------------------------------------------------- #
# message_impl(m,k,(x1,T1,... xn,Tn))                             #

define(`message_impl',`
define(`_MESSAGE_',`message_name($1)')dnl
/*
 * $1 message implementation 
 *
 */

type_impl(record,$1_par,$3)

const char* _MESSAGE_::NAME = IfMessage::SIGNAME[$2] = "$1";

ifelse($3,(),,_MESSAGE_::_MESSAGE_`'() 
  : IfMessage(signal_name($1)) {
  reset($1_par,m_par);
}
)
_MESSAGE_::_MESSAGE_`'(var_par_decl$3) 
  : IfMessage(signal_name($1)) {
  ifelse($3,(),reset($1_par,m_par);,par_cin$3)
}

_MESSAGE_::_MESSAGE_`'(const _MESSAGE_& message) 
  : IfMessage(message) {
  copy($1_par,m_par,message.m_par);
}

int _MESSAGE_::`compare'(const IfMessage* X) const {
  _MESSAGE_* x = (_MESSAGE_*)X;
  int cmp = IfMessage::`compare'(X);
  if (cmp == 0) cmp = compare($1_par,m_par,x->m_par);
  return cmp;
}

unsigned long _MESSAGE_::`hash'(const unsigned long base) const {
  unsigned long key = IfMessage::`hash'(base);
  if (sizeof(m_par) >= 4) // void
    key += 17 * if_hash((char*) &m_par, sizeof(m_par), base);
  return key % base;
}

IfMessage* _MESSAGE_::`copy'() const {
  return new _MESSAGE_`'(*this);
}

void _MESSAGE_::`print'(FILE* f) const {
  fprintf(f, "%s", NAME);
  print($1_par,m_par,f);
  IfMessage::`print'(f);
}

void _MESSAGE_::`printXML'(std::ostream& buf) const {
  buf << "<IfMessage ";
  buf << "type=\"" << NAME << "\">\n";	    

  buf << "<par>\n";	    
  printXML($1_par,m_par,buf);
  buf << "</par>\n";	    

  IfMessage::`printXML'(buf);
  buf << "</IfMessage>\n";
}
') 
