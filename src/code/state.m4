#
#
#  IF-Toolset - Copyright (C) UGA - CNRS - G-INP
#
#  by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
#        Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
# 
#  This  software is  a computer  program whose  purpose is  to simulate,
#  explore and model-check  real-time systems represented using  the IF -
#  intermediate  format  -  language.   This  software  package  contains
#  sources,  documentation,  and  examples.
# 
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the  rules of distribution of free software.   You can use,
#  modify  and/ or  redistribute  the  software under  the  terms of  the
#  CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
#  URL "http://www.cecill.info".
#
#  As a counterpart to the access to  the source code and rights to copy,
#  modify and  redistribute granted  by the  license, users  are provided
#  only with a limited warranty and  the software's author, the holder of
#  the economic  rights, and the  successive licensors have  only limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using, modifying  and/or developing or  reproducing the
#  software by the user in light of its specific status of free software, 
#  that may  mean that  it is  complicated to  manipulate, and  that also
#  therefore means  that it  is reserved  for developers  and experienced
#  professionals having in-depth computer  knowledge. Users are therefore
#  encouraged  to load  and test  the software's  suitability as  regards
#  their  requirements  in  conditions  enabling the  security  of  their
#  systems and/or  data to  be ensured  and, more  generally, to  use and
#  operate it in the same conditions as regards security.
# 
#  The fact that  you are presently reading this means  that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
#



# --------------------------------------------------------------- #
#                                                                 #
# State Definitions                                               #
#                                                                 #
# --------------------------------------------------------------- #

# --------------------------------------------------------------- #
# dispatch_name(state)                                            #

define(`dispatch_name', `_$1_dispatch')

# --------------------------------------------------------------- #
# message dispatcher                                              #
#                                                                 #
# dispatch(state, parent,                                         #
#	        (sig1,tr11,... tr1n),                             #
#               (sig2,tr21,... tr2m),                             #
#	                 ...                                      #
#               (sigk,trk1,... trkp),                             #
#               (,tr1,... trj))                         	  #

define(`dispatch_option',
`ifelse($1,,,`fire_name(_STATE_,$1)(message);
      ITERATOR->restore();
      dispatch_option(shift($@))')')

define(`dispatch_case',
`ifelse($1,, `}
  else {
    dispatch_option(shift($@))',
    `case signal_name($1):
      dispatch_option(shift($@))break;')')

define(`dispatch_case_list', 
`ifelse($1,,,`dispatch_case$1
    dispatch_case_list(shift($@))')')

# ------

define(`dispatch_decl', 
`         void dispatch_name($1)(IfMessage*);')

define(`dispatch_impl', 
`define(`_STATE_', `$1')dnl
void _INSTANCE_::dispatch_name($1)(IfMessage* message) {
  if (message != NULL)
    switch(message->getSid()) {
    dispatch_case_list(shift(shift($@)))}
  ifelse($2,,,dispatch_name($2)(message);)
}')

