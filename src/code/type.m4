#
#
#  IF-Toolset - Copyright (C) UGA - CNRS - G-INP
#
#  by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
#        Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
# 
#  This  software is  a computer  program whose  purpose is  to simulate,
#  explore and model-check  real-time systems represented using  the IF -
#  intermediate  format  -  language.   This  software  package  contains
#  sources,  documentation,  and  examples.
# 
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the  rules of distribution of free software.   You can use,
#  modify  and/ or  redistribute  the  software under  the  terms of  the
#  CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
#  URL "http://www.cecill.info".
#
#  As a counterpart to the access to  the source code and rights to copy,
#  modify and  redistribute granted  by the  license, users  are provided
#  only with a limited warranty and  the software's author, the holder of
#  the economic  rights, and the  successive licensors have  only limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using, modifying  and/or developing or  reproducing the
#  software by the user in light of its specific status of free software, 
#  that may  mean that  it is  complicated to  manipulate, and  that also
#  therefore means  that it  is reserved  for developers  and experienced
#  professionals having in-depth computer  knowledge. Users are therefore
#  encouraged  to load  and test  the software's  suitability as  regards
#  their  requirements  in  conditions  enabling the  security  of  their
#  systems and/or  data to  be ensured  and, more  generally, to  use and
#  operate it in the same conditions as regards security.
# 
#  The fact that  you are presently reading this means  that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
#



define(`last_item',`ifelse($2,,$1,`last_item(shift($@))')')

# --------------------------------------------------------------- #
#                                                                 #
# Type Definitions                                                #
#                                                                 #
# --------------------------------------------------------------- #


define(`type_name', `if_$1_type')                  # type_name(T) #


# ----------------------------------------------------------------#
# Common Interfaces                                               #
# ----------------------------------------------------------------#

define(`copy_func', `if_$1_copy')
define(`copy',`copy_func($1)($2,$3)')               # copy(T,x,y) #

define(`compare_func', `if_$1_compare')
define(`compare',`compare_func($1)($2,$3)')      # compare(T,x,y) #

define(`print_func', `if_$1_print')
define(`print',`print_func($1)($2,$3)')            # print(T,x,f) #

define(`print_func_xml', `if_$1_print_xml')
define(`printXML',`print_func_xml($1)($2,$3)')     # printXML(T,x,f) #

define(`iterate_func', `if_$1_iterate')
define(`iterate',`iterate_func($1)($2)')           # iterate(T,x) #

define(`reset_func', `if_$1_reset')
define(`reset',`reset_func($1)($2)')                 # reset(T,x) #


# ----------------------------------------------------------------#
# Specific Implementations                                        #
# ----------------------------------------------------------------#

define(`type_decl',
`ifelse($1,enum,`enum_type_decl(shift($@))',
	$1,range,`range_type_decl(shift($@))',
	$1,sig_union,`sig_union_type_decl(shift($@))',
	$1,record,`record_type_decl(shift($@))',
	$1,array,`array_type_decl(shift($@))',
	$1,string,`string_type_decl(shift($@))',
	$1,tree,`tree_type_decl(shift($@))',
	$1,abstract,`abstract_type_decl(shift($@))',
	$1,other,`other_type_decl(shift($@))',)
`#define' if_$2_eq(x,y) compare($2,x,y)==0
`#define' if_$2_ne(x,y) compare($2,x,y)!=0')

define(`type_impl',
`ifelse($1,enum,`enum_type_impl(shift($@))',
	$1,range,`range_type_impl(shift($@))',
	$1,sig_union,`sig_union_type_impl(shift($@))',
	$1,record,`record_type_impl(shift($@))',
	$1,array,`array_type_impl(shift($@))',
	$1,string,`string_type_impl(shift($@))',
	$1,tree,`tree_type_impl(shift($@))',
	$1,abstract,`abstract_type_impl(shift($@))',
	$1,other,`other_type_impl(shift($@))',)')

# ----------------------------------------------------------------#
# Enum Type                                                       #
# ----------------------------------------------------------------#

# enum_constant(e1,... en)                                        #

define(`enum_constant',
`ifelse($1$2,,,
	$2,,constant_name($1),
	`constant_name($1),
  enum_constant(shift($@))')')

# enum_type_decl(T,e1,... en)                                     #

define(`enum_type_decl', `
enum type_name($1) : long {
  enum_constant(shift($@))
};

extern const char* if_$1_name[];

`#define' copy_func($1)(x,y) (x)=(y)
`#define' compare_func($1)(x,y) (x)-(y)
`#define' print_func($1)(x,f) fprintf(f,"%s",if_$1_name[x])
`#define' print_func_xml($1)(x,b) b << "<$1 value=\"" << if_$1_name[x] << "\" />"
`#define' reset_func($1)(x) (x)=(type_name($1))0
`#define' iterate_func($1)(x) for(x=constant_name($2);x<=constant_name(last_item($@));x=(type_name($1))(x+1))')

# enum_quote(e1,... en)                                           #

define(`enum_quote',
`ifelse($1$2,,,
	$2,,"$1",
	`"$1",
  enum_quote(shift($@))')')

# enum_type_impl(T,e1,... en)                                     #

define(`enum_type_impl', `
const char* if_$1_name[] = {
  enum_quote(shift($@))
};')


# ----------------------------------------------------------------#
# Range Type                                                      #
# ----------------------------------------------------------------#

# range_type_decl(T,T1,l,u,s)

define(`range_type_decl', `
typedef type_name($2) type_name($1);

`#define' copy_func($1)(x,y) copy_func($2)(x,y)
`#define' compare_func($1)(x,y) compare_func($2)(x,y)
`#define' print_func($1)(x,f) print_func($2)(x,f)
`#define' print_func_xml($1)(x,b) print_func_xml($2)(x,b)
`#define' reset_func($1)(x) (x)=$3
`#define' iterate_func($1)(x) for(x=$3;x<=$4;x=(type_name($1))(x+$5))')

# range_type_impl(T,T1,l,u)

define(`range_type_impl', `')


# ----------------------------------------------------------------#
# Record Type                                                     #
# ----------------------------------------------------------------#

# record_type(T,(x1,T1... xn,Tn))                                 #

define(`record_type_decl',
`ifelse($2,,`void_record_type_decl($1)',
	$2,(),`void_record_type_decl($1)',
	`full_record_type_decl($1,$2)')')

define(`record_type_impl',
`ifelse($2,,`void_record_type_impl($1)',
	$2,(),`void_record_type_impl($1)',
	`full_record_type_impl($1,$2)')')


# void_record_type(T)                                             #

define(`void_record_type_decl', `
other_type_decl($1,void)')

define(`void_record_type_impl', `
other_type_impl($1,void)')


# full_record_type(T,(x1,T1... xn,Tn))                            #

define(`full_record_type_decl', `
typedef struct {
  var_var_decl$2
} type_name($1);

inline void copy_func($1)
    (type_name($1)& x, const type_name($1) y);
inline int compare_func($1)
    (const type_name($1) x, const type_name($1) y);
inline void print_func($1)
    (const type_name($1) x, FILE* f);
inline void reset_func($1)
    (type_name($1)& x);
`#define' iterate_func($1)(_x_)\
  var_iterate$2
inline type_name($1) if_$1_make
    (var_par_decl$2);')

define(`full_record_type_impl', `
inline void copy_func($1)
    (type_name($1)& x, const type_name($1) y) {
  var_copy$2
}
inline int compare_func($1)
    (const type_name($1) x, const type_name($1) y) {
  int c = 0;
  var_compare$2
  return c;
}
inline void print_func($1)
    (const type_name($1) x, FILE* f) { 
  fprintf(f,"{");
  var_print$2
  fprintf(f,"}");
}
inline void print_func_xml($1)
    (const type_name($1) x, std::ostream& b) { 
  b << "<$1>\n";
  var_print_xml$2
  b << "</$1>\n";
}
inline void reset_func($1)
    (type_name($1)& x) {
  var_reset$2
}

inline type_name($1) if_$1_make
    (var_par_decl$2) {
  type_name($1) m_par;
  par_cin$2
  return m_par;
}')

# ----------------------------------------------------------------#
# SignalUnion Type                                                     #
# ----------------------------------------------------------------#

# sig_union_type(T,(x1,T1... xn,Tn))                                 #

define(`sig_union_type_decl', `
typedef IfMessage* type_name($1);
')

define(`sig_union_type_impl', `
inline void copy_func($1)
    (type_name($1)& x, const type_name($1) y) {
  x = y;
}
inline int compare_func($1)
    (const type_name($1) x, const type_name($1) y) {
  int c = x - y;
  if(c || x == NULL) return c;
  return x->`compare'(y);
}
inline void print_func($1)
    (const type_name($1) x, FILE* f) { 
  fprintf(f,"{");
  if(x) x->`print'(f);
  else fprintf(f,"nil");
  fprintf(f,"}");
}
inline void print_func_xml($1)
    (const type_name($1) x, std::ostream& b) { 
  b << "<sigunion>\n";
  if(x) x->`printXML'(b);
  b << "</sigunion>\n";
}
inline void reset_func($1)
    (type_name($1)& x) {
  x = NULL;
}
')

# ----------------------------------------------------------------#
# Array Type                                                      #
# ----------------------------------------------------------------#

define(`array_iterate',
`ifelse($2,0,`  iterate_func($1)(x[$2])', `  iterate_func($1)(x[$2])\
array_iterate($1,decr($2))')')

# array_type_decl(T,T1,n,k) k=(int)n

define(`array_type_decl', `
typedef type_name($2) type_name($1)[$3];

inline void copy_func($1)
    (type_name($1)& x, const type_name($1) y);
inline int compare_func($1)
    (const type_name($1) x, const type_name($1) y);
inline void print_func($1)
    (const type_name($1) x, FILE* f);
inline void reset_func($1)
    (type_name($1)& x);
inline int if_$1_length
    (const type_name($1) x);
int if_$1_`index'
    (const type_name($1) x, const type_name($2) e);
inline type_name(boolean) if_$1_in
    (const type_name($2) e, const type_name($1) x);
`#define' iterate_func($1)(x)\
array_iterate($2,decr($4))')

# array_type_decl(T,T1,n,k) k=(int)n

define(`array_type_impl', `
inline void copy_func($1)
    (type_name($1)& x, const type_name($1) y) {
  for(int i=0;i<$3;i++)
    copy_func($2)(x[i],y[i]);
}
inline int compare_func($1)
    (const type_name($1) x, const type_name($1) y) {
  int c = 0;
  for(int i=0;i<$3 && !c;i++)
    c = compare_func($2)(x[i],y[i]);
  return c;
}
inline void print_func($1)
    (const type_name($1) x, FILE* f) { 
  fprintf(f,"{");
  for(int i=0;i<$3;i++) {
    print_func($2)(x[i],f);
    fprintf(f,",");
  }
  fprintf(f,"}");
}
inline void print_func_xml($1)
    (const type_name($1) x, std::ostream& b) { 
  b << "<array>\n";
  for(int i=0;i<$3;i++) {
    print_func_xml($2)(x[i],b);
  }
  b << "</array>\n";
}
inline void reset_func($1)
    (type_name($1)& x) {
  for(int i=0;i<$3;i++)
    reset_func($2)(x[i]);
}
inline int if_$1_length
    (const type_name($1)) {
  return $3;
}
int if_$1_`index'
    (const type_name($1) x, const type_name($2) e) {
  int idx = -1;
  for(int i=0;i<$3 && idx<0;i++)
    if (compare_func($2)(x[i],e)==0)
      idx = i;
  return idx;
}
inline type_name(boolean) if_$1_in
    (const type_name($2) e, const type_name($1) x) {
  return if_$1_`index'(x,e)==-1 ? if_boolean_false : if_boolean_true;
}')


# ----------------------------------------------------------------#
# String Type                                                     #
# ----------------------------------------------------------------#

# string_type_decl(T,T1,n)

define(`string_type_decl', `
struct type_name($1) {
  long int length;
  type_name($2) elem[$3];

  type_name($2)& operator[](const int i) 
    { return elem[i]; }
};

inline void copy_func($1)
    (type_name($1)& x, const type_name($1) y);
inline int compare_func($1)
    (const type_name($1) x, const type_name($1) y);
inline void print_func($1)
    (const type_name($1) x, FILE* f);
inline void reset_func($1)
    (type_name($1)& x);
inline int if_$1_length
    (const type_name($1) x);
type_name($1) if_$1_make
    ();
type_name($1) if_$1_make
    (const type_name($2) e);
type_name($1) if_$1_concat
    (const type_name($1) x, const type_name($1) y);
type_name($1) if_$1_substring
    (const type_name($1) x, const int k, const int l);
type_name($1) if_$1_remove
    (const type_name($1) x, const int k);
type_name($1) if_$1_insert
    (const type_name($1) x, const int k, const type_name($2) e);
type_name(boolean) if_$1_in
    (const type_name($2) e, const type_name($1) x);')

# string_type_impl(T,T1,n)

define(`string_type_impl', `
inline void copy_func($1)
    (type_name($1)& x, const type_name($1) y) {
  reset_func($1)(x);
  for(int i=0;i<y.length;i++)
    copy_func($2)(x.elem[i],y.elem[i]);
  x.length=y.length;
}
inline int compare_func($1)
    (const type_name($1) x, const type_name($1) y) {
  int c = x.length-y.length;
  for(int i=0;i<x.length && !c;i++)
    c = compare_func($2)(x.elem[i],y.elem[i]);
  return c;
}
inline void print_func($1)
    (const type_name($1) x, FILE* f) {
  fprintf(f,"{");
  for(int i=0;i<x.length;i++) {
    print_func($2)(x.elem[i],f);
    fprintf(f,",");
  }
  fprintf(f,"}");
}
inline void print_func_xml($1)
    (const type_name($1) x, std::ostream& b) {
  b << "<string>\n";
  for(int i=0;i<x.length;i++) {
    print_func_xml($2)(x.elem[i],b);
  }
  b << "</string>\n";
}
inline void reset_func($1)
    (type_name($1)& x) {
  x.length=0;	
  for(int i=0;i<$3;i++)
    reset_func($2)(x.elem[i]);
}
inline int if_$1_length
    (const type_name($1) x) {
  return x.length;
}
type_name($1) if_$1_make
    () {
  type_name($1) z;
  reset_func($1)(z);
  return z;
}
type_name($1) if_$1_make
    (const type_name($2) e) {
  type_name($1) z;
  reset_func($1)(z);
  if (0<$3) {
    copy_func($2)(z.elem[0],e);
    z.length=1;
  }
  return z;
}
type_name($1) if_$1_concat
    (const type_name($1) x, const type_name($1) y) {
  type_name($1) z;
  reset_func($1)(z);
  int i;

  for(i=0;i<x.length && z.length<$3;i++,z.length++) 
    copy_func($2)(z.elem[z.length],x.elem[i]);
  for(i=0;i<y.length && z.length<$3;i++,z.length++)
    copy_func($2)(z.elem[z.length],y.elem[i]);
  return z;
}
type_name($1) if_$1_substring
    (const type_name($1) x, const int k, const int l) {
  type_name($1) z;
  reset_func($1)(z);
  for(int i=k;i<=l && 
              i<x.length && z.length<$3; i++,z.length++)
    copy_func($2)(z.elem[z.length], x.elem[i]);
  return z;
}
type_name($1) if_$1_remove
    (const type_name($1) x, const int k) {
  type_name($1) z;
  reset_func($1)(z);
  if (0<=k && k<x.length) 
    for(int i=0;i<x.length;i++,z.length++) {
      copy_func($2)(z.elem[z.length],x.elem[i]);
      if (i==k) z.length--;
    }
  else
    copy_func($1)(z,x);
  return z;
}
type_name($1) if_$1_insert
    (const type_name($1) x, const int k, const type_name($2) e) {
  type_name($1) z;
  reset_func($1)(z);
  if (0<=k && k<=x.length && x.length<$3) {
    int i;
    for(i=0;i<k;i++,z.length++) 
      copy_func($2)(z.elem[z.length],x.elem[i]);
    copy_func($2)(z.elem[z.length++],e);
    for(i=k;i<x.length;i++,z.length++) 
      copy_func($2)(z.elem[z.length],x.elem[i]);
  }
  else 
    copy_func($1)(z,x);
  return z;
}
int if_$1_`index'
    (const type_name($1) x, const type_name($2) e) {
  int idx = -1;
  for(int i=0;i<x.length && idx<0;i++)
    if (compare_func($2)(x.elem[i],e)==0)
      idx = i;
  return idx;
}
inline type_name(boolean) if_$1_in
    (const type_name($2) e, const type_name($1) x) {
  return if_$1_`index'(x,e) == -1 ? if_boolean_false : if_boolean_true;
}
')


# ----------------------------------------------------------------#
# Tree Type                                                       #
# ----------------------------------------------------------------#

# tree_type_decl(T,T1,r,h)

define(`tree_type_decl', `
/* tree type : not yet implemented */
')

# tree_type_impl(T,T1,r,h)

define(`tree_type_impl', `
/* tree type : not yet implemented */
')


# ----------------------------------------------------------------#
# Abstract Type                                                   #
# ----------------------------------------------------------------#

define(`function_name', `if_$1_function')      # function_name(C) #

# function_decl(f,(x1,T1,...xn,Tn),Tr)                            #

define(`function_decl',`type_name($3) function_name($1)(var_par_decl$2);')

# function_impl(f,(x1,T1,...xn,Tn),Tr)                            #

define(`function_impl',`')



# abstract_type_decl(T)                                           #

define(`abstract_type_decl', `
`#ifndef' ABSTRACT
typedef char* type_name($1);

`#define' copy_func($1)(x,y) (x)=(y)
`#define' compare_func($1)(x,y) (x)-(y)
`#define' print_func($1)(x,f) fprintf(f,"%#x",(unsigned)x)
`#define' print_func_xml($1)(x,b) b << "<$1 value=\"" << x << "\" />"
`#define' reset_func($1)(x) (x)=NULL
`#else'
`#define' __$1__
`#include' "_FILE_.i"
`#undef' __$1__
`#endif'')

# abstract_type_impl(T)                                           #

define(`abstract_type_impl', `')


# ----------------------------------------------------------------#
# Other Type                                                      #
# ----------------------------------------------------------------#

# other_type_decl(T1,T2)                                          #

define(`other_type_decl', `
typedef type_name($2) type_name($1);

`#define' copy_func($1)(x,y) copy_func($2)(x,y)
`#define' compare_func($1)(x,y) compare_func($2)(x,y)
`#define' print_func($1)(x,f) print_func($2)(x,f)
`#define' print_func_xml($1)(x,b) print_func_xml($2)(x,b)
`#define' reset_func($1)(x) reset_func($2)(x)
`#define' iterate_func($1)(x) iterate_func($2)(x)') 

# other_type_impl(T1,T2)                                          #

define(`other_type_impl', `')
