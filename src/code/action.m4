#
#
#  IF-Toolset - Copyright (C) UGA - CNRS - G-INP
#
#  by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
#        Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
# 
#  This  software is  a computer  program whose  purpose is  to simulate,
#  explore and model-check  real-time systems represented using  the IF -
#  intermediate  format  -  language.   This  software  package  contains
#  sources,  documentation,  and  examples.
# 
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the  rules of distribution of free software.   You can use,
#  modify  and/ or  redistribute  the  software under  the  terms of  the
#  CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
#  URL "http://www.cecill.info".
#
#  As a counterpart to the access to  the source code and rights to copy,
#  modify and  redistribute granted  by the  license, users  are provided
#  only with a limited warranty and  the software's author, the holder of
#  the economic  rights, and the  successive licensors have  only limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using, modifying  and/or developing or  reproducing the
#  software by the user in light of its specific status of free software, 
#  that may  mean that  it is  complicated to  manipulate, and  that also
#  therefore means  that it  is reserved  for developers  and experienced
#  professionals having in-depth computer  knowledge. Users are therefore
#  encouraged  to load  and test  the software's  suitability as  regards
#  their  requirements  in  conditions  enabling the  security  of  their
#  systems and/or  data to  be ensured  and, more  generally, to  use and
#  operate it in the same conditions as regards security.
# 
#  The fact that  you are presently reading this means  that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
#



# ----------------------------------------------------------------#
#                                                                 #
# Action Definitions                                              #
#                                                                 #
# ----------------------------------------------------------------#


# skip                                                            #

define(`skip_body', `')

# informal                                                        #

define(`informal_body', `dnl
  ITERATOR->trace(IfEvent::INFORMAL, m_pid, $1, m_pid, (IfObject*)$1);')

# task                                                            #

define(`task_body',`dnl
  copy($1,$2,$3);')

# set                                                             #

define(`set_body', `dnl
  ITERATOR->set(&$1,$2,m_pid);')

# reset                                                           #

define(`reset_body', `dnl
  reset($1,$2);')

# input                                                           #

define(`input_body', `dnl
  message_name($1)* x = (message_name($1)*) X;
  par_cout$2
  ITERATOR->trace(IfEvent::INPUT, m_pid, x->string(), m_pid, x->store());')

# output                                                          #

define(`output_body', `dnl
  message_name($1) x`'ifelse($2,(),,(expr$2));
  ifelse($3,0,,x.setDestination($4);)
  ITERATOR->trace(IfEvent::OUTPUT, m_pid, x.string(), ifelse($3,0,$4,$3), x.store()); // anticipate the store. problem???
  ITERATOR->deliver(ifelse($3,0,$4,$3), &x);')

# acquire

define(`acquire_body', `dnl
  int resources[] = {`resource_list($@)'}; 
  int enabled = ITERATOR->acquire(m_pid,resources);
  if (enabled)
    ITERATOR->trace(IfEvent::ACQUIRE,m_pid,"{$@}");
  return enabled;')

# release

define(`release_body', `dnl
  int resources[] = {`resource_list($@)'};
  ITERATOR->release(m_pid,resources);
  ITERATOR->trace(IfEvent::RELEASE,m_pid,"{$@}");')

# fork                                                            #

define(`fork_body', `dnl
  ifelse($4,,type_name(pid) y;,)
  ifelse($1,instance,instance_name($2),
	$1,buffer,buffer_name($2),) x`'ifelse($3,(),,(expr$3));
  ITERATOR->fork(&x, &ifelse($4,,y,$4));
  ITERATOR->trace(IfEvent::FORK, m_pid, "$2", ifelse($4,,y,$4));') 

# kill                                                            #

define(`kill_body', `dnl
  unsigned pid = $1;
  ITERATOR->trace(IfEvent::KILL, m_pid, "", pid);
  if (m_pid != pid)
    ITERATOR->kill(pid);')

# call                                                            #

define(`call_body',`dnl
  ifelse($3,,,$3 = )procedure_name($1)(expr$2);
  ITERATOR->trace(IfEvent::CALL, m_pid, "$1");') 
# warning -- use copy(T)($3,...) instead of $3 = ...

# nextstate                                                       #

define(`nextstate_body',`dnl
  ctl_assign$1
  nextstate();')

# stop                                                            #

define(`stop_body',`dnl
  ITERATOR->kill(m_pid);
  ITERATOR->step();')

# flush                                                           #

define(`flush_body', `dnl
  IfObserverInstance::flushLabel();')

# cut                                                             #

define(`cut_body', `dnl
  IfObserverInstance::cut();')

# matchinput(name, (var, par, type, ...), sigvar, inpid)                  #

define(`matchinput_body', `ifelse($1,,`dnl
  IfMessage* x = if_obs_input_ext((unsigned)-1, $4) ;
ifelse($3,,,`dnl
  $3 = x;')',
  `dnl    
  message_name($1)* x = (message_name($1)*)if_obs_input_ext(signal_name($1), $4) ;
  par_cout$2
')')


# matchdiscard(name, (var, par, type, ...), sigvar, inpid)                  #

define(`matchdiscard_body', `ifelse($1,,`dnl
  IfMessage* x = if_obs_discard_ext((unsigned)-1, $4) ;
ifelse($3,,,`dnl
  $3 = x;')',
  `dnl    
  message_name($1)* x = (message_name($1)*)if_obs_discard_ext(signal_name($1), $4) ;
  par_cout$2
')')


# matchoutput(name, (var, par, type, ...), ord, sigvar, from, via, to)    #

define(`matchoutput_body', `ifelse($1,,`dnl
  IfMessage* x = if_obs_output_ext((unsigned)-1, $3, $5, $6, $7) ;
ifelse($4,,,`dnl
  $4 = x;')',
  `dnl    
  message_name($1)* x = (message_name($1)*)if_obs_output_ext(signal_name($1), $3, $5, $6, $7) ;
  par_cout$2')')


# matchfork(processname, pid, in)					    #

define(`matchfork_body', `dnl
  if_obs_fork_ext(ifelse($1,,(unsigned)-1,process_name($1)), $2, $3) ;
')


# matchkill(processname, pid, in)					    #

define(`matchkill_body', `dnl
  if_obs_kill_ext(ifelse($1,,(unsigned)-1,process_name($1)), $2, $3) ;
')


# matchdeliver(name, (var, par, type, ...), sigvar, from)		    #

define(`matchdeliver_body', `ifelse($1,,`dnl
  IfMessage* x = if_obs_deliver_ext(-1, $4) ;
ifelse($3,,,`dnl
  $3 = x;')',
  `dnl    
  message_name($1)* x = (message_name($1)*)if_obs_deliver_ext(signal_name($1), $4) ;
  par_cout$2')')


# matchinformal(text, in)						    #

define(`matchinformal_body', `dnl
  if_obs_informal_ext($1,$2) ;
')


# matchprobability(pvar)						    #

define(`matchprobability_body', `dnl
  ifelse($1,,,$1 = )if_obs_probability_ext() ;
')

# --------------------------------------------------------------- #

define(`action_body',
`ifelse($1,skip,skip_body(shift($@)),
       	$1,informal,informal_body(shift($@)),
       	$1,task,task_body(shift($@)),
       	$1,set,set_body(shift($@)),
       	$1,reset,reset_body(shift($@)),
       	$1,input,input_body(shift($@)),
       	$1,output,output_body(shift($@)),
        $1,acquire,acquire_body(shift($@)),
        $1,release,release_body(shift($@)),
       	$1,fork,fork_body(shift($@)),
       	$1,kill,kill_body(shift($@)),
       	$1,call,call_body(shift($@)),
       	$1,flush,flush_body(shift($@)),
       	$1,cut,cut_body(shift($@)),
       	$1,matchinput,matchinput_body(shift($@)),
       	$1,matchdiscard,matchdiscard_body(shift($@)),
       	$1,matchoutput,matchoutput_body(shift($@)),
       	$1,matchfork,matchfork_body(shift($@)),
       	$1,matchkill,matchkill_body(shift($@)),
       	$1,matchdeliver,matchdeliver_body(shift($@)),
       	$1,matchinformal,matchinformal_body(shift($@)),
       	$1,matchprobability,matchprobability_body(shift($@)),
       	$1,nextstate,nextstate_body(shift($@)),
       	$1,stop,stop_body(shift($@)),)')

# --------------------------------------------------------------- #

define(`action_decl', 
`  inline ifelse($4,acquire,int ,void) fire_name($1,$2$3)(ifelse($4,input,IfMessage*,
					    $4,matchinput,IfMessage* X,
					    $4,matchdiscard,IfMessage* X,
					    $4,matchoutput,IfMessage* X,
					    $4,matchdeliver,IfMessage* X,
					    $4,matchinformal,IfMessage* X,
					    $4,matchfork,IfMessage* X,
					    $4,matchkill,IfMessage* X,
					    $4,matchprobability,IfMessage* X,));')

define(`action_impl', 
`inline ifelse($4,acquire,int ,void) _INSTANCE_::fire_name($1,$2$3)(ifelse($4,input,IfMessage* X,
						    $4,matchinput,IfMessage* X,
						    $4,matchdiscard,IfMessage* X,
						    $4,matchoutput,IfMessage* X,
						    $4,matchdeliver,IfMessage* X,
						    $4,matchinformal,IfMessage* X,
						    $4,matchfork,IfMessage* X,
						    $4,matchkill,IfMessage* X,
						    $4,matchprobability,IfMessage* X,)) {

action_body(shift(shift(shift($@))))
}')
