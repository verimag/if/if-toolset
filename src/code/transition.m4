#
#
#  IF-Toolset - Copyright (C) UGA - CNRS - G-INP
#
#  by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
#        Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
# 
#  This  software is  a computer  program whose  purpose is  to simulate,
#  explore and model-check  real-time systems represented using  the IF -
#  intermediate  format  -  language.   This  software  package  contains
#  sources,  documentation,  and  examples.
# 
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the  rules of distribution of free software.   You can use,
#  modify  and/ or  redistribute  the  software under  the  terms of  the
#  CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
#  URL "http://www.cecill.info".
#
#  As a counterpart to the access to  the source code and rights to copy,
#  modify and  redistribute granted  by the  license, users  are provided
#  only with a limited warranty and  the software's author, the holder of
#  the economic  rights, and the  successive licensors have  only limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using, modifying  and/or developing or  reproducing the
#  software by the user in light of its specific status of free software, 
#  that may  mean that  it is  complicated to  manipulate, and  that also
#  therefore means  that it  is reserved  for developers  and experienced
#  professionals having in-depth computer  knowledge. Users are therefore
#  encouraged  to load  and test  the software's  suitability as  regards
#  their  requirements  in  conditions  enabling the  security  of  their
#  systems and/or  data to  be ensured  and, more  generally, to  use and
#  operate it in the same conditions as regards security.
# 
#  The fact that  you are presently reading this means  that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
#



# --------------------------------------------------------------- #
#                                                                 #
# Transition Definitions                                          #
#                                                                 #
# --------------------------------------------------------------- #


# --------------------------------------------------------------- #
# fire_name(state,index)

define(`fire_name', `_$1_$2_fire')

# ----------------------------------------------------------------#
# fire(state,index,deadline                                       #
#      acquire,                                                   #
#      discrete-guard,(timed-constraints)                         #
#      input,body,terminator,priority,probability,lineno)         #

define(`write_constraints',
`IfTime::Constraint constraints[] = 
    {$@};')

# ------ 

define(`fire_decl',
`         void fire_name($1,$2)(IfMessage*);')

define(`fire_impl',
`void _INSTANCE_::fire_name($1,$2)(IfMessage* X) {
define(`_STATE_',`$1')dnl
define(`_TRANS_',`$2')dnl
  ifelse($6,,,`write_constraints$6')
  ifelse($5,,,`if (! ($5))
    return;')
  ifelse($4,,,`if (! fire_name($1,$2$4)())
    return;')
  ifelse($6,,,`if (! ITERATOR->guard(constraints,$3))
    return;') 
  if (ifelse($6,,,!constraints[0].isEmpty() || )$3!=EAGER) 
    if(getPriority() != OBSERVER_PRIORITY) ITERATOR->trace(IfEvent::DELTA, m_pid, "");
  ifelse($10,,,`ITERATOR->setPriority($10);')
  ifelse($12,,,`if (opt_gen_lineno) ITERATOR->trace(IfEvent::DEBUG, m_pid, "$12");') 
  ifelse($7,,,fire_name($1,$2$7)(X);)
seq_stmt_impl$8
  ifelse($11,,,`ITERATOR->trace(IfEvent::PROBABILITY, m_pid, "", $11);')
  fire_name($1,$2$9)();
  ifelse($10,,,`ITERATOR->setPriority(getPriority());')
}')

