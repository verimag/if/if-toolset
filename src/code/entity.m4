#
#
#  IF-Toolset - Copyright (C) UGA - CNRS - G-INP
#
#  by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
#        Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
# 
#  This  software is  a computer  program whose  purpose is  to simulate,
#  explore and model-check  real-time systems represented using  the IF -
#  intermediate  format  -  language.   This  software  package  contains
#  sources,  documentation,  and  examples.
# 
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the  rules of distribution of free software.   You can use,
#  modify  and/ or  redistribute  the  software under  the  terms of  the
#  CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
#  URL "http://www.cecill.info".
#
#  As a counterpart to the access to  the source code and rights to copy,
#  modify and  redistribute granted  by the  license, users  are provided
#  only with a limited warranty and  the software's author, the holder of
#  the economic  rights, and the  successive licensors have  only limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using, modifying  and/or developing or  reproducing the
#  software by the user in light of its specific status of free software, 
#  that may  mean that  it is  complicated to  manipulate, and  that also
#  therefore means  that it  is reserved  for developers  and experienced
#  professionals having in-depth computer  knowledge. Users are therefore
#  encouraged  to load  and test  the software's  suitability as  regards
#  their  requirements  in  conditions  enabling the  security  of  their
#  systems and/or  data to  be ensured  and, more  generally, to  use and
#  operate it in the same conditions as regards security.
# 
#  The fact that  you are presently reading this means  that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
#



# --------------------------------------------------------------- #
#                                                                 #
# Ctl Helper Definitions                                          #
#                                                                 #
# --------------------------------------------------------------- #

# ctl_input_body(s1,integer,...)

define(`ctl_input_body', 
`ifelse($2,,,`if (m_ctl.$1 != -1)
      ok |= (STATE[m_ctl.$1].sigtab[signal] & 1);
    ctl_input_body(shift(shift($@)))')')

# ctl_save_body(s1,integer,...)

define(`ctl_save_body', 
`ifelse($2,,,`if (m_ctl.$1 != -1) 
      ok |= (STATE[m_ctl.$1].sigtab[signal] & 2);
    ctl_save_body(shift(shift($@)))')')

# ctl_is_body(s1,integer,...)

define(`ctl_is_body',
`ifelse($2,,,`if (m_ctl.$1 != -1)
    ok |= (STATE[m_ctl.$1].flags & flag);
  ctl_is_body(shift(shift($@)))')')

# ctl_tpc_body(s1,integer,...)

define(`ctl_tpc_body',
`ifelse($2,,,`if (m_ctl.$1 != -1)
    for(int sp = m_ctl.$1; ; sp = STATE[sp].parent) {
      if (STATE[sp].flags & TPC)
        (this->*(STATE[sp].tpc))();
      if (STATE[sp].flags & CONTROL)
        break;
    }
  ctl_tpc_body(shift(shift($@)))')')

# ctl_dispatch_body(s1,integer,...)

define(`ctl_dispatch_body',
`ifelse($2,,,`if (m_ctl.$1 != -1 && (int)(STATE[m_ctl.$1].flags & UNSTABLE) == unstable)
    (this->*(STATE[m_ctl.$1].dispatch))(message);
  ctl_dispatch_body(shift(shift($@)))')')

# ctl_assign(s1,v1,...)

define(`ctl_assign', 
`ifelse($2,,,`m_ctl.$1=$2;
  ctl_assign(shift(shift($@)))')')

define(`Aarg1', `$1')

# --------------------------------------------------------------- #
# print_state(c1,int,...)                                         #
define(`print_state', 
`ifelse($1,,,`if(m_ctl.$1 != -1) { sprintf(tmp,"$1:%s,", STATE[m_ctl.$1].name); strcat(r,tmp); }
  print_state(shift(shift($@)))')')



# --------------------------------------------------------------- #
#                                                                 #
# Instance Definitions                                            #
#                                                                 #
# --------------------------------------------------------------- #


define(`process_name', `if_$1_process')

# --------------------------------------------------------------- #
#  instance_name(i)                                               #

define(`instance_name', `if_$1_instance')


# --------------------------------------------------------------- #
# instance_decl(i,k,                                              #
#               (x1,T1,... ),                          parameters #
#               (x1,T1,... ),                           variables #
#               (x1,T1,e1, ..),                    var initialize #
#               (c1,int,...),                             control #
#               (...),                             ctl initialize #
#               (s1,... ))                                signals #

define(`instance_decl', `
define(`_INSTANCE_', `instance_name($1)')dnl
/* 
 * $1 instance interface 
 *
 */
 
`#define' process_name($1) $2

type_decl(record,$1_par,$3)

type_decl(record,$1_var,$4)

type_decl(record,$1_ctl,$6)

class _INSTANCE_ : public instance_name(_SYSTEM_) {

public:
  ifelse($3,(),,_INSTANCE_`'();)
  _INSTANCE_`'(var_par_decl$3);
  _INSTANCE_`'(const _INSTANCE_&);

  inline int GetSP() const { return m_ctl.top; }	// still used by observer operator "instate" -> only working without concurrent SMs

public:
  virtual const char* getState() const;
  virtual int is(const unsigned) const;

public:
  virtual int `compare'(const IfInstance*) const;
  virtual unsigned long `hash'(const unsigned long) const;
  virtual IfInstance* `copy'() const;
  virtual void `print'(FILE*) const;
  virtual void `printXML'(std::ostream&) const;

public: 
  virtual void `reset'();
  virtual void `iterate'(IfIterator*);
  virtual void `copy'(const IfInstance*);

public:
  static const char* NAME;

public:
  par_export$3
  var_export$4

private: 
  declare(m_par,$1_par);  /* parameters */ 
  declare(m_var,$1_var);  /* variables */
  declare(m_ctl,$1_ctl);  /* control */

private:  
  inline int input(unsigned signal) const {
    int ok = 0;
    ctl_input_body$6return ok;
  }
  inline int save(unsigned signal) const {
    int ok = 0;
    ctl_save_body$6return ok;
  }

private:
         void fire();
//         void tpc();
         void dispatch(IfMessage*, int);
  inline void nextstate();')

# instance_table_decl(L,B)                                        #

# instance_table_decl(L,B)                                        #

define(`instance_table_decl',
`private:
  typedef void (_INSTANCE_::*dispatcher)(IfMessage*);
//  typedef void (_INSTANCE_::*tpcchecker)();
  static if_state<$1,dispatcher/*,tpcchecker*/> STATE[];
')

private:
         void fire();
  inline void nextstate(int);')

# end_instance_decl(L,B)                                          #

define(`end_instance_decl', `};')



# --------------------------------------------------------------- #
# instance_impl(i,k,                                              #
#               (x1,T1,... ),                          parameters #
#               (x1,T1,... ),                           variables #
#               (x1,T1,e1, ..),                    var initialize #
#               (c1,int,...),                             control #
#               (...),                             ctl initialize #
#               (s1,... ))                                signals #

define(`instance_impl', `
define(`_INSTANCE_', `instance_name($1)')dnl
/* 
 * $1 instance implementation 
 *
 */

type_impl(record,$1_par,$3)

type_impl(record,$1_var,$4)

type_impl(record,$1_ctl,$6)

const char* _INSTANCE_::NAME = IfInstance::PROCNAME[$2] = "$1";

ifelse($3,(),,_INSTANCE_::_INSTANCE_`'() 
  : instance_name(_SYSTEM_)`'(if_pid_mk(process_name($1),0), IfQueue::NIL) {
  ctl_assign$7
    STATUS |=  CREATE;
  reset($1_var,m_var);
  reset($1_par,m_par);
    STATUS &= ~CREATE;
  var_initialize$5
})

_INSTANCE_::_INSTANCE_`'(var_par_decl$3) 
  : instance_name(_SYSTEM_)`'(if_pid_mk(process_name($1),0), IfQueue::NIL) {
  ctl_assign$7
    STATUS |=  CREATE;
  reset($1_var,m_var);
  ifelse($3,(),reset($1_par,m_par);,)
    STATUS &= ~CREATE;
  var_initialize$5
  par_cin$3
}

_INSTANCE_::_INSTANCE_`'(const _INSTANCE_& instance) 
  : instance_name(_SYSTEM_)`'(instance) {
  copy($1_ctl,m_ctl,instance.m_ctl);
  copy($1_var,m_var,instance.m_var);
  copy($1_par,m_par,instance.m_par);
}

const char* _INSTANCE_::`getState'() const {
  static char r[1024];
  char tmp[256];
  *r = (char)0;
  print_state$6
  r[strlen(r)-1]=(char)0;
  return r;
}

int _INSTANCE_::`is'(const unsigned flag) const {
  int ok = 0;
  ctl_is_body$6return ok;
}

int _INSTANCE_::`compare'(const IfInstance* X) const {
  _INSTANCE_* x = (_INSTANCE_*)X;
  int cmp = IfInstance::`compare'(X);
  if (cmp == 0) cmp = compare($1_ctl,m_ctl,x->m_ctl);
  if (cmp == 0) cmp = compare($1_var,m_var,x->m_var);
  if (cmp == 0) cmp = compare($1_par,m_par,x->m_par);
  return cmp;
}

unsigned long _INSTANCE_::`hash'(const unsigned long base) const {
  unsigned long key = m_pid;
  key += (unsigned long) m_queue; 
  if (sizeof(m_var) >= 4) // void 
    key += if_hash((char*) &m_var, sizeof(m_var), base);
  if (sizeof(m_par) >= 4) // void
    key += if_hash((char*) &m_par, sizeof(m_par), base);
  key += if_hash((char*) &m_ctl, sizeof(m_ctl), base);
  return key % base;
}

IfInstance* _INSTANCE_::`copy'() const {
  return new _INSTANCE_`'(*this);
}

void _INSTANCE_::`print'(FILE* f) const {
  // warning: `print' the topmost state name only
  IfInstance::`print'(f);
  fprintf(f, "\n  @%s\t\t", getState());
  print($1_var,m_var,f);
  fprintf(f, " ");
  print($1_par,m_par,f);
}

void _INSTANCE_::`printXML'(std::ostream& buf) const {
  buf << "<IfInstance ";
  buf << "type=\"$1\" ";
  buf << "state=\"" << getState() << "\" >\n";    
  IfInstance::`printXML'(buf);

  buf << "<var>\n";     
  printXML($1_var,m_var,buf);
  buf << "</var>\n";        

  buf << "<par>\n";     
  printXML($1_par,m_par,buf);
  buf << "</par>\n";        
  buf << "</IfInstance>\n";   
}

void _INSTANCE_::`copy'(const IfInstance* X) {
  _INSTANCE_* x = (_INSTANCE_*) X;
  IfInstance::`copy'(X);
  copy($1_ctl,m_ctl,x->m_ctl);
  copy($1_var,m_var,x->m_var);
  copy($1_par,m_par,x->m_par);
}

void _INSTANCE_::`reset'() {
  reset($1_ctl,m_ctl);
  ctl_assign$7
  reset($1_var,m_var);
  reset($1_par,m_par);
}

void _INSTANCE_::`iterate'(IfIterator* iterator) {
  STEP = 0;
  ITERATOR = iterator;
  fire();
  ITERATOR = NULL;
}


void _INSTANCE_::fire() {
  unsigned step = STEP;

// CS: no DISCARD since it complicates everything
//  if (opt_discard && (STATE[m_sp].flags & STABLE)) {
//    IfMessage* message = NULL;
//    for(IfQueue* queue = m_queue; 
//        queue != IfQueue::NIL; 
//        queue = (IfQueue*) queue->getTail()) {
//      message = (IfMessage*) queue->getHead();
//      if (!save(message->getSid()))
//        break;
//      message = NULL;
//    }
//    if (message != NULL) {
//      m_queue = m_queue->remove(message);
//      if (STATE[m_sp].flags & TSIG) {
//        ITERATOR->record();
//        (this->*(STATE[m_sp].dispatch))(message);
//        ITERATOR->forget();
//      }
//      if(STEP == step) {
//        IfTime::Constraint constraints[] = {{0,0,0}};
//        ITERATOR->guard(constraints,EAGER);      
//        ITERATOR->trace(IfEvent::DISCARD, m_pid, message->string(), m_pid, message->store());
//        nextstate(-1);
//      }
//    }
//    ITERATOR->restore();
//  } else {

  int unstable = is(UNSTABLE);

  if ( is(TSIG) ) {
    IfMessage* message = NULL;
    for(IfQueue* queue = m_queue; 
        queue != IfQueue::NIL; 
        queue = (IfQueue*) queue->getTail()) {
      message = (IfMessage*) queue->getHead();
      if (!save(message->getSid()))
        break;
      message = NULL;
    }
    if (message != NULL) {
      m_queue = m_queue->remove(message);
      ITERATOR->record();
      dispatch(message, unstable);
      ITERATOR->forget();
    }
    ITERATOR->restore();
  }
//  }
  if ( is(TNONE) )
    dispatch(NULL, unstable);
  if ( is(TSIG) ) {
    fire_external_signal$8
  }

  if (is(UNSTABLE) && step == STEP)
      fprintf(stderr, "\nerror #2: unstable deadlock: %s@%s\n", 
          NAME, getState());
}

void _INSTANCE_::dispatch(IfMessage* message, int unstable) {
  ctl_dispatch_body$6
}

void _INSTANCE_::nextstate() {

  if ( !is(UNSTABLE) )
    ITERATOR->step();
  else {
    ITERATOR->record();
    fire();
    ITERATOR->forget();
  }
}

') 

# instance_table_impl(L,B)                                        #

define(`instance_table_impl', 
`if_state<$1,_INSTANCE_::dispatcher> _INSTANCE_::STATE[] = {$2};')

# end_instance_impl(L,B)                                          #

define(`end_instance_impl', `')

# --------------------------------------------------------------- #

define(`fire_external_signal', 
`ifelse($1,,,`    if (input(signal_name($1)) && !save(signal_name($1))) {
      message_name($1) x;
      iterate($1_par,x.m_par) {
        ITERATOR->trace(IfEvent::FEED, m_pid, "");
        dispatch(&x,unstable);
      }
    }
    fire_external_signal(shift($@))')')

# --------------------------------------------------------------- #


# --------------------------------------------------------------- #
#                                                                 #
# Observer Definitions                                            #
#                                                                 #
# --------------------------------------------------------------- #


define(`observer_name', `if_$1_observer')

# --------------------------------------------------------------- #
#  obsinstance_name(i)                                               #

define(`obsinstance_name', `if_$1_instance')


# --------------------------------------------------------------- #
# obsinstance_decl(i,k,                                           #
#               (),                                 no parameters #
#               (x1,T1,... ),                           variables #
#               (x1,T1,e1, ..),                    var initialize #
#               (c1,int,...),                             control #
#               (...),                             ctl initialize #
#               (s1,... ))                                signals #

define(`obsinstance_decl', `
define(`_INSTANCE_', `obsinstance_name($1)')dnl
/* 
 * $1 instance interface 
 *
 */
 
`#define' observer_name($1) $2

type_decl(record,$1_var,$4)

type_decl(record,$1_ctl,$6)

class _INSTANCE_ : public IfObserverInstance {

public:
  ifelse($3,(),,_INSTANCE_`'();)
  _INSTANCE_`'(var_par_decl$3);
  _INSTANCE_`'(const _INSTANCE_&);

  inline int GetSP() const { return m_ctl.top; }	// still used by observer operator "instate" -> only working without concurrent SMs

public:
  virtual const char* getState() const;
  virtual int is(const unsigned) const;

public:
  virtual int `compare'(const IfInstance*) const;
  virtual long unsigned `hash'(const unsigned long) const;
  virtual IfInstance* `copy'() const;
  virtual void `print'(FILE*) const;
  virtual void `printXML'(std::ostream&) const;

public:
  virtual int isObsSuccess() const { return STATE[m_ctl.top].flags & OSUCCESS; }
  virtual int isObsError() const { return STATE[m_ctl.top].flags & OERROR; }

public: 
  virtual void `reset'();
  virtual void `iterate'(IfIterator*);
  virtual void `copy'(const IfInstance*);

public:
  static const char* NAME;

public:
  var_export$4

private:
  declare(m_var,$1_var);  /* variables */
  declare(m_ctl,$1_ctl);  /* control */

private:  
  inline int input(unsigned signal) const {
    int ok = 0;
    ctl_input_body$6return ok;
  }
  inline int save(unsigned signal) const {
    int ok = 0;
    ctl_save_body$6return ok;
  }

private:
         void fire();
         void dispatch(IfMessage*, int);
  inline void nextstate();')

# obsinstance_table_decl(L,B)                                        #

define(`obsinstance_table_decl',
`private:
  typedef void (_INSTANCE_::*dispatcher)(IfMessage*);
  static if_state<$1,dispatcher> STATE[];
')

# end_obsinstance_decl(L,B)                                          #

define(`end_obsinstance_decl', `};')



# --------------------------------------------------------------- #
# obsinstance_impl(i,k,                                           #
#               (),                                 no parameters #
#               (x1,T1,... ),                           variables #
#               (x1,T1,e1, ..),                    var initialize #
#               (c1,int,...),                             control #
#               (...),                             ctl initialize #
#               (s1,... ))                                signals #

define(`obsinstance_impl', `
define(`_INSTANCE_', `obsinstance_name($1)')dnl
/* 
 * $1 instance implementation 
 *
 */

type_impl(record,$1_var,$4)

type_impl(record,$1_ctl,$6)

const char* _INSTANCE_::NAME = IfInstance::PROCNAME[$2] = "$1";

ifelse($3,(),,_INSTANCE_::_INSTANCE_`'() 
  : IfObserverInstance`'(if_pid_mk(observer_name($1),0), IfQueue::NIL) {
  ctl_assign$7
    STATUS |=  CREATE;
  reset($1_var,m_var);
    STATUS &= ~CREATE;
  var_initialize$5
})

_INSTANCE_::_INSTANCE_`'(var_par_decl$3) 
  : IfObserverInstance`'(if_pid_mk(observer_name($1),0), IfQueue::NIL) {
  ctl_assign$7
    STATUS |=  CREATE;
  reset($1_var,m_var);
    STATUS &= ~CREATE;
  var_initialize$5
}

_INSTANCE_::_INSTANCE_`'(const _INSTANCE_& instance) 
  : IfObserverInstance`'(instance) {
  copy($1_ctl,m_ctl,instance.m_ctl);
  copy($1_var,m_var,instance.m_var);
}

const char* _INSTANCE_::`getState'() const {
  static char r[1024];
  char tmp[256];
  *r = (char)0;
  print_state$6
  r[strlen(r)-1]=(char)0;
  return r;
}

int _INSTANCE_::`is'(const unsigned flag) const {
  int ok = 0;
  ctl_is_body$6return ok;
}

int _INSTANCE_::`compare'(const IfInstance* X) const {
  _INSTANCE_* x = (_INSTANCE_*)X;
  int cmp = IfObserverInstance::`compare'(X);
  if (cmp == 0) cmp = compare($1_ctl,m_ctl,x->m_ctl);
  if (cmp == 0) cmp = compare($1_var,m_var,x->m_var);
  return cmp;
}

unsigned long _INSTANCE_::`hash'(const unsigned long base) const {
  unsigned long key = m_pid;
  key += (unsigned long) m_curLabel; 
  if (sizeof(m_var) >= 4) // void 
    key += if_hash((char*) &m_var, sizeof(m_var), base);
  key += if_hash((char*) &m_ctl, sizeof(m_ctl), base);
  return key % base;
}

IfInstance* _INSTANCE_::`copy'() const {
  return new _INSTANCE_`'(*this);
}

void _INSTANCE_::`print'(FILE* f) const {
  // warning: `print' the topmost state name only
  IfObserverInstance::`print'(f);
  fprintf(f, "\n  @%s\t\t", getState());
  print($1_var,m_var,f);
}

void _INSTANCE_::`printXML'(std::ostream& buf) const {
  buf << "<IfObserverInstance ";
  buf << "type=\"$1\" ";
  buf << "state=\"" << getState() << "\" ";
  buf << "status = \"" << (isObsError()? "error" : (isObsSuccess()? "success" : "none")) << "\" ";
  buf << "cut=\"" << (m_bCut?"true":"false") << "\" >\n";    

  IfObserverInstance::`printXML'(buf);

  buf << "<var>\n";     
  printXML($1_var,m_var,buf);
  buf << "</var>\n";        

  buf << "</IfObserverInstance>\n";
}

void _INSTANCE_::`copy'(const IfInstance* X) {
  _INSTANCE_* x = (_INSTANCE_*) X;
  IfObserverInstance::`copy'(X);
  copy($1_ctl,m_ctl,x->m_ctl);
  copy($1_var,m_var,x->m_var);
}

void _INSTANCE_::`reset'() {
  reset($1_ctl,m_ctl);
  ctl_assign$7
  reset($1_var,m_var);
}

void _INSTANCE_::`iterate'(IfIterator* iter) {
  STEP = 0;
  ITERATOR = iter;
  fire();
  ITERATOR = NULL;
}

void _INSTANCE_::fire() {
  unsigned step = STEP;

  int unstable = is(UNSTABLE);

  if ( is(TSIG) ) {
    IfMessage* message = NULL;
    for(IfQueue* queue = m_queue; 
        queue != IfQueue::NIL; 
        queue = (IfQueue*) queue->getTail()) {
      message = (IfMessage*) queue->getHead();
      if (!save(message->getSid()))
        break;
      message = NULL;
    }
    if (message != NULL) {
      m_queue = m_queue->remove(message);
      ITERATOR->record();
      dispatch(message, unstable);
      ITERATOR->forget();
    }
    ITERATOR->restore();
  }
  if ( is(TNONE) )
    dispatch(NULL, unstable);
  if ( is(TSIG) ) {
    fire_external_signal$8
  }

  if (is(UNSTABLE) && step == STEP)
      fprintf(stderr, "\nerror #2: unstable deadlock: %s@%s\n", 
          NAME, STATE[m_ctl.top].name);
}

void _INSTANCE_::dispatch(IfMessage* message, int unstable) {
  ctl_dispatch_body$6
}

void _INSTANCE_::nextstate() {

  if ( !is(UNSTABLE) )
    ITERATOR->step();
  else {
    ITERATOR->record();
    fire();
    ITERATOR->forget();
  }
}

') 

# obsinstance_table_impl(L,B)                                        #

define(`obsinstance_table_impl', 
`if_state<$1,_INSTANCE_::dispatcher> _INSTANCE_::STATE[] = {$2};')

# end_obsinstance_impl(L,B)                                          #

define(`end_obsinstance_impl', `')

# --------------------------------------------------------------- #
#                                                                 #
# System Definitions                                              #
#                                                                 #
# --------------------------------------------------------------- #

#
# start((route1,... routen),(proc1, ... procm),[monitor],time)
#

define(`start_decl',`')

define(`start_buffer',
`ifelse($1,,,`instance = new buffer_name($1);
  fork(instance, &pid);
  delete instance;
  start_buffer(shift($@))')')

define(`start_instance',
`ifelse($1,,,`instance = new instance_name($1);
  fork(instance, &pid);
  delete instance;
  start_instance(shift($@))')')

define(`start_impl',
`IfConfig* IfIterator::start() {
  if_pid_type pid = 0;
  IfInstance* instance = NULL;
  IfConfig empty(1);
  
  m_config.put(&empty);

  start_buffer$1
  start_instance$2

  ifelse($3,monitor,instance=new IfMonitor;
  fork(instance, &pid);
  delete instance;,)

  instance = new If`'_TIME_`'Time();
  fork(instance, &pid);
  delete instance; 

  // options
  opt_use_priorities = x_use_priorities;

  return m_config.get();
}
')

# --------------------------------------------------------------- #
#
# system_instance_decl(S)
#

define(`system_instance_decl', `
define(`_INSTANCE_', `instance_name($1)')dnl
/* 
 * $1 [system] instance interface 
 *
 */

class _INSTANCE_ : public IfInstance { 

  public:
    _INSTANCE_`'(if_pid_type = 0, IfQueue* = IfQueue::NIL);
    _INSTANCE_`'(const _INSTANCE_&);

  protected:
')

define(`end_system_instance_decl', `};')

# --------------------------------------------------------------- #
#
# system_instance_impl(S)


define(`system_instance_impl',`
define(`_INSTANCE_', `instance_name($1)')dnl
/* 
 * $1 [system] instance implementation 
 *
 */

_INSTANCE_::_INSTANCE_`'(if_pid_type pid, IfQueue* queue) 
  : IfInstance(pid, queue) {
} 

_INSTANCE_::_INSTANCE_`'(const _INSTANCE_& instance) 
  : IfInstance(instance) {
}
')

define(`end_system_instance_impl', `')

# --------------------------------------------------------------- #
#
# system_decl(S)

define(`system_decl', `define(`_SYSTEM_', `$1')')

define(`system_impl', `define(`_SYSTEM_', `$1')')
