#
#
#  IF-Toolset - Copyright (C) UGA - CNRS - G-INP
#
#  by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
#        Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
# 
#  This  software is  a computer  program whose  purpose is  to simulate,
#  explore and model-check  real-time systems represented using  the IF -
#  intermediate  format  -  language.   This  software  package  contains
#  sources,  documentation,  and  examples.
# 
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the  rules of distribution of free software.   You can use,
#  modify  and/ or  redistribute  the  software under  the  terms of  the
#  CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
#  URL "http://www.cecill.info".
#
#  As a counterpart to the access to  the source code and rights to copy,
#  modify and  redistribute granted  by the  license, users  are provided
#  only with a limited warranty and  the software's author, the holder of
#  the economic  rights, and the  successive licensors have  only limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using, modifying  and/or developing or  reproducing the
#  software by the user in light of its specific status of free software, 
#  that may  mean that  it is  complicated to  manipulate, and  that also
#  therefore means  that it  is reserved  for developers  and experienced
#  professionals having in-depth computer  knowledge. Users are therefore
#  encouraged  to load  and test  the software's  suitability as  regards
#  their  requirements  in  conditions  enabling the  security  of  their
#  systems and/or  data to  be ensured  and, more  generally, to  use and
#  operate it in the same conditions as regards security.
# 
#  The fact that  you are presently reading this means  that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
#



# --------------------------------------------------------------- #
#                                                                 #
# Variable Definitions                                            #
#                                                                 #
# --------------------------------------------------------------- #


# ----------------------------------------------------------------#
# Declarations                                                    #
# ----------------------------------------------------------------#

# declare(x,T)                                                    #

define(`declare',`type_name($2) $1') 

# var_par_decl(x1,T1,...,xn,Tn)                                   #

define(`var_par_decl',
`ifelse($2$4,,,
	$4,,declare($1,$2),
	`declare($1,$2),var_par_decl(shift(shift($@)))')')

# var_var_decl(x1,T1,... xn,Tn)                                   #

define(`var_var_decl',
`ifelse($2$4,,,
	$4,,declare($1,$2);,
	`declare($1,$2);
  var_var_decl(shift(shift($@)))')')


# ----------------------------------------------------------------#
# Structure Implementation                                        #
# ----------------------------------------------------------------#

# var_copy(x1,T1,...xn,Tn)                                        #

define(`var_copy',
`ifelse($2$4,,,
	$4,,copy($2,x.$1,y.$1);,
	`copy($2,x.$1,y.$1);
  var_copy(shift(shift($@)))')')


# var_compare(x1,T1,...xn,Tn)                                     #

define(`var_compare',
`ifelse($2$4,,,
	$4,,if (c == 0) c = compare($2,x.$1,y.$1);,
	`if (c == 0) c = compare($2,x.$1,y.$1);
  var_compare(shift(shift($@)))')')

# var_print(x1,T1,...xn,Tn)                                       #

define(`var_print',
`ifelse($2$4,,,
	$4,,fprintf(f,"$1="); print($2,x.$1,f);,
	`fprintf(f,"$1="); print($2,x.$1,f); fprintf(f,",");
  var_print(shift(shift($@)))')')

# var_print_xml(x1,T1,...xn,Tn)                                       #

define(`var_print_xml',
`ifelse($2$4,,,
	$4,,b << "<$1>"; printXML($2,x.$1,b); b << "</$1>"; ,
	`b << "<$1>"; printXML($2,x.$1,b); b << "</$1>";
  var_print_xml(shift(shift($@)))')')

# var_reset(x1,T1,...xn,Tn)                                       #

define(`var_reset',
`ifelse($2$4,,,
	$4,,reset($2,x.$1);,
	`reset($2,x.$1);
  var_reset(shift(shift($@)))')')

# var_iterate(x1,T1,...xn,Tn)                                       #

define(`var_iterate',
`ifelse($2$4,,,
	$4,,iterate($2,_x_.$1),
	`iterate($2,_x_.$1)\
  var_iterate(shift(shift($@)))')')


# ----------------------------------------------------------------#

# var_export(x1,T1,... xn,Tn)

define(`var_export',
`ifelse($2,,,`inline type_name($2)& `$1'()
    { return m_var.$1; }
  var_export(shift(shift($@)))')')

# var_initialize(x1,T1,e1,... xn,Tn,en)

define(`var_initialize',
`ifelse($3,,,`copy($2,m_var.$1,$3);
  var_initialize(shift(shift(shift($@))))')')

# ----------------------------------------------------------------#

# par_export(x1,T1,... xn, Tn)

define(`par_export',
`ifelse($2,,,`inline type_name($2)& `$1'()
    { return m_par.$1; }
  par_export(shift(shift($@)))')')

# ----------------------------------------------------------------#

# par_cin(x1,T1,... xn,Tn)                                        #

define(`par_cin',
`ifelse($2$4,,,
	$4,,copy($2,m_par.$1,$1);,
	`copy($2,m_par.$1,$1);
  par_cin(shift(shift($@)))')')

# par_cout(x1,p1,T1,... xn,pn,Tn)                                 #

define(`par_cout',
`ifelse($3$6,,,
	$6,,copy($3,$1,x->m_par.$2);,
	`copy($3,$1,x->m_par.$2);
  par_cout(shift(shift(shift($@))))')')


