#
#
#  IF-Toolset - Copyright (C) UGA - CNRS - G-INP
#
#  by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
#        Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
# 
#  This  software is  a computer  program whose  purpose is  to simulate,
#  explore and model-check  real-time systems represented using  the IF -
#  intermediate  format  -  language.   This  software  package  contains
#  sources,  documentation,  and  examples.
# 
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the  rules of distribution of free software.   You can use,
#  modify  and/ or  redistribute  the  software under  the  terms of  the
#  CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
#  URL "http://www.cecill.info".
#
#  As a counterpart to the access to  the source code and rights to copy,
#  modify and  redistribute granted  by the  license, users  are provided
#  only with a limited warranty and  the software's author, the holder of
#  the economic  rights, and the  successive licensors have  only limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using, modifying  and/or developing or  reproducing the
#  software by the user in light of its specific status of free software, 
#  that may  mean that  it is  complicated to  manipulate, and  that also
#  therefore means  that it  is reserved  for developers  and experienced
#  professionals having in-depth computer  knowledge. Users are therefore
#  encouraged  to load  and test  the software's  suitability as  regards
#  their  requirements  in  conditions  enabling the  security  of  their
#  systems and/or  data to  be ensured  and, more  generally, to  use and
#  operate it in the same conditions as regards security.
# 
#  The fact that  you are presently reading this means  that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
#



# --------------------------------------------------------------- #
#                                                                 #
# Route Definitions                                               #
#                                                                 #
# --------------------------------------------------------------- #

define(`route_name', `if_$1_route')


# --------------------------------------------------------------- #
# buffer_name(b)                                                  #

define(`buffer_name', `if_$1_buffer')


# --------------------------------------------------------------- #
# buffer_decl(b,k,X,p1,p2,f,l,u,p)

define(`buffer_decl', `
define(`_BUFFER_', `buffer_name($1)')dnl
/*
 * $1 buffer interface
 *
 */

`#define' route_name($1) $2

class _BUFFER_ : public If$3Buffer {

public:
  _BUFFER_`'();
  _BUFFER_`'(const _BUFFER_&);

public:
  virtual IfInstance* `copy'() const;
  virtual void `printXML'(std::ostream&) const;

public:
  static const char* NAME;

};')


# --------------------------------------------------------------- #
# buffer_decl(b,k,X,p1,p2,f,l,u,p)

define(`buffer_impl', `
define(`_BUFFER_', `buffer_name($1)')dnl
/*
 * $1 buffer inmplementation
 *
 */

const char* _BUFFER_::NAME = IfInstance::PROCNAME[$2] = "$1";

_BUFFER_::_BUFFER_`'() 
 : If$3Buffer(if_pid_mk(process_name($4),0), if_pid_mk(process_name($5),0), 
     $6, $7, $8, if_pid_mk(route_name($1),0), IfQueue::NIL, $9) {
}

_BUFFER_::_BUFFER_`'(const _BUFFER_& buffer) 
 : If$3Buffer(buffer) {
}

IfInstance* _BUFFER_::`copy'() const {
  return new _BUFFER_`'(*this);
}

void _BUFFER_::`printXML'(std::ostream& buf) const {
  buf << "<IfInstance ";
  buf << "type=\"$1\" >\n";
  IfBuffer::`printXML'(buf);
  buf << "</IfInstance>\n";   
}
')

# --------------------------------------------------------------- #
