/*
 *
 * IF-Toolset - Copyright (C) UGA - CNRS - G-INP
 *
 * by  Marius  -  Iulian  -  Susanne  -  Laurent  -   Joseph
 *       Bozga  -    Ober  -     Graf  -  Mounier  -   Sifakis
 * 
 * This  software is  a computer  program whose  purpose is  to simulate,
 * explore and model-check  real-time systems represented using  the IF -
 * intermediate  format  -  language.   This  software  package  contains
 * sources,  documentation,  and  examples.
 * 
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the  rules of distribution of free software.   You can use,
 * modify  and/ or  redistribute  the  software under  the  terms of  the
 * CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to  the source code and rights to copy,
 * modify and  redistribute granted  by the  license, users  are provided
 * only with a limited warranty and  the software's author, the holder of
 * the economic  rights, and the  successive licensors have  only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using, modifying  and/or developing or  reproducing the
 * software by the user in light of its specific status of free software, 
 * that may  mean that  it is  complicated to  manipulate, and  that also
 * therefore means  that it  is reserved  for developers  and experienced
 * professionals having in-depth computer  knowledge. Users are therefore
 * encouraged  to load  and test  the software's  suitability as  regards
 * their  requirements  in  conditions  enabling the  security  of  their
 * systems and/or  data to  be ensured  and, more  generally, to  use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that  you are presently reading this means  that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 */
/* 
 * This example describes a parallel server which can handle at 
 * maximum N requests simultaneously.  Thus, when possible, for a 
 * -request- message (received from the environment) a client 
 * is created.  The server keeps in the -i- variable the number 
 * of running clients.  Client processes are quite simple: once 
 * created, they work, and when finished they send a -done- message 
 * back to the server.  In addition, these signals are delayed 
 * through the signalroute cs. 
 * 
 */

system server_test;

const N = 2;

signal done(pid);
signal request();


signalroute es(1)
  from env to server
  with request;

signalroute cs(1) // #delay[1,2]
  from client to server
  with done;


process server(1);

var i integer;
var x pid;

state idle #start ;
  deadline lazy;
  provided (i < N);
  input request();
    x := fork client(self);
    task i := (i + 1);
      nextstate idle;
  input done(x);
    task i := (i - 1);
      nextstate idle;
endstate;
endprocess;


process client(0);
 fpar parent pid;

state init #start ;
   deadline lazy;
    informal "work";
    output done(self) via {cs}0 to parent;
      stop;
endstate;
endprocess;
endsystem;
